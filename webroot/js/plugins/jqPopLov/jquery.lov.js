(function($){


	$.fn.lov = function( options ){

		var loaded = false;
		var windowRendered = false;
		var localdata = [];
		var settings  = {'title':'Pop Lov','width':500,'height':400,'fullscreen':false,'filtertype':'row','fitlerable':true,'sortable':true,'selectionmode':'singlerow',"value":"","uid":"id"};
		var component;
		settings.valueid = ($(this).attr("data-value")!=undefined)?$(this).attr("data-value"):"";

		if(options == 'clearFields')
		{
			clearFields();
			return;
		}

		settings = $.extend(settings,options);

		component = $(this);

		settings.url = ($(this).attr("data-source")!=""||$(this).attr("data-source")!="undefined")?$(this).attr("data-source"):"/";

		$(this).removeAttr("data-source");
		
		if(!loaded){
			$.get(settings.url,function(data){
				jsonData = JSON.parse(data);
				loaded = true;
				settings.localdata = jsonData;	
				settings.source = $.extend({localdata:settings.localdata,id:settings.uid,datatype:'json'},settings.source);
				settings.adapter = new $.jqx.dataAdapter(settings.source);
				fillValue();
				render();
			});
		}
		else{
			fillValue();
			render();
		}

		settings.id = randomString(5,"aA#");
			

		var clearFields = function()
		{
			$("#"+settings.id+" .label-input").val("");
		}

		var refreshData = function()
		{
			$.get(settings.url,function(data){
				jsonData = JSON.parse(data);
				loaded = true;
				localdata = jsonData;	
				settings.source.localdata = undefined;
				settings.source = $.extend({localdata:localdata,id:settings.uid,datatype:'json'},settings.source);

				settings.adapter = new $.jqx.dataAdapter(settings.source);
				settings.adapter.dataBind();
				$("#"+settings.gridid).jqxGrid({source:settings.adapter});
				$("#"+settings.gridid).jqxGrid('refreshData');
			});
		}

		var fillValue = function()
		{
			if(settings.valueid!=""){
				for(var idx in settings.localdata)
				{
					if(settings.localdata[idx][settings.uid] == settings.valueid)
					{
						settings.value = settings.localdata[idx][settings.displayfield];
						break;
					}
				}
			}
			else{
				settings.value = "";
			}
		}

		var render = function()
		{
			if(settings.selectionmode == "singlerow"){
				var required = "";
				if((typeof(settings.required) != undefined) && settings.required)
					required = "required";
				$(component).append(
					"<div id='"+settings.id+"'>"+
						"<div class='input-group'>"+
							"<input type='text' class='form-control label-input' value='"+settings.value+"' "+required+" readonly/>"+
							"<span class='input-group-btn'>"+
								"<button class='btn btn-default btn-open'>"+
									"<i class='fa fa-search'></i>"+
								"</button>"+
								"<button class='btn btn-danger btn-remove'>"+
									"<i class='fa fa-trash'></i>"+
								"</button>"+
							"</span>"+
						"</div>"+
					"</div>");

				$("#"+settings.id+" .btn-remove").click(function(e)
				{
					e.preventDefault();

					$("#"+settings.id+" .label-input").val("");
					settings.removeCallback();
				});
			}
			else
			{
				$(component).append(
					"<div id='"+settings.id+"' style='float:left'>"+
						"<button class='btn btn-default btn-open'>"+
							"<i class='fa fa-plus'></i>"+
						"</button>"+
					"</div>"
				);
			}


			$("#"+settings.id+" .btn-open").click(function(e)
			{
				e.preventDefault();

				if(!windowRendered){
					renderWindow();
					renderGrid();
				}
			})
		}
		var renderWindow = function()
		{
			settings.windowid = randomString(5,"aA#");
			settings.gridid = randomString(5,"aA#");
			$(component).append(
				"<div id='"+settings.windowid+"'>"+
					"<div>"+settings.title+"</div>"+
					"<div class='content'>"+
						"<div class='toolbar'>"+
							"<button class='refresh btn btn-sm btn-default'>refresh</button>"+
							"<button class='clearfilter btn btn-sm btn-default'>clear filter</button>"+
						"</div>"+
						"<div id='"+settings.gridid+"'></div>"+
					"</div>"+
				"</div>");
			
			var x = ($(window).width()-settings.width)/2
			var y = ($(window).height()-settings.height)/2+$(window).scrollTop();

			$("#"+settings.windowid).jqxWindow({
				width:settings.width+"px",
				height:settings.height+"px",
				maxWidth:$(window).width(),
				maxHeight:$(window).height(),
				position:{x:x,y:y}
			});
			windowRendered = true;	
			$("#"+settings.windowid).on('close',function(e)
			{
				windowRendered = false;
			});
			$("#"+settings.windowid+" .refresh").on('click',function(e)
			{
				e.preventDefault();
				refreshData();
			});
			$("#"+settings.windowid+" .clearfilter").on('click',function(e)
			{
				e.preventDefault();
				$("#"+settings.gridid).jqxGrid('clearfilters');
			});
			if(settings.selectionmode=='multiplerows')
			{
				$("#"+settings.windowid+" .toolbar").append("<button class='btn btn-sm btn-default btn-select pull-right'>select</button>");
				$("#"+settings.windowid+" .btn-select").on('click',function(e)
				{
					var indexes = $("#"+settings.gridid).jqxGrid('getselectedrowindexes');
					var items = [];
					if(indexes.length>0)
					{
						for(index in indexes)
						{
							var item = settings.adapter.records[indexes[index]];
							items.push(item);
						}
					}
					$("#"+settings.windowid).jqxWindow('close');
					settings.callback(items);
				});
			}
		}
		var renderGrid = function()
		{
			var selectionmode = (settings.selectionmode=="singlerow")?"singlerow":"multiplerows";

			var gridSettings = {
				width:'100%',
				height: '100%',
				source: settings.adapter,
				filterable:  settings.filterable,
				sortable: settings.sortable,
				columnsresize: true,
				selectionmode : selectionmode,
				columns : settings.columns
			};

			if(settings.filtertype == 'row')
			{
				gridSettings.showfilterrow = true
			}
			else if(settings.filtertype == 'excel')
			{
				gridSettings.filtermode = 'excel'
			}

			$("#"+settings.gridid).jqxGrid(gridSettings);
			$("#"+settings.gridid).jqxGrid('clearfilters');

			if(selectionmode=="singlerow"){
				$("#"+settings.gridid).on('rowdoubleclick',function(e)
				{
					var index = e.args.rowindex;
					var item = settings.adapter.records[index];
					$("#"+settings.id+" .label-input").val(item[settings.displayfield]);
					$("#"+settings.windowid).jqxWindow('close');
					settings.callback([item]);
				});
			}
		}

	}

	var randomString = function(length, chars) {
	    var mask = '';
	    if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
	    if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    if (chars.indexOf('#') > -1) mask += '0123456789';
	    if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
	    var result = '';
	    for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
	    return result;
	}

})(jQuery);