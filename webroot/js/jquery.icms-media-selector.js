(function($){

    var settings = {'title':'Media Selector','multiple':true,'filter':'image,video,audio,document,excel,archive'};

    var methods = {
        init : function(options)
        {

            var checked = [];

            var mode = 'singlemode';

            settings = $.extend(settings,options);

            settings.id = randomString(5,"aA#");

            var component = $(this);
            var innercomp = "";

            var rendered = false;

            var render = function()
            {
                $(component).wrap("<div class='media-selector'></div>")
                renderWindow();
                $(component).attr("data-toggle","modal").attr("data-target","#"+settings.id);
                renderTab();

                $(component).click(function (e) {
                    e.preventDefault();
                    $.get(settings.list_url+"?m="+settings.filter,function(data){
                        renderList(data);
                    });
                });

                rendered = true;
            }

            var renderWindow = function()
            {
                innercomp = '<div id="'+settings.id+'" class="modal icms-modal fade" tabindex="-1" role="dialog"><div class="modal-dialog icms-modal-lg-prc" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">'+settings.title+'</h4></div><div class="modal-body"></div><div class="modal-footer"><button id="'+settings.id+'-btn" class="btn btn-sm btn-primary pull-right">set '+settings.title.toLowerCase()+'</button></div></div></div></div></div>';
                $(component).parent().append($(innercomp));
                $("#"+settings.id+"-btn").click(function(e)
                {
                    e.preventDefault();
                    collectChecked();
                    if(checked.length>0) {
                        $("#"+settings.id).modal("hide");
                        if ("onselect" in settings) {
                            if (!settings.multiple) {
                                if (checked.length > 0)
                                    checked = checked[0];
                            }
                            settings.onselect.call(this, e, checked);
                        }
                    }
                    else{
                        alert("please choose one");
                    }
                });
            }

            var renderTab = function()
            {
                var randomId = randomString(5,"aA#");
                settings.tab_id = randomId;
                var tab = '<div><ul class="nav nav-tabs icms-nav-tabs" role="tablist"><li role="presentation"><a href="#'+randomId+'-uploadmedia" aria-controls="uploadmedia" role="tab" data-toggle="tab">Upload Media</a></li><li role="presentation" class="active"><a href="#'+randomId+'-mediaselector" aria-controls="profile" role="tab" data-toggle="tab">Media Library</a></li></ul><div class="tab-content"><div role="tabpanel" class="tab-pane upload-media" id="'+randomId+'-uploadmedia"></div><div role="tabpanel" class="tab-pane media-selector clearfix active" id="'+randomId+'-mediaselector"></div></div></div>';
                $("#"+settings.id+" .modal-body").append($(tab));
                renderMediaSelector();
                renderMediaUpload();
            }

            var renderMediaSelector = function()
            {
                if(settings.multiple){
                    var mediaselector = '<div class="col-md-12 col-sm-12 col-xs-12 media-list-wrap"><ol class="media-list"></ol></div>'
                }
                else {
                    var mediaselector = '<div class="col-md-9 col-sm-12 col-xs-12 media-list-wrap"><ol class="media-list"></ol></div><div class="col-md-3 hidden-sm hidden-xs attribute-wrap"></div>'
                }
                $("#"+settings.id+" .media-selector").append($(mediaselector));
            }

            var renderAttribute = function(id)
            {
                if(id==undefined)
                {
                    $("#"+settings.id+" .attribute-wrap").empty();
                }else {
                    $.get(settings.attribute_url + "/" + id, function (data) {
                        $("#" + settings.id + " .attribute-wrap").empty().append($(data));
                    });
                }
            }

            var renderList = function(listComp)
            {
                $("#"+settings.id+" .media-list").empty().append($(listComp));
                isradio = !settings.multiple;
                $("#"+settings.id+" .media-list img").imgCheckbox({
                    radio:isradio,
                    checkMarkPosition:'top-right',
                    canDeselect:true,
                    onclick:function(){
                        if(!settings.multiple){
                            var mediaId = $(".imgChked img").first().attr("id");
                            renderAttribute(mediaId);
                        }
                    }
                });
            }

            var collectChecked = function()
            {
                $(".imgChked img").each(function(data)
                {
                    checked.push($(this).attr("id"));
                })
            }

            var recheckChecked = function()
            {
                for(check in checked){
                    $("#"+checked[check]).select();
                }
                checked = [];
            }

            var renderMediaUpload = function()
            {
                var randomId = randomString(5,"aA#");
                var dropzone = '<div id="'+randomId+'-dropzone" class="dropzone"><div class="dropzone-inner"><p class="dropzone-inner-info">Drop File here</p><p>or</p><p class="dropzone-inner-buttons"><span class="btn btn-default btn-sm fileinput-button"><span>select files</span><input id="'+randomId+'-fileupload" type="file" name="files[]" multiple/></span></p><p>Maximum upload file size: 200 MB.</p></div><div id="'+randomId+'-progress-bar"><div class="progress-bar"></div></div></div>';
                $("#"+settings.id+" .upload-media").append($(dropzone));
                $(document).bind('dragover', function (e) {
                    var dropZone = $('#'+randomId+"-dropzone"),
                        timeout = window.dropZoneTimeout;
                    if (!timeout) {
                        dropZone.addClass('dropzone-in');
                    } else {
                        clearTimeout(timeout);
                    }
                    var found = false,
                        node = e.target;
                    do {
                        if (node === dropZone[0]) {
                            found = true;
                            break;
                        }
                        node = node.parentNode;
                    } while (node != null);
                    window.dropZoneTimeout = setTimeout(function () {
                        window.dropZoneTimeout = null;
                        dropZone.removeClass('dropzone-in hover');
                    }, 100);
                });
                $('#'+randomId+'-fileupload').fileupload({
                    dropZone: $('#'+randomId+'-dropzone'),
                    url: settings.upload_url+"?m="+settings.filter,
                    dataType: 'json',
                    done: function (e, data) {
                        $("#"+randomId+"-progress-bar").addClass("hidden");
                        $.get(settings.list_url+"?m="+settings.filter,function(data){
                            collectChecked();
                            renderList(data);
                            recheckChecked();
                            $("#"+settings.id+" .media-list-wrap .progress-wrap").remove();
                            $("#"+settings.id+" .media-list-wrap").css("overflow-y",'auto');
                        });
                    },
                    progressall: function (e, data) {
                        $("#"+settings.id+" .media-list-wrap").append("<div class='progress-wrap'></div>");
                        $("#"+settings.id+" .media-list-wrap").css("overflow-y",'hidden');
                        $("#"+settings.id+" a[href='#"+settings.tab_id+"-mediaselector']").tab('show');
                    }
                }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
            }
            if(!rendered)
            {
                render();
            }

        },
        destroy : function(){
            $(this).unbind('click');
            $("#"+settings.id).remove();
        }
    }

    $.fn.mediaSelector = function( methodOrOptions )
    {
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.tooltip' );
        }
    }

    var randomString = function(length, chars) {
        var mask = '';
        if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
        if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if (chars.indexOf('#') > -1) mask += '0123456789';
        if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
        var result = '';
        for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
        return result;
    }
})(jQuery);