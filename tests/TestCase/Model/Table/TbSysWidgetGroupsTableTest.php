<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TbSysWidgetGroupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TbSysWidgetGroupsTable Test Case
 */
class TbSysWidgetGroupsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TbSysWidgetGroupsTable
     */
    public $TbSysWidgetGroups;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tb_sys_widget_groups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysWidgetGroups') ? [] : ['className' => 'App\Model\Table\TbSysWidgetGroupsTable'];
        $this->TbSysWidgetGroups = TableRegistry::get('TbSysWidgetGroups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysWidgetGroups);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
