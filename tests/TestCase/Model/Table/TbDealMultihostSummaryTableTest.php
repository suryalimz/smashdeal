<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TbDealMultihostSummaryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TbDealMultihostSummaryTable Test Case
 */
class TbDealMultihostSummaryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TbDealMultihostSummaryTable
     */
    public $TbDealMultihostSummary;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tb_deal_multihost_summary'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbDealMultihostSummary') ? [] : ['className' => 'App\Model\Table\TbDealMultihostSummaryTable'];
        $this->TbDealMultihostSummary = TableRegistry::get('TbDealMultihostSummary', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbDealMultihostSummary);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
