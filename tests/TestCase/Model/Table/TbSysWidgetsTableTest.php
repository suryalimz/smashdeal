<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TbSysWidgetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TbSysWidgetsTable Test Case
 */
class TbSysWidgetsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TbSysWidgetsTable
     */
    public $TbSysWidgets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tb_sys_widgets'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysWidgets') ? [] : ['className' => 'App\Model\Table\TbSysWidgetsTable'];
        $this->TbSysWidgets = TableRegistry::get('TbSysWidgets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysWidgets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
