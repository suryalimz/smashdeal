<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TbSysMenusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TbSysMenusTable Test Case
 */
class TbSysMenusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TbSysMenusTable
     */
    public $TbSysMenus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tb_sys_menus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysMenus') ? [] : ['className' => 'App\Model\Table\TbSysMenusTable'];
        $this->TbSysMenus = TableRegistry::get('TbSysMenus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysMenus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
