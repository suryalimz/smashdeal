<?php
namespace System\View\Cell;

use Cake\View\Cell;

/**
 * PostBodyRenderer cell
 */
class PostBodyRendererCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($post)
    {
        $defaultlang = $this->request->session()->read('Setting')['SITEDFLTLG_VALUE'];
        foreach($post['langs'] as $lang)
        {
            if($lang['languageid']==$defaultlang)
            {
                $body = $lang['body'];
            }
        }
        if(!isset($body))
        {
            $body = $post['body'];
        }
        $plugins = $this->setPlugin($body);
        $this->set('body',$body);
        $this->set('plugins',$plugins);
    }

    private function setPlugin($body)
    {
        preg_match_all("/\[START PLUGIN\](.*?)\[END PLUGIN\]/", $body, $plugins);
        return $plugins;
    }

    public function plugin($plugin)
    {
        $temp = explode("|",$plugin);
        $name = trim($temp[0]);
        $args = [];
        if(trim($temp[1])!=""){
            $argStrings = explode(",",$temp[1]);
            foreach($argStrings as $argString)
            {
                $temp2 = explode(":",$argString);
                $args[$temp2[0]] = $temp2[1];
            }
        }
        $this->set('pluginname',$name);
        $this->set('args',$args);
    }

}
