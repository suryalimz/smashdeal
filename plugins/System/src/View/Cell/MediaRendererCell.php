<?php
namespace System\View\Cell;

use Cake\View\Cell;

/**
 * MediaRenderer cell
 */
class MediaRendererCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($mediaId = null, $plain = false)
    {
        $this->set('plain',$plain);
        if($mediaId!=null){
            $this->loadModel('System.TbSysPosts');
            $media = $this->TbSysPosts->get($mediaId,['contain'=>['Metas']]);
            $this->set('media',$media);
        }
        else {
            $this->set('media', null);
        }
    }
}
