<?php
namespace System\View\Cell;

use Cake\View\Cell;

/**
 * WidgetRenderer cell
 */
class WidgetRendererCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($widgetgroup = null, $attribute = null)
    {
        $this->loadModel('System.TbSysWidgetGroups');
        $group = $this->TbSysWidgetGroups->find('all')
            ->contain(['Widgets'])
            ->where(['isadmin'=>false,'moduleid'=>$this->request->session()->read("Setting")["THEME"],'code'=>$widgetgroup])->first();
        $this->set('group',$group);
        $this->set('attribute',$attribute);
    }
}
