<?php
namespace System\View\Cell;

use Cake\View\Cell;

/**
 * CustomLink cell
 */
class CustomLinkCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($widget)
    {
        $this->set('widget',$widget);
    }

    public function configForm($menu,$index)
    {
        $this->set('menu',$menu);
        $this->set('index',$index);
    }

    public function menuRenderer($widget,$menu)
    {
        $this->set('menu',$menu);
        $this->set('widget',$widget);
    }
}
