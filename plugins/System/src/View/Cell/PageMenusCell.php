<?php
namespace System\View\Cell;

use Cake\View\Cell;
use System\Controller\Component\PagesComponent;

/**
 * PageMenus cell
 */
class PageMenusCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($widget)
    {
        $pagescomponent = new PagesComponent();
        $this->loadModel('System.TbSysPosts');
        $tmppages = $this->TbSysPosts->find('all',['contain'=>'Langs'])->where(['and'=>['posttype'=>1,'publishstatus'=>3]]);
        $pages = $pagescomponent->collectPageNested($tmppages->toArray());
        $this->loadModel('System.TbSysMenus');
        $menu = $this->TbSysMenus->find('all')->where(['code'=>'STATICPAGE'])->first();

        $this->set('menu',$menu->id);
        $this->set('pages',$pages);
        $this->set('widget',$widget);
    }

    public function configForm($menu,$index)
    {
        $this->loadModel('System.TbSysPosts');
        $page = $this->TbSysPosts->get($menu["config"]["pageid"],['contain'=>'Langs']);
        $this->set('page',$page);
        $this->set('menu',$menu);
        $this->set('index',$index);
    }

    public function menuRenderer($widget,$menu)
    {
        $this->loadModel('System.TbSysMenus');
        $this->loadModel('System.TbSysPosts');
        if($menu['config']["isoriginal"]=="true") {
            $page = $this->TbSysPosts->get($menu["config"]["pageid"],['contain'=>'PostLangs']);
            $this->set('page',$page);
        }
        $menuobj = $this->TbSysMenus->get($menu["menuid"]);
        $this->set('menuobj',$menuobj);
        $this->set('menu',$menu);
        $this->set('widget',$widget);
    }
}
