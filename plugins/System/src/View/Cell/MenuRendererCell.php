<?php
namespace System\View\Cell;

use Cake\View\Cell;
use System\Controller\Component\PagesComponent;

/**
 * MenuRenderer cell
 */
class MenuRendererCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($locator = null,$ulclass = null)
    {
        $this->loadModel('System.TbSysMenuLocators');
        $this->loadModel('System.TbSysMenuGroups');
        if($locator!=null)
        {
            $locator = $this->TbSysMenuLocators->find('all')->where(['code'=>$locator])->first();
            $menugroupid =  $locator->menugroupid;
            if($menugroupid!=null)
            {
                $menugroup = $this->TbSysMenuGroups->get($menugroupid,['contain'=>'MenuDetails']);
                $this->set('menugroup',$menugroup);
            }
            else{
                $pagescomponent = new PagesComponent();
                $this->loadModel('System.TbSysPosts');
                $tmppages = $this->TbSysPosts->find('all',['contain'=>'Langs'])->where(['and'=>['posttype'=>1,'publishstatus'=>3]]);
                $pages = $pagescomponent->collectPageNested($tmppages->toArray());
                $this->set('pages',$pages);
            }
        }
        $this->set('ulclass',$ulclass);
    }

    public function singleMenu($menu = null)
    {
        $this->loadModel('System.TbSysWidgets');
        $widget = $this->TbSysWidgets->get($menu['widgetid']);
        $this->set('widget',$widget);
        $this->set('menu',$menu);
    }
}