<?php
/**
 * Created by PhpStorm.
 * User: zeno
 * Date: 24/10/16
 * Time: 19:56
 */

namespace System\Form;


use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class ReadingSettingForm extends Form
{
    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField("POSTPAGE.id","string")
            ->addField("POSTPAGE.value","integer");
    }

    protected function _buildValidator(Validator $validator)
    {
        $validator->notEmpty('DFTCATE.id',__d("system","Invalid Form"));
        $validator->notEmpty('DFTCATE.value',__d("system","Default Category can't be empty"));
        return $validator;
    }

    protected function _execute(array $data)
    {
        return true;
    }
}