<?php
/**
 * Created by PhpStorm.
 * User: zeno
 * Date: 24/10/16
 * Time: 20:40
 */

namespace System\Form;


use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class MediaSettingForm extends Form
{
    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField("THUMWIDTH.id","string")
            ->addField("THUMWIDTH.value","integer")
            ->addField("THUMHEIGHT.id","string")
            ->addField("THUMHEIGHT.value","integer")
            ->addField("THUMCROP.id","string")
            ->addField("THUMCROP.value","integer")
            ->addField("MIDHEIGHT.id","string")
            ->addField("MIDHEIGHT.value","integer")
            ->addField("MIDWIDTH.id","string")
            ->addField("MIDWIDTH.value","integer")
            ->addField("LRGHEIGHT.id","string")
            ->addField("LRGHEIGHT.value","integer")
            ->addField("LRGWIDTH.id","string")
            ->addField("LRGWIDTH.value","integer");
    }

    protected function _buildValidator(Validator $validator)
    {
        $validator->notEmpty('THUMWIDTH.id',__d("system","Invalid Form"));
        $validator->notEmpty('THUMWIDTH.value',__d("system","Thumbnail Width can't be empty"));
        $validator->notEmpty('THUMHEIGHT.id',__d("system","Invalid Form"));
        $validator->notEmpty('THUMHEIGHT.value',__d("system","Thumbnail Height can't be empty"));
        $validator->notEmpty('THUMCROP.id',__d("system","Invalid Form"));
        $validator->notEmpty('THUMCROP.value',__d("system","Thumbnail Crop can't be empty"));
        $validator->notEmpty('MIDHEIGHT.id',__d("system","Invalid Form"));
        $validator->notEmpty('MIDHEIGHT.value',__d("system","Medium Height can't be empty"));
        $validator->notEmpty('MIDWIDTH.id',__d("system","Invalid Form"));
        $validator->notEmpty('MIDWIDTH.value',__d("system","Medium Width can't be empty"));
        $validator->notEmpty('LRGHEIGHT.id',__d("system","Invalid Form"));
        $validator->notEmpty('LRGHEIGHT.value',__d("system","Large Height can't be empty"));
        $validator->notEmpty('LRGWIDTH.id',__d("system","Invalid Form"));
        $validator->notEmpty('LRGWIDTH.value',__d("system","Large Width can't be empty"));
        return $validator;
    }

    protected function _execute(array $data)
    {
        return true;
    }


}