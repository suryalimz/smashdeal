<?php

namespace System\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class UserPreferenceForm extends Form{

	protected function _buildSchema(Schema $schema)
	{
		return $schema
			->addField("ADMDFLTLG.id","string")
			->addField("ADMDFLTLG.value","string")
			->addField("ADMTHEME.id","string")
			->addField("ADMTHEME.value","string");
	}

	protected function _buildValidator(Validator $validator)
	{
		$validator->notEmpty('ADMDFLTLG.id',__d("system","Invalid Form"));
		$validator->notEmpty('ADMDFLTLG.value',__d("system","Please choose interface language"));
		$validator->notEmpty('ADMTHEME.id',__d("system","Invalid Form"));
		$validator->notEmpty('ADMTHEME.value',__d("system","Please choose color schema"));
		return $validator;
	}

	protected function _execute(array $data)
	{
		return true;
	}
	
}