<?php

namespace System\Auth;

use Cake\Auth\BaseAuthorize;
use Cake\Network\Request;
use Cake\ORM\TableRegistry;

class IdeaAuthorize extends BaseAuthorize
{

	public function authorize($user, Request $request)
	{
		if($this->isAnonymousAccess($request))
		{
			return true;
		}

		if($this->isAuthenticateAccess($request))
		{
			return true;
		}

        return $this->isAuthorize($user,$request);
	}

	public function isAuthorize($user, Request $request)
	{
		foreach($user["role"]["access_menus"] as $menu)
		{
			if($menu["controller"] == $request["controller"] && $menu["action"] == $request["action"] && $menu["plugin"] == $request["plugin"] && $menu["prefix"] == $request["prefix"])
			{
				return true;
			}
			foreach($menu["auth_refs"] as $ref)
			{
				if($ref["controller"] == $request["controller"] && $ref["action"] == $request["action"] && $ref["plugin"] == $request["plugin"] && $ref["prefix"] == $request["prefix"])
				{
					return true;
				}
			}
		}
		return false;
	}

	public function isAnonymousAccess(Request $request)
	{
		$this->TbSysMenus = TableRegistry::get('System.TbSysMenus');
		$menus = $this->TbSysMenus->find('all')->where(["and"=>[
				"controller" => $request["controller"],
				"action" => $request["action"],
				"plugin" => $request["plugin"],
				"prefix" => $request["prefix"],
				"authtype" => 3
			]]);
		return count($menus->toArray())>0;
	}

	public function isAuthenticateAccess(Request $request)
	{
		$this->TbSysMenus = TableRegistry::get('System.TbSysMenus');
		$menus = $this->TbSysMenus->find('all')->where(["and"=>[
				"controller" => $request["controller"],
				"action" => $request["action"],
				"plugin" => $request["plugin"],
				"prefix" => $request["prefix"],
				"authtype" => 2
			]]);
		return count($menus->toArray())>0;
	}	

}