<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysMenuRights Model
 *
 * @method \System\Model\Entity\TbSysMenuRight get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysMenuRight newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysMenuRight[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuRight|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysMenuRight patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuRight[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuRight findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysMenuRightsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_menu_rights');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('System.TbSysMenus',['foreignKey'=>'menuid']);

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->requirePresence('displayname', 'create')
            ->notEmpty('displayname');

        $validator
            ->requirePresence('menuid', 'create')
            ->notEmpty('menuid');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
