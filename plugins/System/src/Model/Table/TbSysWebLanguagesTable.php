<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysWebLanguages Model
 *
 * @method \System\Model\Entity\TbSysWebLanguage get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysWebLanguage newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysWebLanguage[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysWebLanguage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysWebLanguage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysWebLanguage[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysWebLanguage findOrCreate($search, callable $callback = null)
 */
class TbSysWebLanguagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_web_languages');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id');

        $validator
            ->allowEmpty('languageid');

        return $validator;
    }
}
