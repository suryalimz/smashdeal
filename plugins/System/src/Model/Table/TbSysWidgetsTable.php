<?php
namespace System\Model\Table;

use Cake\Database\Schema\Table as Schema;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Database\Type;
Type::map('json','Cake\Database\Type\JsonType');
/**
 * TbSysWidgets Model
 *
 * @method \System\Model\Entity\TbSysWidget get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysWidget newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysWidget[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysWidget|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysWidget patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysWidget[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysWidget findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysWidgetsTable extends Table
{
    protected function _initializeSchema(Schema $table)
    {
        $table->columnType('config','json');
        return $table;
    }


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_widgets');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->belongsToMany('Groups',
        [
            'className'=>'System.TbSysWidgetGroups',
            'joinTable'=>'tb_sys_widget_group_details',
            'foreignKey'=>'widgetid',
            'targetForeignKey'=>'groupid',
            'sort'=>['TbSysWidgetGroupDetails.index'=>'ASC'],
            'saveStrategy'=>'replace'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('displayname', 'create')
            ->notEmpty('displayname');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('pluginname');

        $validator
            ->allowEmpty('component');

        $validator
            ->requirePresence('moduleid', 'create')
            ->notEmpty('moduleid');

        $validator
            ->boolean('isadmin')
            ->allowEmpty('isadmin');

        $validator
            ->integer('widgettype')
            ->requirePresence('widgettype', 'create')
            ->notEmpty('widgettype');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
