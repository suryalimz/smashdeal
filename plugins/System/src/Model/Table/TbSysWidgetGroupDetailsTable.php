<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysWidgetGroupDetails Model
 *
 * @method \System\Model\Entity\TbSysWidgetGroupDetail get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysWidgetGroupDetail newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysWidgetGroupDetail[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysWidgetGroupDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysWidgetGroupDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysWidgetGroupDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysWidgetGroupDetail findOrCreate($search, callable $callback = null)
 */
class TbSysWidgetGroupDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_widget_group_details');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Uuid');

        $this->belongsTo('Widgets',['className'=>'System.TbSysWidgets','foreignKey'=>'widgetid']);
        $this->belongsTo('System.TbSysWidgetGroups',['foreignKey'=>'groupid']);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('groupid');

        $validator
            ->allowEmpty('widgetid');

        $validator
            ->integer('index')
            ->allowEmpty('index');

        $validator
            ->allowEmpty('config');

        return $validator;
    }
}
