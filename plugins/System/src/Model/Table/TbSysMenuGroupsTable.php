<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysMenuGroups Model
 *
 * @method \System\Model\Entity\TbSysMenuGroup get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysMenuGroup newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysMenuGroup[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuGroup|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysMenuGroup patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuGroup[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenuGroup findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysMenuGroupsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_menu_groups');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');

        $this->belongsTo('MenuLocators',['className'=>'System.TbSysMenuLocators','foreignKey'=>'menugroupid']);
        $this->hasMany('MenuDetails',['className'=>'System.TbSysMenuGroupDetails','foreignKey'=>'groupid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
