<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysUserPreferences Model
 *
 * @method \System\Model\Entity\TbSysUserPreference get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysUserPreference newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysUserPreference[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysUserPreference|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysUserPreference patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysUserPreference[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysUserPreference findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysUserPreferencesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_user_preferences');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('User',[
                'className'=>'System.TbSysUsers',
                'foreignKey'=>"userid"
            ]);

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('label', 'create')
            ->notEmpty('label');

        $validator
            ->requirePresence('value', 'create')
            ->notEmpty('value');

        $validator
            ->allowEmpty('moduleid');

        $validator
            ->allowEmpty('userid');

        $validator
            ->allowEmpty('groupname');

        $validator
            ->integer('valuetype')
            ->allowEmpty('valuetype');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
