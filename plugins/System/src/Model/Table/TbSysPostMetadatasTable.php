<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysPostMetadatas Model
 *
 * @method \System\Model\Entity\TbSysPostMetadata get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysPostMetadata newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysPostMetadata[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysPostMetadata|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysPostMetadata patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysPostMetadata[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysPostMetadata findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysPostMetadatasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_post_metadatas');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');


        $this->belongsTo('Post',['System.TbSysPosts','foreignKey'=>'postid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('postid');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('value');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
