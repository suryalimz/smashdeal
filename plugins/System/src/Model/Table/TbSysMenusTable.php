<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysMenus Model
 *
 * @method \System\Model\Entity\TbSysMenu get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysMenu newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysMenu[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenu|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysMenu patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenu[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysMenu findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysMenusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_menus');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('ChildrenMenus',['className'=>'System.TbSysMenus','foreignKey'=>'parentid']);
        
        $this->belongsTo('ParentMenu',['className'=>'System.TbSysMenus','foreignKey'=>'parentid']);

        $this->belongsToMany('SubMenus',['className'=>'System.TbSysMenus','joinTable'=>'tb_sys_sub_menus','foreignKey'=>'menuid','targetForeignKey'=>'submenuid']);

        $this->belongsTo('Modules',['className'=>'System.TbSysModules','foreignKey'=>'moduleid']);

        $this->belongsTo('Themes',['className'=>'System.TbSysThemes','foreignKey'=>'moduleid']);

        $this->hasMany('AuthRefs',['className'=>'System.TbSysMenus','foreignKey'=>'authref']);

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('label');

        $validator
            ->allowEmpty('icon');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('keyword');

        $validator
            ->integer('linktype')
            ->allowEmpty('linktype');

        $validator
            ->integer('rendertype')
            ->allowEmpty('rendertype');

        $validator
            ->allowEmpty('extraconfig');

        $validator
            ->boolean('isneedextra')
            ->allowEmpty('isneedextra');

        $validator
            ->integer('level')
            ->allowEmpty('level');

        $validator
            ->integer('index')
            ->allowEmpty('index');

        $validator
            ->boolean('isadmin')
            ->allowEmpty('isadmin');

        $validator
            ->boolean('isajax')
            ->allowEmpty('isajax');

        $validator
            ->boolean('isactive')
            ->allowEmpty('isactive');

        $validator
            ->allowEmpty('parentid');

        $validator
            ->allowEmpty('controller');

        $validator
            ->allowEmpty('action');

        $validator
            ->allowEmpty('plugin');

        $validator
            ->allowEmpty('prefix');

        $validator
            ->allowEmpty('url');

        $validator
            ->allowEmpty('moduleid');

        $validator
            ->integer('authtype')
            ->requirePresence('authtype', 'create')
            ->notEmpty('authtype');

        $validator
            ->allowEmpty('authref');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
