<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysUsers Model
 *
 * @method \System\Model\Entity\TbSysUser get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysUser newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysUser[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysUser|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysUser[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysUser findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_users');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->hasMany('Preferences',[
            "className"=>"System.TbSysUserPreferences",
            "foreignKey"=>"userid"]
        );

        $this->belongsTo('Profile',
            ['className'=>'System.TbSysProfiles','foreignKey'=>'profileid']);

        $this->belongsTo('Role',['className'=>'System.TbSysUserRoles','foreignKey'=>'userroleid']);

        $this->belongsToMany('DashboardComponents',[
            'className'=>'System.TbSysDashboardComponents',
            'joinTable'=>'tb_sys_dashboard_authorizations',
            'foreignKey'=>'userid',
            'targetForeignKey'=>'dashboardcomponentid',
            'saveStrategy'=>'replace',
            'dependent'=>true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username');

        $validator
            ->requirePresence('email','create')
            ->add('email', 'validFormat', [
                    'rule' => 'email',
                    'message' => __d('system',"Email is not valid")
                ])
            ->notEmpty('email');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->requirePresence('confirm_password','create')
            ->notEmpty('confirm_password')
            ->add('confirm_password','same_as_password',['rule'=>function($value,$context){
                    if (isset($context['data']['password']) && $value == $context['data']['password']) {
                        return true;
                    }
                    return false;
                },'message'=>'confirm password and password are not match']);

        $validator
            ->boolean('isactive')
            ->allowEmpty('isactive');

        $validator
            ->notEmpty('profileid');

        $validator
            ->requirePresence('userroleid', 'create')
            ->notEmpty('userroleid');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']),['message'=>__d('system',"Username has been used")]);
        $rules->add($rules->isUnique(['email']),['message'=>__d('system',"Email has been used")]);
        return $rules;
    }

    public function findStatus($query,array $options)
    {
        $s = strtolower($options['s']);
        $status = (strtolower($s)=="active")?TRUE:FALSE;
        $query->where(['isactive'=>$status]);
        return $query;
    }

    public function findName($query,array $options)
    {
        $s = strtolower($options['s']);
        $status = $options["status"];
        $query->contain('Profile');
        if($status==null)
            $query->where([
                'or'=>[
                    ['lower(username) LIKE'=>'%'.$s.'%'],
                    ['lower(email) LIKE'=>'%'.$s.'%'],
                    ['lower(Profile.displayname) LIKE'=>'%'.$s.'%'],
                    ['lower(Profile.fullname) LIKE'=>'%'.$s.'%'],
                    ['lower(Profile.firstname) LIKE'=>'%'.$s.'%'],
                    ['lower(Profile.lastname) LIKE'=>'%'.$s.'%']
                ]]);
        else{
            $bools = (strtolower($status)=="active")?TRUE:FALSE;
            $query->where(
                [
                'AND'=>
                    ['or'=>[
                        ['lower(username) LIKE'=>'%'.$s.'%'],
                        ['lower(email) LIKE'=>'%'.$s.'%'],
                        ['lower(Profile.displayname) LIKE'=>'%'.$s.'%'],
                        ['lower(Profile.fullname) LIKE'=>'%'.$s.'%'],
                        ['lower(Profile.firstname) LIKE'=>'%'.$s.'%'],
                        ['lower(Profile.lastname) LIKE'=>'%'.$s.'%']
                    ]],
                    'isactive'=>$bools
                ]);
        }
        return $query;
    }

    public function findAuth(Query $query,array $options)
    {
        $query->contain(["Role"=>function($q)
            {
                return $q->contain(["AccessMenus"=>function($q2)
                    {
                        return $q2->contain('AuthRefs');
                    },"MenuRights"]);
            },'Profile'])->where(['TbSysUsers.isactive'=>TRUE]);
        return $query;
    }
}
