<?php
namespace System\Model\Table;

use Cake\Database\Schema\Table as Schema;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Database\Type;
use Cake\Database\Type\JsonType;
Type::map('json','Cake\Database\Type\JsonType');
/**
 * TbSysTerms Model
 *
 * @method \System\Model\Entity\TbSysTerm get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysTerm newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysTerm[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysTerm|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysTerm patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysTerm[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysTerm findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysTermsTable extends Table
{

    protected function _initializeSchema(Schema $table)
    {
        $table->columnType('config','json');
        return $table;
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */


    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_terms');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');


        $this->hasMany('TermMetas',['className'=>'System.TbSysTermmetas','foreignKey'=>'termid','saveStrategy'=>'replace','dependent'=>true]);
        $this->hasMany('Langs',['className'=>'System.TbSysTermLangs','foreignKey'=>'termid','saveStrategy'=>'replace','dependent'=>true]);
        $this->hasMany('TermLangs',['className'=>'System.TbSysTermLangs','foreignKey'=>'termid','saveStrategy'=>'replace','dependent'=>true]);
        $this->hasMany('Children',['className'=>'System.TbSysTerms','foreignKey'=>'parentid','dependent'=>true]);
        $this->belongsTo('Parent',['className'=>'System.TbSysTerms','foreignKey'=>'parentid','dependent'=>true]);
        $this->hasMany('TbSysTermRelationships',['foreignKey'=>'termid','dependent'=>true]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('slug', 'create')
            ->notEmpty('slug');

        $validator
            ->requirePresence('taxonomy', 'create')
            ->notEmpty('taxonomy');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('parent');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedy');

        return $validator;
    }


    public function findType($query,array $options)
    {
        $type = $options['type'];
        $query->contain(['TermMetas','Parent']);
        $query->where(['TbSysTerms.taxonomy'=>$type]);
        return $query;
    }

    public function findStatus($query, array $options)
    {
        $s = strtolower($options['s']);
        $type = $options['type'];
        if(strtolower($s)=="active")
            $status = true;
        else
            $status = false;
        $query->contain('Langs');
        $query->where(['isactive'=>$status,'taxonomy'=>$type]);
        return $query;
    }

    public function findName($query, array $options)
    {
        $s = strtolower($options['s']);
        $status = $options["status"];
        $type = $options['type'];
        $defaultlang = $options["defaultlang"];
        $query->contain('Langs');
        if($status == null)
        {
            $query->innerJoinWith('Langs',function($q) use ($defaultlang){
                return $q->where(['languageid'=>$defaultlang]);
            })->where(
                [
                    'AND'=>[
                        [
                            'OR'=>["lower(Langs.name) LIKE"=>'%'.$s.'%',"lower(TbSysTerms.name) LIKE"=>'%'.$s."%"]
                        ],
                        'taxonomy'=>$type
                    ]
                ]
            );
        }
        else
        {
            if(strtolower($status)=="active")
            {
                $bools = true;
            }
            else{
                $bools =  false;
            }
            $query->innerJoinWith('Langs',function($q) use ($defaultlang){
                return $q->where(['languageid'=>$defaultlang]);
            })->where(
                [
                    'AND'=>[
                        [
                            'OR'=>["lower(Langs.name) LIKE"=>'%'.$s.'%',"lower(TbSysTerms.name) LIKE"=>'%'.$s."%"]
                        ],
                        'taxonomy'=>$type,
                        'isactive'=>$bools
                    ]
                ]
            );
        }
        return $query;
    }
}
