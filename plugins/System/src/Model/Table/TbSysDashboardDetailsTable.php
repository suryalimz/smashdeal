<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysDashboardDetails Model
 *
 * @method \System\Model\Entity\TbSysDashboardDetail get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysDashboardDetail newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysDashboardDetail[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysDashboardDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysDashboardDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysDashboardDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysDashboardDetail findOrCreate($search, callable $callback = null)
 */
class TbSysDashboardDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_dashboard_details');
        $this->displayField('dashboardid');
        $this->primaryKey(['dashboardid', 'dashboardcomponentid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('dashboardid', 'create');

        $validator
            ->allowEmpty('dashboardcomponentid', 'create');

        $validator
            ->integer('pos')
            ->allowEmpty('pos');

        $validator
            ->integer('index')
            ->allowEmpty('index');

        return $validator;
    }
}
