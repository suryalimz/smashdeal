<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysTermmetas Model
 *
 * @method \System\Model\Entity\TbSysTermmeta get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysTermmeta newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysTermmeta[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysTermmeta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysTermmeta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysTermmeta[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysTermmeta findOrCreate($search, callable $callback = null)
 */
class TbSysTermmetasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_termmetas');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Uuid');

        $this->belongsTo('Term',['className'=>'TbSysTerms','dependent'=>true]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('termid', 'create')
            ->notEmpty('termid');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('value');

        return $validator;
    }
}
