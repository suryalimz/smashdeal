<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysUserRoles Model
 *
 * @method \System\Model\Entity\TbSysUserRole get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysUserRole newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysUserRole[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysUserRole|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysUserRole patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysUserRole[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysUserRole findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysUserRolesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_user_roles');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsToMany('AccessMenus',[
            'className' => 'System.TbSysMenus',
            'joinTable' => 'tb_sys_menu_accesses',
            'foreignKey' => 'userroleid',
            'targetForeignKey' => 'menuid',
            'saveStrategy'=>'replace'
        ]);

        $this->belongsToMany('MenuRights',[
            'className' => 'System.TbSysMenuRights',
            'joinTable' => 'tb_sys_menu_right_accesses',
            'foreignKey' => 'userroleid',
            'targetForeignKey' => 'menurightid',
            'saveStrategy'=>'replace'
        ]);

        $this->hasMany('System.TbSysUsers',['foreignKey'=>'userroleid']);

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('description');

        $validator
            ->boolean('isactive')
            ->allowEmpty('isactive');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

     public function findStatus($query,array $options)
    {
        $s = strtolower($options['s']);
        $status = (strtolower($s)=="active")?TRUE:FALSE;
        $query->where(['isactive'=>$status]);
        return $query;
    }

    public function findName($query,array $options)
    {
        $s = strtolower($options['s']);
        $status = $options["status"];
        if($status==null)
            $query->where(
                    ['lower(name) LIKE'=>'%'.$s.'%']
            );
        else{
            $bools = (strtolower($status)=="active")?TRUE:FALSE;
            $query->where(
                [
                'AND'=>[
                    'lower(name) LIKE'=>'%'.$s.'%',
                    'isactive'=>$bools
                ]]);
        }
        return $query;
    }
}
