<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysInterfaceLanguages Model
 *
 * @method \System\Model\Entity\TbSysInterfaceLanguage get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysInterfaceLanguage newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysInterfaceLanguage[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysInterfaceLanguage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysInterfaceLanguage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysInterfaceLanguage[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysInterfaceLanguage findOrCreate($search, callable $callback = null)
 */
class TbSysInterfaceLanguagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_interface_languages');
        $this->displayField('id');
        $this->primaryKey(['id', 'languageid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('languageid', 'create');

        return $validator;
    }
}
