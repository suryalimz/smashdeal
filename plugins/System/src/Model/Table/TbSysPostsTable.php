<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysPosts Model
 *
 * @method \System\Model\Entity\TbSysPost get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysPost newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysPost[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysPost|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysPost patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysPost[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysPost findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysPostsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_posts');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->hasMany('Metas',['className'=>'System.TbSysPostMetadatas','foreignKey'=>'postid','saveStrategy'=>'replace','dependent'=>true]);
        $this->hasMany('Langs',['className'=>'System.TbSysPostLangs','foreignKey'=>'postid','saveStrategy'=>'replace','dependent'=>true]);
        $this->hasMany('PostLangs',['className'=>'System.TbSysPostLangs','foreignKey'=>'postid','saveStrategy'=>'replace','dependent'=>true]);
        $this->hasMany('Children',['className'=>'System.TbSysPosts','foreignKey'=>'parentid']);
        $this->belongsToMany('Terms',[
            'className'=>'System.TbSysTerms',
            'joinTable'=>'tb_sys_term_relationships',
            'foreignKey'=>'objectid',
            'targetForeignKey'=>'termid',
            'saveStrategy'=>'replace'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->integer('posttype')
            ->requirePresence('posttype', 'create')
            ->notEmpty('posttype');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->integer('publishstatus')
            ->allowEmpty('publishstatus');

        $validator
            ->integer('visibility')
            ->allowEmpty('visibility');

        $validator
            ->allowEmpty('postpassword');

        $validator
            ->allowEmpty('parentid');

        $validator
            ->integer('level')
            ->allowEmpty('level');

        $validator
            ->allowEmpty('permalink');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    public function findType($query,array $options)
    {
        $type = $options['type'];
        $query->contain('Langs');
        $query->where(['posttype'=>$type]);
        return $query;
    }

    public function findStatus($query,array $options)
    {
        $s = strtolower($options['s']);
        $type = $options['type'];
        if(strtolower($s)=="draft")
            $status = 1;
        elseif(strtolower($s)=="review")
            $status = 2;
        elseif(strtolower($s)=="publish")
            $status = 3;
        $query->contain('Langs');
        $query->where(['publishstatus'=>$status,'posttype'=>$type]);
        return $query;
    }

    public function findName($query,array $options)
    {
        $s = strtolower($options['s']);
        $status = $options["status"];
        $type = $options['type'];
        $defaultlang = $options["defaultlang"];
        $query->contain('Langs');
        if($status==null) {
            $query->innerJoinWith('Langs',function($q) use ($defaultlang)
            {
                return $q->where(["languageid"=>$defaultlang]);
            })->where(
                [
                    "AND"=>[
                        ["OR"=>["lower(Langs.title) LIKE"=>"%".$s."%",'lower(TbSysPosts.createdby) LIKE'=>"%".$s."%"]],
                        "posttype"=>$type
                    ]
                ]
            );
        }
        else{
            if(strtolower($status)=="draft")
                $bools = 1;
            elseif(strtolower($status)=="review")
                $bools = 2;
            elseif(strtolower($status)=="publish")
                $bools = 3;
            $query->innerJoinWith('Langs',function($q) use ($defaultlang)
            {
                return $q->where(["languageid"=>$defaultlang]);
            })->where(
                [
                    "AND"=>[
                        ["OR"=>["lower(Langs.title) LIKE"=>"%".$s."%",'lower(TbSysPosts.createdby) LIKE'=>"%".$s."%"]],
                        "posttype"=>$type,
                        'publishstatus'=>$bools
                    ]
                ]
            );
        }
        return $query;
    }

    public function findMediaMimeType($query, array $options)
    {
        $s = strtolower($options['s']);
        $mime = $options["mime"];
        $options["type"] = 3;
        $options["status"] = null;
        if($s!=null)
        {
            if(isset($mime) && $mime!="")
            {
                $query = $this->findName($query,$options);
                $query->where(["AND"=>[
                    "postmime IN"=>$this->getMimeList($mime)
                ]]);
            }
            else
            {
                $query = $this->findName($query,$options);
            }
        }
        else
        {
            if(isset($mime) && $mime!="")
            {
                $query = $this->findType($query,$options);
                $query->where(["AND"=>[
                    "postmime IN"=>$this->getMimeList($mime)
                ]]);
            }
            else{
                $query = $this->findType($options);
            }
        }
        return $query;
    }

    private function getMimeList($mime)
    {
        if($mime == "image")
            return ["image/gif","image/jpeg","image/png"];
        else if($mime == "video")
            return ["video/mp4","video/ogg","video/x-matroska"];
        else if($mime == "audio")
            return ["audio/mp3","audio/ogg"];
        else
            return ["application/zip","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-powerpointapplication/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/x-rar-compressed","application/pdf","text/plain"];
    }
}