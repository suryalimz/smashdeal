<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysLookups Model
 *
 * @method \System\Model\Entity\TbSysLookup get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysLookup newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysLookup[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysLookup|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysLookup patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysLookup[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysLookup findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysLookupsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_lookups');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany("TbSysLookupDetails",['className'=>"System.TbSysLookupDetails","foreignKey"=>"lookupid"]);

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->requirePresence('moduleid', 'create')
            ->notEmpty('moduleid');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
