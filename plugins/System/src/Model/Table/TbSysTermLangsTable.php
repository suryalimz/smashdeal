<?php
namespace System\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbSysTermLangs Model
 *
 * @method \System\Model\Entity\TbSysTermLang get($primaryKey, $options = [])
 * @method \System\Model\Entity\TbSysTermLang newEntity($data = null, array $options = [])
 * @method \System\Model\Entity\TbSysTermLang[] newEntities(array $data, array $options = [])
 * @method \System\Model\Entity\TbSysTermLang|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \System\Model\Entity\TbSysTermLang patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysTermLang[] patchEntities($entities, array $data, array $options = [])
 * @method \System\Model\Entity\TbSysTermLang findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbSysTermLangsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_sys_term_langs');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->belongsTo('Terms',['className'=>'System.TbSysTerms','dependent'=>true,'foreignKey'=>'termid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('languageid', 'create')
            ->notEmpty('languageid');

        $validator
            ->allowEmpty('termid', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
