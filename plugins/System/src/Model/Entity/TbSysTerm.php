<?php
namespace System\Model\Entity;

use Cake\ORM\Entity;

/**
 * TbSysTerm Entity
 *
 * @property string $id
 * @property string $name
 * @property string $slug
 * @property string $taxonomy
 * @property string $description
 * @property string $parent
 * @property string $createdby
 * @property \Cake\I18n\Time $created
 * @property string $modifiedy
 * @property \Cake\I18n\Time $modified
 */
class TbSysTerm extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
