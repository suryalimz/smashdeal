<?php
namespace System\Model\Entity;

use Cake\ORM\Entity;

/**
 * TbSysPost Entity
 *
 * @property string $id
 * @property int $posttype
 * @property \Cake\I18n\Time $date
 * @property int $publishstatus
 * @property int $visibility
 * @property string $postpassword
 * @property string $parentid
 * @property int $level
 * @property string $permalink
 * @property \Cake\I18n\Time $created
 * @property string $createdby
 * @property \Cake\I18n\Time $modified
 * @property string $modifiedby
 */
class TbSysPost extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
