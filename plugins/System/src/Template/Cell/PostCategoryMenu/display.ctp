<div class="col-md-12 icms-box no-padding">
    <div class="hidden" id="postcategorylinkid" data-value="<?= $menu ?>"></div>
    <div class="page-wrapper min-padding-top">
        <?php

        function generatedCategoryList($index = 0,$list,$defaultlng)
        {
            echo "<ul class='tree-list' id='".$index."-list'>";
            foreach($list as $l)
            {
                $label = "";
                foreach($l["term_langs"] as $lang){
                    if($lang["languageid"]==$defaultlng)
                    {
                        $label = $lang["name"];
                    }
                }
                echo "<li><input id='".$l->id."' type='checkbox' name='terms[][id]' value='".$l->id."' data-label='".$label."' data-slug='".$l->slug."' data-category='".$l->id."' /><label>&nbsp;".$label."</label>";
                $index++;
                if(!empty($l->children))
                {
                    generatedCategoryList($index,$l->children,$defaultlng);
                }
                echo "</li>";
            }
            echo "</ul>";
        }
        $defaultlang = $this->request->session()->read('Config')["SITEDFLTLG_VALUE"];
        generatedCategoryList(0,$categories,$defaultlang);
        ?>
    </div>
</div>
<div class="button-group">
    <button id="postcategorylinkadd" class="btn btn-default btn-sm pull-right icms-apply-btn" data-target="<?= $widget->id ?>" data-component-target="#<?= $widget->id ?>-form-component">
        <?= __d("system","Add to Menu") ?>
    </button>
</div>