<ul class="<?=  $ulclass ?>">
    <?php if(isset($menugroup)): ?>
        <?php foreach($menugroup["menu_details"] as $detail): ?>
            <li><?= $this->cell('System.MenuRenderer::singleMenu',['menu'=>$detail]) ?></li>
        <?php endforeach; ?>
    <?php elseif(isset($pages)): ?>
        <?php foreach($pages as $page): ?>
            <li><?= $this->cell('System.MenuRenderer::singlePageMenu',['page'=>$page]) ?></li>
        <?php endforeach; ?>
    <?php endif; ?>
</ul>