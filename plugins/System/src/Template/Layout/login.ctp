<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?= __d('system',"Sign In") ?> - <?= $config["SITETITLE"] ?></title>
	<meta content="width=device-width, intial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One|Quintessential" rel="stylesheet">
	<?php
		echo $this->AssetCompress->css('System.eDd2XSD');
        echo $this->fetch('css');
	?>
</head>
<body class="login">
	<?= $this->fetch('content') ?>
</body>
<?php
    echo $this->fetch('footer-script');
?>
</html>