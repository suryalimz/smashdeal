<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?= __d('system',"iCMS") ?> - <?= $config["SITETITLE"] ?></title>
	<meta content="width=device-width, intial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<?php
		echo $this->AssetCompress->css('System.eDd2XSD');
        echo $this->AssetCompress->script('System.eDd2XSD');
        echo $this->fetch('css');

        ini_set('intl.default_locale', $config["ADMDFLTLG"]);
	?>
</head>
<body class="hold-transition <?= $config["ADMTHEME"] ?> fixed sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
	      	<a href="index2.html" class="logo">
	        	<span class="logo-mini"><b>i</b></span>
	        	<span class="logo-lg"><b>Idea</b>CMS</span>
	      	</a>
		    <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                   <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span>
                                    <?= $this->request->session()->read('Auth')['User']['profile']['fullname']; ?>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header">
                                    <?php if(isset($this->request->session()->read('Auth')['User']['profile']['profilepic'])): ?>
                                        <?= $this->Html->image(DS."files".DS.$this->request->session()->read('Auth')['User']['profile']['profilepic'],['class'=>'img-circle','alt'=>'User Image']); ?>
                                    <?php else: ?>
                                        <?= $this->Html->image('System.user-2.png',['class'=>'img-circle','alt'=>'User Image']); ?>
                                    <?php endif; ?>
                                    <p>
                                        <?= $this->Html->link($this->request->session()->read('Auth')['User']["profile"]['fullname'],["controller"=>"Users","action"=>"profile","prefix"=>"Backend","plugin"=>"System",$this->request->session()->read('Auth')['User']['id']],['class'=>'ilink']); ?>
                                        <small><?= $this->request->session()->read('Auth')['User']["username"]; ?></small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        
                                    </div>
                                    <div class="pull-right">
                                        <?= $this->Html->link(__d('system',"Sign Out"),
                                                ['controller'=>'Users','action'=>'logout','prefix'=>'Backend','plugin'=>'System'],
                                                ['class'=>'btn btn-primary']); ?>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            &nbsp;
                        </li>
                    </ul>
                </div>
            </nav>
	    </header>
    	<aside class="main-sidebar">
    		<?= $this->cell('System.Sidebar'); ?>
    	</aside>
 		<div class="content-wrapper">
 			<?= $this->Flash->render('auth'); ?>
      		<?= $this->Flash->render(); ?>
	 		<section class="content-header clearfix">
				<h1 class="col-md-6"><?= $active["label"]; ?></h1>
				<ul class="pull-right sub-menu">
					<?php foreach($active->sub_menus as $submenu): ?>
						<li>
							<?= $this->element('System.singlelink',['menu'=>$submenu]); ?>
						</li>	
					<?php  endforeach; ?>
				</ul>
			</section>
			<section class="content">
				<?= $this->fetch('content'); ?>
			</section>
 		</div>
	</div>
    <div class="footer-script">

    </div>
    <?php
        echo $this->fetch('footer-script');
    ?>
</body>
</html>