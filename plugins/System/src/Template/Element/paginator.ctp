<div class="pull-right">
    <div class="pull-left" style="padding-right: 5px">
        <span><?= $this->Paginator->counter(['format'=>'pages']) ?></span>
    </div>
    <div class="btn-group">
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->next() ?>
    </div>
</div>