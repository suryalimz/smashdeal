<div id="<?= $menu["id"] ?>" class="widget-box draggable clearfix">
    <div class="widget-title">
        <h3><?= $menu["config"]["label"] ?></h3>
        <div class="widget-action">
            <a href="">
                <span class="info"><?= $menu["source"] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-down"></i></span>
            </a>
        </div>
    </div>
    <div class="widget-inside clearfix" style="display:none">
        <div class="hidden">
            <label for="">id</label><input class="id" name="menu_details[<?= $index ?>][id]" value="<?= $menu["id"] ?>" type="text"/><br>
            <label for="i">index</label><input class="index" name="menu_details[<?= $index ?>][index]" value="<?= $menu["index"] ?>" type="text"/><br>
            <label for="">source</label><input name="menu_details[<?= $index ?>][source]" value="<?= $menu["source"] ?>" type="text"/><br>
            <label for="">menutype</label><input name="menu_details[<?= $index ?>][menutype]" value="<?= $menu["menutype"] ?>" type="text"/><br>
            <label for="">widgetid</label><input name="menu_details[<?= $index ?>][widgetid]" value="<?= $menu["widgetid"] ?>" type="text"/><br>
            <label for="">parentid</label><input class="parentid" name="menu_details[<?= $index ?>][parentid]" value="<?= isset($menu["parentid"])?$menu["parentid"]:"" ?>" type="text"/><br>
            <label for="">extraconfig</label>
            <?php if($menu["extraconfig"]!="" && count($menu["extraconfig"])>0): ?>
                <?php foreach($menu["extraconfig"] as $key=>$extra): ?>
                    <input name="menu_details[<?= $index ?>][extraconfig][<?= $key ?>]" value="<?= isset($extra)?$extra:'' ?>" type="text"/><br>
                <?php endforeach; ?>
            <?php else: ?>
                <input name="menu_details[<?= $index ?>][extraconfig]" value="" type="text"/><br>
            <?php endif; ?>
            <label for="">menuid</label><input name="menu_details[<?= $index ?>][menuid]" value="<?= isset($menu["menuid"])?$menu["menuid"]:"" ?>" type="text"/><br>
        </div>
        <?php if($menu["configForm"]!=""): ?>
            <?= $this->cell($menu['configForm'],['menu'=>$menu,'index'=>$index]); ?>
        <?php else: ?>
            <div class="form-group">
                <label for="" class="col-md-12">
                    <?= __d("system","Navigation Menu Label") ?>
                </label>
                <div class="col-md-12">
                    <input type="text" class="form-control" name="menu_details[<?= $index ?>][config][label]" value="<?= isset($menu["config"]["label"])?$menu["config"]["label"]:"" ?>" />
                </div>
            </div>
        <?php endif; ?>
        <div class="padding-top">
            <div class="pull-left">
                <a href="#" class="remove"><?= __d("system","Delete") ?></a>|
                <a href="#" class="close-box"><?= __d("system","Close") ?></a>
            </div>
        </div>
    </div>
</div>