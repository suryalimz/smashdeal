<?php 
	if($menu->rendertype == 1){
		$label = "<i class='fa ".$menu->icon."'></i><span>".$menu->label."</span>";
	}
	elseif($menu->rendertype == 2){
		$label = "<span>".$menu->label."</span>";
	}
	elseif($menu->rendertype == 3){
		$label = "<i class='fa ".$menu->icon."'></i>";
	}

	if($menu->linktype==2)
	{
		$url = $menu->url;
	}
	else
	{
		if(isset($menu["controller"]))
			$url["controller"] = $menu["controller"];

		if(isset($menu["action"]))
			$url["action"] = $menu["action"];

		if(isset($menu["plugin"]))
			$url["plugin"] = $menu["plugin"];

		if(isset($menu["prefix"]))
			$url["prefix"] = $menu["prefix"];
		
		if($menu->isneedextra)
			if(isset($extradata))
				$url = array_merge($url,$extradata);
	}	
	$config["escape"]=false;
	if($menu->rendertype==3)
	{
		$config["class"] = 'btn btn-default';
	}

	if(isset($menu->extraconfig)){
		if(array_key_exists("class", $menu->extraconfig))
		{
			$config["class"] = $config["class"]." ".$menu->extraconfig["class"];
			unset($menu->extraconfig["class"]);
		}
		array_merge($config,$menu->extraconfig);
	}

	echo $this->Html->link($label,$url,$config);
?>