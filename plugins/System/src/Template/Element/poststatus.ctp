<?= $this->AssetCompress->script('System.Post234234',['block'=>'footer-script']); ?>
<div class="statusbar">
    <i class="fa fa-key"></i>
    <span><?= __d("system","status : "); ?></span>
    <input type="hidden" id="publishstatus" name="publishstatus" value="<?= $bean->publishstatus ?>"/>
    <strong id="printedstatus">
        <?php
        if($bean->publishstatus == 1)
            echo __d("system","Draft");
        else if($bean->publishstatus == 2)
            echo __d("system","Review Editor");
        else if($bean->publishstatus == 3)
            echo __d("system","Published");
        ?>
    </strong>
    <a href="#" id="editStatus"><?= __d("system","Edit"); ?></a>
    <?php
    $status = [1=>__d("system","Draft"),2=>__d("system","Review Editor"),3=>__d("system","Published")];
    ?>
    <select id="statusTemplate" class="hidden form-control input-sm">
        <?php for($i=1;$i<=3;$i++): ?>
            <option value="<?= $i ?>"><?= $status[$i]; ?></option>
        <?php endfor; ?>
    </select>
</div>