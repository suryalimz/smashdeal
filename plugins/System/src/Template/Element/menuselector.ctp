<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div id="menuselector" data-href="<?= $this->Url->build(['controller'=>'Menus','action'=>'renderMenuSelector','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
    <?php foreach($menu_selector_widgets as $widget): ?>
        <div id="<?= $widget->id ?>" class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading-<?= $widget->id ?>">
                <h4 class="panel-title">
                    <a href="#collapse-<?= $widget->id ?>" role="button" data-toggle="collapse" data-parent="#accordion" aria-control="collapse-<?= $widget->id ?>">
                        <?= $widget->displayname ?>
                    </a>
                </h4>
            </div>
            <div id="collapse-<?= $widget->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?= $widget->id ?>">
                <div class="panel-body">
                    <div class="link-list">
                        <div class="hidden" id="<?= $widget->id ?>-form-component" data-value="<?= (isset($widget->config["configForm"]))?$widget->config["configForm"]:"" ?>">
                        </div>
                        <?= $this->Cell($widget->pluginname.".".$widget->component,['widget'=>$widget]) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>