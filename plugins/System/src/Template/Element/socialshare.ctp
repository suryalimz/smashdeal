<?= $this->AssetCompress->script('System.Social333',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('System.Social333',['block'=>'css']) ?>
<div class="box box-primary social">
    <div class="box-header">
        <h2 class="box-title"><?= __d("system","Social Share Button") ?></h2>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label class="col-md-3 control-label">
                <?= __d("system","Enable Share Button") ?>
            </label>
            <div class="col-md-5">
                <input type="hidden" name="metas[<?= $index ?>][name]" value="share_button_enable"/>
                <input type="hidden" name="metas[<?= $index ?>][value]" value="disable"/>
                <input type="checkbox" name="metas[<?= $index ?>][value]" value="enable" <?= ($bean["metas"][$index]["value"]=="enable")?"checked":"" ?>/>
            </div>
        </div>
    </div>
</div>