<?= $this->AssetCompress->script('System.FeatureImage',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('System.FeatureImage',['block'=>'css']) ?>
<?= $this->Html->script('System.media/featureimage',['block'=>'footer-script']); ?>
<div class="box box-default">
    <div class="box-header">
        <h2 class="box-title"><?= __d("system","Feature Image") ?></h2>
    </div>
    <div class="box-body">
        <div id="get-image-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'getMedia','prefix'=>'Ajax','plugin'=>'System']); ?>"></div>
        <input type="hidden" name="metas[<?= $index ?>][name]" value="feature_image"/>
        <?php
            $image = '';
            if(isset($bean["metas"])) {
                foreach ($bean["metas"] as $meta) {
                    if($meta["name"]=="feature_image")
                    {
                        $image = $meta["value"];
                    }
                }
            }
        ?>
        <input type="hidden" id="feature-image" name="metas[<?= $index ?>][value]" value="<?= $image ?>"/>
        <div id="image-wrapper">
            <?php
                if($image!=""){
                    echo $this->cell('System.MediaRenderer',['mediaId'=>$image]);
                }
            ?>
        </div>
        <div>
            <div id="media-list-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'mediaList','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
            <div id="media-upload-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'upload','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
            <div id="media-attribute-link" data-href="<?= $this->Url->build(['controller'=>'Medias','action'=>'mediaAttribute','prefix'=>'Ajax','plugin'=>'System']) ?>"></div>
            <a href="#" id="media-selector"><?= __d("system","feature image"); ?></a>
        </div>
    </div>
</div>