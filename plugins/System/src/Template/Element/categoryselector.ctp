<?= $this->AssetCompress->script('System.Cate2349892',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('System.Cate2349892',['block'=>'css']) ?>
<div class="nav-tabs-custom box">
    <div class="box-header">
        <h2 class="box-title"><?= __d("system","Categories"); ?></h2>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <ul class="nav nav-tabs nav-sm-tabs">
            <li class="active"><a href="#allcat" data-toggle="tab"><?= __d("system","All Categories"); ?></a></li>
            <li><a href="#mostused" data-toggle="tab"><?= __d("system","Most Used") ?></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="allcat">
                <div class="category-wrapper min-padding-top">
                    <?php
                        function isChecked($bean,$id,$dftcate)
                        {
                            if(isset($bean->terms)) {
                                foreach ($bean->terms as $term) {
                                    if ($term["id"] == $id) {
                                        return true;
                                    }
                                }
                            }
                            if($bean->id==null || !isset($bean->id))
                                if($id==$dftcate)
                                    return true;

                            return false;
                        }
                        function generatedList($bean, $index = 0,$list,$config)
                        {
                            echo "<ul class='tree-list' id='".$index."-list'>";
                            foreach($list as $l)
                            {
                                echo "<li><input id='".$l->id."' type='checkbox' name='terms[][id]' value='".$l->id."' ".((isChecked($bean,$l->id,$config["DFTCATE"]))?"checked":"")."/><label>&nbsp;".$l->name."</label>";
                                $index++;
                                if(!empty($l->children))
                                {
                                    generatedList($bean,$index,$l->children,$config);
                                }
                                echo "</li>";
                            }
                            echo "</ul>";
                        }
                        generatedList($bean, 0,$allcategories,$config);
                    ?>
                </div>
            </div>
            <div class="tab-pane" id="mostused">
                <?php if(count($mostcategories)>0): ?>
                    <ul class="tree-list" id="mostused">
                        <?php foreach($mostcategories as $c): ?>
                            <li><input id="clone-<?= $c["id"] ?>" type="checkbox" class="checkbox" data-target="#<?= $c['id'] ?>" <?= ((isChecked($bean,$c["id"],$config["DFTCATE"]))?"checked":"") ?>/><label><?= $c["name"]; ?></label></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>