<?= $this->AssetCompress->script('System.Page34234',['block'=>'footer-script']); ?>
<?= $this->Html->link('',['controller'=>'Pages','action'=>'getPossibleParent','plugin'=>'System','prefix'=>'Ajax'],['id'=>'getparent','class'=>'hidden']) ?>
<div class="form-group">
    <label class="col-md-12">
        <?= __d("system","Parent : ") ?>
    </label>
    <div class="col-md-12">
        <input type="hidden" id="pagelevel" name="level" value="<?= $bean->level ?>"/>
        <select name="parentid" id="postparent" class="form-control input-sm" value="<?= $bean->parentid ?>">
            <option value="" level="0"><?= __d("system","(no parent)"); ?></option>
        </select>
    </div>
</div>