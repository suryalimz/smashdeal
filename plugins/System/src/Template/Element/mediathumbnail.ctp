<?php
    $filename = "";
    foreach($bean->metas as $meta) {
        if ($meta["name"] == "file_path") {
            $filename = $meta["value"];
            break;
        }
    }
    ?>

<?php
    if(preg_match("/(mp4|ogv|mkv)$/",$filename)){
        $image = "video.png";
    }
    elseif(preg_match("/(mp3|oga)$/",$filename)) {
        $image = "audio.png";
    }
    elseif(preg_match("/(xls?x)$/",$filename)) {
        $image = "excel.png";
    }
    elseif(preg_match("/(doc?x|ppt?x|pdf)$/",$filename)){
        $image =  "word.png";
    }
    elseif(preg_match("/(zip|rar|7z)$/",$filename)){
        $image =  "compressed.png";
    }
    elseif(preg_match("/(gif|jpe?g|png)$/",$filename)){
        $image = DS."files".DS."thumbnail".DS.$filename;
    }
    else {
        $file = "file.png";
    }
?>

<?= $this->Html->image($image,['class'=>'img media-wrap','width'=>'100%','id'=>$bean->id]) ?>