<?= $this->AssetCompress->css('System.Role36dd',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Role36dd',['block'=>'footer-script']) ?>
<?= $this->Form->create($userRole,['class'=>'form-horizontal']); ?>
<div class="box box-default">
	<div class="box-header">
		<h3 class="box-title"><?= __d('system',"Remove User Role") ?></h3>
	</div>
	<div class="box-body">
		<div class="form-group">
			<label class="col-md-2 control-label"><?= __d('system',"User Role") ?></label>
			<div class="col-md-4">
				<?= $this->Form->input('name',['div'=>false,'label'=>false,'readonly'=>true,'class'=>'form-control']) ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label"><?= __d('system',"Replacement Role") ?></label>
			<div class="col-md-4">
				<?= $this->Form->input('userroleid',['div'=>false,'label'=>false,'type'=>'hidden','id'=>'userroleid']) ?>
				<div id="roleLov" 
					data-value="<?= (!empty($user->userroleid))?$user->userroleid:"" ?>"
					data-dynamic-source="<?= $this->Url->build(['controller'=>'UserRoles','action'=>'dynamicView','plugin'=>'System','prefix'=>'Ajax']) ?>"
					data-source="<?= $this->Url->build(['controller'=>'UserRoles','action'=>'getActiveUserRole','plugin'=>'System','prefix'=>'Ajax']) ?>"></div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<input type="submit" value="<?= __d('system',"Execute") ?>" class="btn btn-danger"/>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>