<?= $this->AssetCompress->css('System.Role34dd',['block'=>'css']); ?>
<?= $this->AssetCompress->script('System.Role34dd',['block'=>'footer-script']) ?>
<?php
	$allAmount = count($allUserRoles->toArray());
	$activeAmount = 0;
	$inactiveAmount = 0;
	foreach($allUserRoles as $mdl){
		if($mdl["isactive"]==true)
			$activeAmount++;
		else
			$activeAmount++;
	} 
?>
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">
			<small>
				<strong>
					<?php if($status==null): ?>
						<?= "All (".$allAmount.")" ?>
					<?php else: ?>
						<?php if(isset($s)): ?>
							<?= $this->Html->link("All (".$allAmount.")",['controller'=>'UserRoles','action'=>'search','plugin'=>'System','prefix'=>'Backend',"s"=>$s]); ?>
						<?php else: ?>
							<?= $this->Html->link("All (".$allAmount.")",['controller'=>'UserRoles','action'=>'index','plugin'=>'System','prefix'=>'Backend']); ?>
						<?php endif; ?>
					<?php endif; ?>
					|
					<?php if($status=="active"): ?>
						<?= "Active (".$activeAmount.")" ?>
					<?php else: ?>
						<?php if(isset($s)): ?>
							<?= $this->Html->link("Active (".$activeAmount.")",['controller'=>'UserRoles','action'=>'search','plugin'=>'System','prefix'=>'Backend',"active","s"=>$s]); ?>
						<?php else: ?>
							<?= $this->Html->link("Active (".$activeAmount.")",['controller'=>'UserRoles','action'=>'index','plugin'=>'System','prefix'=>'Backend',"active"]); ?>
						<?php endif; ?>
					<?php endif; ?>
					|
					<?php if($status=="inactive"): ?>
						<?= "Inactive (".$inactiveAmount.")" ?>
					<?php else: ?>
						<?php if(isset($s)): ?>
							<?= $this->Html->link("Inactive (".$inactiveAmount.")",['controller'=>'UserRoles','action'=>'search','plugin'=>'System','prefix'=>'Backend',"inactive","s"=>$s]); ?>
						<?php else: ?>
							<?= $this->Html->link("Inactive (".$inactiveAmount.")",['controller'=>'UserRoles','action'=>'index','plugin'=>'System','prefix'=>'Backend',"inactive"]); ?>
						<?php endif; ?>
					<?php endif; ?>
				</strong>
			</small>
	    </h3>
	    <div class="box-tools col-md-4 no-padding">
            <?= $this->Form->create($userRole,['url'=>['action'=>'search',$status],'type'=>'get']) ?>
            <div class="input-group input-group-sm">
                <input value="<?= (isset($s))?$s:''; ?>" name="s" placeholder="<?= __d('system',"Search...") ?>" class="form-control"/>
                <span class="input-group-btn">
                    <button type="submit" class="btn">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </div>
            <?= $this->Form->end(); ?>
        </div>
	</div>
	<div class="box-body">
		<?= $this->Form->create($userRole,['url'=>['action'=>'bulkAction'],'id'=>'bulkActionForm']); ?>
		<div class="list-action clearfix">
			<div class="col-md-2 no-padding">
				<select id="bulk-action" name="action" class="form-control i-combo">
					<option value=""><?= __d('system',"Bulk Action") ?></option>
					<option value="1"><?= __d('system',"Activate") ?></option>
					<option value="2"><?= __d('system',"Non-Activate") ?></option>
				</select>
			</div>
			<div class="col-md-2 no-padding">
				<button class="btn btn-default" class="btn-bulk-action">
					<?= __d('system',"Apply") ?>
				</button>
			</div>
			<div class="pull-right item-amount">
				<?php
					$label = ($allAmount>=2)?" items":" item";
					echo $allAmount	 . $label;
				?>
			</div>
		</div>
		<table class="table">
			<thead>
				<tr>
					<th class="cb-wrap" width="5%"><input type="checkbox" class="checkbox-custom" id="checkall" /></th>
					<th width="20%"><?= __d('system',"Role Name") ?></th>
					<th width="10%"><?= __d('system',"Status") ?></th>
					<th width="40%"><?= __d('system',"Description") ?></th>
					<th width="9%" class="text-center"><?= __d('system',"Action") ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($userRoles)>0): ?>
					<?php foreach($userRoles as $role): ?>
						<tr>
							<td class="cb-wrap">
								<input type="checkbox" name="value[]" value="<?= $role["id"] ?>" class="cb-item checkbox-custom checkbox-child" data-value="<?= $role["id"] ?>"/>
							</td>
							<td>
								<?= $role->name ?>
							</td>
							<td>
								<input type="checkbox" class="cbreadonly" <?= ($role["isactive"])?"checked":"" ?> readonly/>
							</td>
							<td>
								<?= $role->description ?>
							</td>
							<td>
								<d	iv class="btn-group btn-group-sm">
									<?= $this->Html->link("<i class='fa fa-edit'></i>",['controller'=>'UserRoles','action'=>'modify','plugin'=>'System','prefix'=>'Backend',$role["id"]],['class'=>'btn btn-default btn-sm','escape'=>false]) ?>
									<?= $this->Html->link("<i class='fa fa-trash'></i>",['controller'=>'UserRoles','action'=>'remove','plugin'=>'System','prefix'=>'Backend',$role["id"]],['class'=>'btn btn-danger btn-sm','escape'=>false]) ?>
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    <span class="caret"></span>
									    <span class="sr-only">Toggle Dropdown</span>
								  	</button>
								  	<ul class="dropdown-menu dropdown-menu-right">
								  		<li>
								  		 	<a href="#" class="quick-prev" data-href="<?= $this->url->build(['controller'=>'UserRoles','action'=>'dynamicView','plugin'=>'System','prefix'=>'Ajax',$role['id']]); ?>">
								  		 		<?= __d('system',"Quick Preview") ?>
								  		 	</a>
								  		</li>
									    <li>
											<?= $this->Html->link(__d('system',"Preview"),['controller'=>'UserRoles','action'=>'view','plugin'=>'System','prefix'=>'Backend',$role["id"]]); ?>
									    </li>
									    <li>
									    	<?php if(!$role["isactive"]): ?>
												<?= $this->Html->link(__d('system',"Activate"),['controller'=>'UserRoles','action'=>'changeStatus','plugin'=>'System','prefix'=>'Backend','active',$role["id"]]); ?>
											<?php else: ?>
												<?= $this->Html->link(__d('system',"Inactivate"),['controller'=>'UserRoles','action'=>'changeStatus','plugin'=>'System','prefix'=>'Backend','inactive',$role["id"]]); ?>
											<?php endif; ?>
									    </li>
								  	</ul>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="5">
							<?php 
								if(isset($s))
									echo __d('system',"No user role found for ''{0}'' keyword.",[$s]);
								else
									echo __d('system',"No user role avaiable."); 
							?>
						</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<?= $this->Form->end() ?>
	</div>
	<div class="box-footer">
		<?= $this->element('System.paginator'); ?>
	</div>
</div>