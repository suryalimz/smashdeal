<?= $this->AssetCompress->css('System.Role37dd',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Role37dd',['block'=>'footer-script']) ?>
<?= $this->Form->create($userRole,['class'=>'form-horizontal']); ?>
<div class="box box-default">
	<div class="box-header">
		<h3 class="box-title">
			<?= __d('system',"User Role Data") ?>
		</h3>
	</div>
	<div class="box-body">
		<div class="form-group">
			<label class="control-label col-md-2">
				<?= __d('system',"Name") ?>
			</label>
			<div class="col-md-4">
				<?= $this->Form->input('name',['class'=>'form-control','div'=>false,'label'=>false,'readonly'=>true,'placeholder'=>__d('system',"Administrator, Editor")]) ?>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">
				<?= __d('system',"Description") ?>
			</label>
			<div class="col-md-6">
				<?= $this->Form->input('description',['class'=>'form-control','div'=>false,'label'=>false,'readonly'=>true,'placeholder'=>__d('system',"Hold all module's accesss")]) ?>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<label>
					<?= $this->Form->input('isactive',['div'=>false,'label'=>false,'class'=>'cb-readonly']) ?>
				</label>
				<label><?= __d('system',"Active") ?></label>	
			</div>
		</div>
	</div>
</div>
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">
			<?= __d('system',"Menu Access Detail") ?>
		</h3>
		<div class="box-tools">
			<?php
			$amount = count($userRole->access_menus);
			$label = ($amount>2)?__d("system","items"):__d("system","item");
			echo $amount." ".$label;
			?>
		</div>
	</div>
	<div class="box-body no-padding">
		<table id="table-container" class="table">
			<thead>
				<tr>
					<th width="250px">
						<?= __d('system',"Name") ?>
					</th>
					<th>
						<?= __d('system',"Description") ?>
					</th>
					<th width="60px">
						<?= __d('system',"Status") ?>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php if(isset($userRole->access_menus)): ?>
					<?php foreach($userRole->access_menus as $key=>$value): ?>
						<tr id="item-<?= $key ?>" class="item">
							<td>
								<input type="hidden" class="uid" name="access_menus[][id]" value="<?= $value->id ?>"/>
								<input type='text' class='form-control' value='<?= $value->name ?>' readonly/>
							</td>
							<td>
								<span><?= $value->description ?></span>
							</td>
							<td>
								<input type="checkbox" class="cb-readonly" <?= ($value->isactive)?"checked":""; ?> />
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?> 
			</tbody>
		</table>
	</div>
</div>
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">
			<?= __d('system',"Menu Right Access Detail") ?>
		</h3>
		<div class="box-tools">
			<?php
			$amount = count($userRole->menu_rights);
			$label = ($amount>2)?__d("system","items"):__d("system","item");
			echo $amount." ".$label;
			?>
		</div>
	</div>
	<div class="box-body no-padding">
		<table id="table-container-right" class="table">
			<thead>
				<tr>
					<th width="250px">
						<?= __d('system',"Name") ?>
					</th>
					<th>
						<?= __d('system',"Description") ?>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php if(isset($userRole->menu_rights)): ?>
					<?php foreach($userRole->menu_rights as $key=>$value): ?>
						<tr id="itemright-<?= $key ?>" class="item-right">
							<td>
								<input type="hidden" class="uid" name="menu_rights[][id]" value="<?= $value->id ?>"/>
								<input type='text' class='form-control' value='<?= $value->displayname ?>' readonly/>
							</td>
							<td>
								<span><?= $value->description ?></span>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?> 
			</tbody>
		</table>
	</div>
</div>
<?= $this->Form->end(); ?>