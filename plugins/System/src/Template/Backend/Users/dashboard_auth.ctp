<?= $this->AssetCompress->script('System.Dash456',['block'=>'footer-script']); ?>
<?= $this->AssetCompress->css('System.Dash456',['block'=>'css']); ?>
<?= $this->Form->create($user,['class'=>'form-horizontal']); ?>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<div class="box box-primary">
	<div class="box-header">
		<h2 class="box-title"><?= __d("system","Dashboard Authentication") ?></h2>
	</div>
	<div class="box-body">
		<div id="lovDashboard" data-source="<?= $this->Url->build(['controller'=>'Dashboards','action'=>'getActiveDashboard','plugin'=>'System','prefix'=>'Ajax']) ?>"></div>
		<div>
			<button class="btn btn-danger pull-right" id="btnBulkDelete">
				<i class="fa fa-trash"></i>
			</button>
		</div>
		<table id="table-container" class="table">
			<thead>
				<tr>
					<th class="cb-wrap" width="3%">
						<input type="checkbox" class="checkbox-custom" id="checkall" />
					</th>
					<th width="250px">
						<?= __d('system',"Name") ?>
					</th>
					<th>
						<?= __d('system',"Description") ?>
					</th>
					<th width="30px">						
					</th>
				</tr>
			</thead>
			<tbody>
				<?php if(isset($user->dashboard_components)): ?>
					<?php foreach($user->dashboard_components as $key=>$value): ?>
						<tr id="item-<?= $key ?>" class="item">
							<td>
								<input type='checkbox' class='checkbox-child'/>
							</td>
							<td>
								<input type="hidden" class="uid" name="dashboard_components[][id]" value="<?= $value->id ?>"/>
								<input type='text' class='form-control' value='<?= $value->name ?>' readonly/>
							</td>
							<td>
								<span><?= $value->note ?></span>
							</td>
							<td>
								<button class='btn btn-danger btn-delete'>
									<i class='fa fa-trash'></i>
								</button>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?> 
			</tbody>
		</table>
	</div>
</div>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<?= $this->Form->end(); ?>