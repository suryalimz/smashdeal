<?= $this->AssetCompress->css('System.Use55dds',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Use55dds',['block'=>'footer-script']) ?>
<?= $this->Form->create($user,['class'=>'form-horizontal']); ?>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header">
				<h3 class="box-title"><?= __d('system',"User Data") ?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label class="col-md-2 control-label"><?= __d('system',"Username") ?></label>
					<div class="col-md-4">
						<?= $this->Form->input('username',['div'=>false,'label'=>false,'class'=>'form-control','placeholder'=>__d('system',"username")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label"><?= __d('system',"Email") ?></label>
					<div class="col-md-4">
						<?= $this->Form->input('email',['div'=>false,'label'=>false,'class'=>'form-control','placeholder'=>__d('system',"user@mail.com")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label"><?= __d('system',"User Role") ?></label>
					<div class="col-md-4">
						<?= $this->Form->input('userroleid',['div'=>false,'label'=>false,'type'=>'hidden','id'=>'userroleid']) ?>
						<div id="roleLov" 
							data-value="<?= (!empty($user->userroleid))?$user->userroleid:"" ?>"
							data-dynamic-source="<?= $this->Url->build(['controller'=>'UserRoles','action'=>'dynamicView','plugin'=>'System','prefix'=>'Ajax']) ?>"
							data-source="<?= $this->Url->build(['controller'=>'UserRoles','action'=>'getActiveUserRole','plugin'=>'System','prefix'=>'Ajax']) ?>"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-2"></div>
					<div class="col-md-4">
						<label>
							<?= $this->Form->input('isactive',['div'=>false,'label'=>false]) ?>
						</label>
						<label><?= __d('system',"Active") ?></label>	
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-2"></div>
					<div class="col-md-4">
						<label>
							<?= $this->Form->input('issuperadmin',['div'=>false,'label'=>false]) ?>
						</label>
						<label><?= __d('system',"Administrator") ?></label>
					</div>
				</div>
			</div>
		</div>
		<div class="box box-default">
			<div class="box-header">
				<h3 class="box-title">
					<?= __d('system',"Profile") ?>
				</h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label class="col-md-2 control-label">
						<?= __d('system',"First Name") ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('profile.firstname',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"first name")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">
						<?= __d('system',"Last Name") ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('profile.lastname',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"last name")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">
						<?= __d('system',"Full Name") ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('profile.fullname',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"full name")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">
						<?= __d('system',"Display Name") ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('profile.displayname',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"display name")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">
						<?= __d('system',"Birthday") ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('profile.birthday',['class'=>'form-control datepick','label'=>false,'div'=>false,'type'=>'text','placeholder'=>__d('system',"20-07-2015"),'value'=>(!empty($user->profile["birthday"]))?$user->profile["birthday"]->i18nFormat($config['SITEDTFORM']):""]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">
						<?= __d('system',"Address") ?>
					</label>
					<div class="col-md-10">
						<?= $this->Form->input('profile.address',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"address")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">
						<?= __d('system',"Website") ?>
					</label>
					<div class="col-md-10">
						<?= $this->Form->input('profile.website',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"website")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">
						<?= __d('system',"About me") ?>
					</label>
					<div class="col-md-10">
						<?= $this->Form->input('profile.aboutme',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"about me")]) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>