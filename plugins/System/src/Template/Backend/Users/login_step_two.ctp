<?php $this->extend("login"); ?>
<div class="login-form clearfix">
	<?= $this->Form->create(); ?>
	<div class="hidden">
		<?= $this->Form->input('phrase',['value'=>'two']);?>
		<?= $this->Form->input('username',['value'=>$user->username]); ?>
	</div>
	<div class="form-group clearfix">
		<div class="text-center">
			<div class="col-md-6 col-xs-6 col-sm-6 col-md-push-3 col-xs-push-3 col-sm-push-3">
				<?php if(empty($user->profile["profilepic"])): ?>
					<?= $this->Html->image('System.user-2.png',['class'=>'img img-circle','style'=>'width:100%']); ?>
				<?php else: ?>
					<?= $this->Html->image(DS."files".DS.$user["profile"]["profilepic"],["class"=>"img-circle img-thumbnail","style"=>"height:135px;width:100%"]); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="text-center">
			<strong><?= $user->profile["fullname"]; ?></strong>
		</div>
		<div class="text-center">
			<strong><?= $user->username; ?></strong>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12 col-xs-12 col-sm-12">
			<?= $this->Form->input('password',['div'=>false,'label'=>false,'class'=>'form-control','value'=>'','placeholder'=>__d('system','Enter your password'),'autofocus'=>true]) ?>
		</div>
		<?php if(isset($error)): ?>
			<div class="col-md-12 col-xs-12 col-sm-12 error-message">
				<?= $error; ?>
			</div>
		<?php endif; ?>
	</div>
	<div class="form-group">
		<div class="col-md-12 col-xs-12 col-sm-12">
			<button type="submit" class="btn btn-primary btn-login">
				<?= __d('system',"Sign In") ?>
			</button>
		</div>
	</div>
	<?= $this->Form->end(); ?>
</div>