<?= $this->AssetCompress->css('System.Use57dds',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Use57dds',['block'=>'footer-script']) ?>
<div class="box box-default">
	<div class="box-header">
		<h2 class="box-title"><?= __d("system","Change Password") ?></h2>
	</div>
	<div class="box-body">
		<?= $this->Form->create($user,['class'=>'form-horizontal','url'=>['action'=>'changePassword']]) ?>
        <div class="form-group">
            <label for="" class="col-md-4 control-label">
                <?= __d("smashdeal","Current Password") ?>
            </label>
            <div class="col-md-7">
                <?= $this->Form->input('password',['class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>__d("smashdeal","Current Password"),'value'=>'']) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-4 control-label">
                <?= __d("smashdeal","New Password") ?>
            </label>
            <div class="col-md-7">
                <?= $this->Form->input('new_password',['class'=>'form-control','type'=>'password','div'=>false,'label'=>false,'placeholder'=>__d("smashdeal","New Password"),'value'=>'']) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-4 control-label">
                <?= __d("smashdeal","Confirm Password") ?>
            </label>
            <div class="col-md-7">
                <?= $this->Form->input('confirm_password',['class'=>'form-control','type'=>'password','div'=>false,'label'=>false,'placeholder'=>__d("smashdeal","Confirm Password"),'value'=>'']) ?>
            </div>
        </div>
        <div class="button-group clearfix">
            <div class="col-md-6 col-md-push-3">
                <button type="submit" class="btn btn-smd btn-sm-primary"><?= __d("smahsdeal","Change Password") ?></button>
            </div>
        </div>
    <?= $this->Form->end() ?>
	</div>
</div>
<?= $this->Form->create($user,['class'=>'form-horizontal','type'=>'file']) ?>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<div class="row">
	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?= __d('system',"User Data") ?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label class="col-md-4 control-label"><?= __d('system',"Username") ?></label>
					<div class="col-md-4">
						<?= $this->Form->input('username',['class'=>'form-control','div'=>false,'label'=>false,'readonly'=>true]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?= __d('system',"Email") ?></label>
					<div class="col-md-4">
						<?= $this->Form->input('email',['div'=>false,'label'=>false,'class'=>'form-control','placeholder'=>__d('system',"user@mail.com")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?= __d('system',"Role") ?></label>
					<div class="col-md-4">
						<div class="input-group">
							<input type="text" class="form-control" value="<?= $user->role["name"] ?>"/>
							<span class="input-group-btn">
								<a href="#" class='btn btn-default quick-prev' id='role-quick-prev' data-href="<?= $this->Url->build(['controller'=>'UserRoles','action'=>'dynamicView','plugin'=>'System','prefix'=>'Ajax',$user->userroleid]) ?>">
									<i class="fa fa-eye"></i>
								</a>
							</span>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<div class="box box-default">
			<div class="box-header">
				<h3 class="box-title">
					<?= __d('system',"Profile Data") ?>
				</h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?= __d('system',"First Name") ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('profile.firstname',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"first name")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?= __d('system',"Last Name") ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('profile.lastname',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"last name")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?= __d('system',"Full Name") ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('profile.fullname',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"full name")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?= __d('system',"Display Name") ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('profile.displayname',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"display name")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?= __d('system',"Birthday") ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('profile.birthday',['class'=>'form-control datepick','label'=>false,'div'=>false,'type'=>'text','placeholder'=>__d('system',"20-07-2015"),'value'=>(!empty($user->profile["birthday"]))?$user->profile["birthday"]->i18nFormat($config['SITEDTFORM']):""]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?= __d('system',"Address") ?>
					</label>
					<div class="col-md-8">
						<?= $this->Form->input('profile.address',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"address")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?= __d('system',"Website") ?>
					</label>
					<div class="col-md-8">
						<?= $this->Form->input('profile.website',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"website")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?= __d('system',"About me") ?>
					</label>
					<div class="col-md-8">
						<?= $this->Form->input('profile.aboutme',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>__d('system',"about me")]) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">
					<?= __d('system',"Profile Picture"); ?>
				</h3>
			</div>
			<div class="box-body">
			<?php
					if(isset($user->profile["profilepic"]))
						echo $this->Html->image(DS."files".DS.$user["profile"]["profilepic"],["class"=>"img-circle img-thumbnail","style"=>"height:240px;width:100%"]);
					else
						echo $this->Html->image('System.user-2.png',['style'=>'width:100%']);
				?>
				<?= $this->Form->input('profile.profilepic',['class'=>'form-control','label'=>false,'div'=>false,'type'=>'file']) ?>
			</div>
		</div>
	</div>
</div>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<?= $this->Form->end(); ?>