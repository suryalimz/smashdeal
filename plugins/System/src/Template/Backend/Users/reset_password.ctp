<div class="box box-default">
	<div class="box-header">
		<h2 class="box-title">
			<?= __d("system","Reset Password") ?>
		</h2>
	</div>
	<div class="box-body">
		<?= $this->Form->create($user,['class'=>'form-horizontal']) ?>
		<div class="form-group">
			<label class="control-label col-md-2">
				<?= __d("system","Password") ?>
			</label>
			<div class="col-md-3">
				<?= $this->Form->input('password',['class'=>'form-control','label'=>false,'placeholder'=>'New Password','value'=>'']); ?>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">
				<?= __d("system","Confirm Password") ?>
			</label>
			<div class="col-md-3">
				<?= $this->Form->input('confirm_password',['class'=>'form-control','label'=>false,'placeholder'=>'New Password','type'=>'password']); ?>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-2"></div>
			<div class="col-md-2"><button type="submit" class="btn btn-primary"><?= __d("system","Reset Password") ?></button></div>
		</div>
		<?= $this->Form->end(); ?>
	</div>
</div>