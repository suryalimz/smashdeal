<?php $this->extend("login"); ?>
<div class="login-form clearfix">
	<?= $this->Form->create(); ?>
	<div class="hidden">
		<?= $this->Form->input('phrase',['value'=>'one']);?>
	</div>
	<div class="form-group">
		<div class="text-center">
			<div class="userimg col-md-6 col-xs-6 col-sm-6 col-md-push-3 col-xs-push-3 col-sm-push-3">
				<?= $this->Html->image('System.user-2.png',['class'=>'img img-circle','style'=>'width:100%']); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12 col-xs-12 col-sm-12">
			<?= $this->Form->input('username',['div'=>false,'label'=>false,'class'=>'form-control','placeholder'=>__d('system','Enter your username'),'autofocus'=>true]) ?>
		</div>
		<?php if(isset($error)): ?>
			<div class="col-md-12 col-xs-12 col-sm-12 error-message">
				<?= $error; ?>
			</div>
		<?php endif; ?>
	</div>
	<div class="form-group">
		<div class="col-md-12 col-xs-12 col-sm-12">
			<button type="submit" class="btn btn-primary btn-login">
				<?= __d('system',"Next") ?>
			</button>
		</div>
	</div>
	<?= $this->Form->end(); ?>
</div>