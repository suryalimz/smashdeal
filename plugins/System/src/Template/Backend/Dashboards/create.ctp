<?= $this->AssetCompress->script('System.Dash4555',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('System.Dash4555',['block'=>'css']) ?>
<?= $this->Form->create($dashboard,['class'=>'form-horizontal']); ?>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<div class="box box-default">
	<div class="box-header">
		<h3 class="box-title">
			<?= __d('system',"New Dashboard") ?>
		</h3>
	</div>
	<div class="box-body">
		<div class="form-group">
			<label for="" class="col-md-3 control-label">
				<?= __d("system","Name") ?>
			</label>
			<div class="col-md-4">
				<?= $this->Form->input('name',['class'=>'form-control','label'=>false]) ?>
			</div>
		</div>
		<div class="form-group">
			<label for="" class="col-md-3 control-label">
				<?= __d("system","Description") ?>
			</label>
			<div class="col-md-4">
				<?= $this->Form->input('userid',['div'=>false,'label'=>false,'type'=>'hidden','id'=>'userid']) ?>
				<?= $this->Form->input('description',['class'=>'form-control','label'=>false]) ?>
			</div>
		</div>
		<?php if($isMgr): ?>
			<div class="form-group">
				<label for="" class="col-md-3 control-label">
					<?= __d("system","Dashboard Owner") ?>
				</label>
				<div class="col-md-4">
					<div id="userLov" 
						data-value="<?= (!empty($dashboard->userid))?$dashboard->userid:"" ?>"
						data-source="<?= $this->Url->build(['controller'=>'Users','action'=>'getActiveUsers','plugin'=>'System','prefix'=>'Ajax']) ?>"></div>
				</div>
			</div>
		<?php endif; ?>
		<div class="col-md-12 hidden" id="toolbar" data-source="<?= $this->Url->build(['controller'=>'Dashboards','action'=>'getAuthorizedDashboard','plugin'=>'System','prefix'=>'Ajax']) ?>">
			<div id="lovDashboard"></div>
		</div>
		<div class="form-group col-md-8 col-md-push-2 hidden" id="dashboardwrap">
			<div class="row">
				<div class="col-md-6 min-padding-right">
					<h3 class="text-center cool-h3"><?= __d("system","Left Side Dashboard") ?></h3>
					<ul id="leftdashboard" class="connected emptylist dashboardcomp">
					</ul>
				</div>
				<div class="col-md-6 min-padding-left">
					<h3 class="text-center cool-h3"><?= __d("system","Right Side Dashboard") ?></h3>
					<ul id="rightdashboard" class="connected emptylist dashboardcomp">
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<?= $this->Form->end(); ?>