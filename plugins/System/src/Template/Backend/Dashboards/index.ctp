<?= $this->AssetCompress->script('System.Dashboar3445',['block'=>'footer-script']) ?>
<?php if(!empty($dashboards->toArray())): ?>
	<div class="col-md-12">
		<div class="dashboard-select">
			<div class="dropdown dropdown-select">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    <?= ($dashboard!=null)?$dashboard["name"]:__d("system","No Active Dashboard"); ?>
			    	<span class="caret"></span>
			  	</button>
			  	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<?php foreach($dashboards  as $key=>$dash): ?>
						<li><?= $this->Html->link($dash,["controller"=>"Dashboards","action"=>"index","plugin"=>"System","prefix"=>"Backend",$key]) ?></li>
					<?php endforeach; ?>
			  	</ul>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php if($dashboard!=null): ?>
	<input type="hidden" id="uid" value="<?= $dashboard["id"] ?>"/>
	<?= $this->Html->link('',['controller'=>'Dashboards','action'=>'updateDashboardPosition','plugin'=>'System','prefix'=>'Ajax'],['class'=>'hidden','id'=>'updatedash'])?>
	<div class="row">
		<div class="col-md-6 min-padding-right">
			<ul id="leftdashboard" class="connected emptylist">
				<?php foreach($dashboard->dashboard_components as $key=>$dash): ?>
					<?php if($dash["_joinData"]["pos"]==1): ?>
						<li id="<?= $dash["id"] ?>">
							<?= $this->cell($dash["plugin"].".".$dash["cell"]) ?>
						</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</div>
		<div class="col-md-6 min-padding-left">
			<ul id="rightdashboard" class="connected emptylist">
				<?php foreach($dashboard->dashboard_components as $key=>$dash): ?>
					<?php if($dash["_joinData"]["pos"]==2): ?>
						<li id="<?= $dash["id"] ?>">
							<?= $this->cell($dash["plugin"].".".$dash["cell"]) ?>
						</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
<?php else: ?>
	<?php if(empty($dashboards->toArray())): ?>
		<div class="dashlinkwrap">
			<?= $this->Html->link(__d("system","Create Dashboard"),["controller"=>"Dashboards","action"=>"create","plugin"=>"System","prefix"=>"Backend"],["class"=>"dashlink"]) ?>
		</div>
	<?php else: ?>
		<div class="dashlinkwrap">
			<?= $this->Html->link(__d("system","Manage Dashboard"),["controller"=>"Dashboards","action"=>"listDash","plugin"=>"System","prefix"=>"Backend"],["class"=>"dashlink"]) ?>
		</div>
	<?php endif; ?>
<?php endif; ?>