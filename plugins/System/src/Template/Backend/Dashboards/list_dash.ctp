<?= $this->AssetCompress->css('System.Dash5677',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Dash5677',['block'=>'footer-script']) ?>
<?php
	$allAmount = count($alldashboards->toArray());
	$activeAmount = 0;
	$inactiveAmount = 0;
	foreach($alldashboards as $mdl){
		if($mdl["isactive"]==true)
			$activeAmount++;
		else
			$inactiveAmount++;
	} 
?>
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">
			<small>
				<strong>
					<?php if($status==null): ?>
						<?= "All (".$allAmount.")" ?>
					<?php else: ?>
						<?php if(isset($s)): ?>
							<?= $this->Html->link("All (".$allAmount.")",['controller'=>'Dashboards','action'=>'search','plugin'=>'System','prefix'=>'Backend',"s"=>$s]); ?>
						<?php else: ?>
							<?= $this->Html->link("All (".$allAmount.")",['controller'=>'Dashboards','action'=>'listDash','plugin'=>'System','prefix'=>'Backend']); ?>
						<?php endif; ?>
					<?php endif; ?>
					|
					<?php if($status=="active"): ?>
						<?= "Active (".$activeAmount.")" ?>
					<?php else: ?>
						<?php if(isset($s)): ?>
							<?= $this->Html->link("Active (".$activeAmount.")",['controller'=>'Dashboards','action'=>'search','plugin'=>'System','prefix'=>'Backend',"active","s"=>$s]); ?>
						<?php else: ?>
							<?= $this->Html->link("Active (".$activeAmount.")",['controller'=>'Dashboards','action'=>'listDash','plugin'=>'System','prefix'=>'Backend',"active"]); ?>
						<?php endif; ?>
					<?php endif; ?>
					|
					<?php if($status=="inactive"): ?>
						<?= "Inactive (".$inactiveAmount.")" ?>
					<?php else: ?>
						<?php if(isset($s)): ?>
							<?= $this->Html->link("Inactive (".$inactiveAmount.")",['controller'=>'Dashboards','action'=>'search','plugin'=>'System','prefix'=>'Backend',"inactive","s"=>$s]); ?>
						<?php else: ?>
							<?= $this->Html->link("Inactive (".$inactiveAmount.")",['controller'=>'Dashboards','action'=>'listDash','plugin'=>'System','prefix'=>'Backend',"inactive"]); ?>
						<?php endif; ?>
					<?php endif; ?>
				</strong>
			</small>
		</h3>
		<div class="box-tools col-md-4 no-padding">
            <?= $this->Form->create($dashboard,['url'=>['action'=>'search',$status],'type'=>'get']) ?>
            <div class="input-group input-group-sm">
                <input value="<?= (isset($s))?$s:''; ?>" name="s" placeholder="<?= __d('system',"Search...") ?>" class="form-control"/>
                <span class="input-group-btn">
                    <button type="submit" class="btn">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </div>
            <?= $this->Form->end(); ?>
        </div>
	</div>
	<div class="box-body">
		<?= $this->Form->create($dashboard,['url'=>['action'=>'bulkAction'],'id'=>'bulkActionForm']); ?>
			<div class="list-action clearfix">
					<div class="col-md-2 no-padding">
						<select id="bulk-action" name="action" class="form-control i-combo">
							<option value=""><?= __d('system',"Bulk Action") ?></option>
							<option value="1"><?= __d('system',"Activate") ?></option>
							<option value="2"><?= __d('system',"Non-Activate") ?></option>
						</select>
					</div>
					<div class="col-md-2 no-padding">
						<button class="btn btn-default" type="submit" id="btn-bulk-action">
							<?= __d('system',"Apply") ?>
						</button>
					</div>
				<div class="pull-right item-amount">
					<?php
						$label = ($allAmount>=2)?" items":" item";
						echo $allAmount . $label;
					?>
				</div>
			</div>
			<table class="table">
				<thead>
					<tr>
						<th class="cb-wrap" width="3%">
							<input type="checkbox" class="checkbox-custom" id="checkall" />
						</th>
						<th width="25%"><?= __d('system',"Name") ?></th>
						<th><?= __d('system',"Description") ?></th>
						<?php if($isMgr): ?>
							<th width="15%"><?= __d("system","Owner") ?></th>
						<?php endif; ?>
						<th width="5%"><?= __d('system',"Status") ?></th>
						<th width="10%" class="text-center"><?= __d('system',"Action") ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($dashboards)>0): ?>
						<?php foreach($dashboards as $u): ?>
							<tr>
								<td class="cb-wrap">
									<input type="checkbox" name="value[]" value="<?= $u["id"] ?>" class="cb-item checkbox-custom checkbox-child" data-value="<?= $u["id"] ?>"/>
								</td>
								<td>
									<?= $u["name"] ?> <strong><?= ($u["isdefault"])?__d("system","[Default]"):""; ?></strong>
								</td>
								<td>
									<p><?= $u["description"] ?></p>
								</td>
								<?php if($isMgr): ?>
									<th width="15%"><?= $u["tb_sys_user"]["username"] ?></th>
								<?php endif; ?>
								<td style="text-align:center">
									<input type="checkbox" class="cbreadonly" <?= ($u["isactive"])?"checked":"" ?> readonly/>
								</td>
								<td style="text-align:center">
									<div class="btn-group btn-group-sm">
										<?= $this->Html->link("<i class='fa fa-edit'></i>",['controller'=>'Dashboards','action'=>'modify','plugin'=>'System','prefix'=>'Backend',$u["id"]],['class'=>'btn btn-default btn-sm','escape'=>false]) ?>
										<?= $this->Html->link("<i class='fa fa-trash'></i>",['controller'=>'Dashboards','action'=>'remove','plugin'=>'System','prefix'=>'Backend',$u["id"]],['class'=>'btn btn-default btn-sm btn-danger','escape'=>false]) ?>
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										    <span class="caret"></span>
										    <span class="sr-only">Toggle Dropdown</span>
									  	</button>
									  	<ul class="dropdown-menu dropdown-menu-right">
										    <li><?= $this->Html->link(__d('system',"Set Default"),['controller'=>'Dashboards','action'=>'setDefault','plugin'=>'System','prefix'=>'Backend',$u["id"]]); ?></li>
										    <li>
										    	<?php if(!$u["isactive"]): ?>
													<?= $this->Html->link(__d('system',"Activate"),['controller'=>'Dashboards','action'=>'changeStatus','plugin'=>'System','prefix'=>'Backend','active',$u["id"]]); ?>
												<?php else: ?>
													<?= $this->Html->link(__d('system',"Inactivate"),['controller'=>'Dashboards','action'=>'changeStatus','plugin'=>'System','prefix'=>'Backend','inactive',$u["id"]]); ?>
												<?php endif; ?>
										    </li>
									  	</ul>
									</div>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php else: ?>
						<tr>
							<td colspan="9">
								<?php 
									if(isset($s))
										echo __d('system',"No dashboard found for ''{0}'' keyword.",[$s]);
									else
										echo __d('system',"No dashboard avaiable."); 
								?>
							</td>
						</tr>
					<?php endif; ?>
				</tbody>
			</table>
		<?= $this->Form->end(); ?>
	</div>
</div>