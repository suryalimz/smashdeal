<?= $this->AssetCompress->script('System.Dash4555',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('System.Dash4555',['block'=>'css']) ?>
<?= $this->Form->create($dashboard,['class'=>'form-horizontal']); ?>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<div class="box box-default">
	<div class="box-header">
		<h3 class="box-title">
			<?= __d('system',"New Dashboard") ?>
		</h3>
	</div>
	<div class="box-body">
		<div class="form-group">
			<label for="" class="col-md-3 control-label">
				<?= __d("system","Name") ?>
			</label>
			<div class="col-md-4">
				<?= $this->Form->input('name',['class'=>'form-control','label'=>false]) ?>
			</div>
		</div>
		<div class="form-group">
			<label for="" class="col-md-3 control-label">
				<?= __d("system","Description") ?>
			</label>
			<div class="col-md-4">
				<?= $this->Form->input('userid',['div'=>false,'label'=>false,'type'=>'hidden','id'=>'userid']) ?>
				<?= $this->Form->input('description',['class'=>'form-control','label'=>false]) ?>
			</div>
		</div>
		<div class="col-md-12 hidden" id="toolbar" data-source="<?= $this->Url->build(['controller'=>'Dashboards','action'=>'getAuthorizedDashboard','plugin'=>'System','prefix'=>'Ajax']) ?>">
			<div id="lovDashboard"></div>
		</div>
		<div class="form-group col-md-8 col-md-push-2 hidden" id="dashboardwrap">
			<div class="row">
				<div class="col-md-6 min-padding-right">
					<h3 class="text-center cool-h3"><?= __d("system","Left Side Dashboard") ?></h3>
					<ul id="leftdashboard" class="connected emptylist dashboardcomp">
						<?php foreach($dashboard->dashboard_components as $key=>$dash): ?>
							<?php if($dash["_joinData"]["pos"]==1): ?>
								<li id="<?= $dash["id"] ?>">
									<input type="hidden" name="dashboard_components[<?= $key ?>][id]" class="uid" value="<?= $dash["id"] ?>"/>
									<input type="hidden" name="dashboard_components[<?= $key ?>][_joinData][pos]" class="pos" value="<?= $dash["_joinData"]["pos"] ?>"/>
									<input type="hidden" name="dashboard_components[<?= $key ?>][_joinData][index]" class="index" value="<?= $dash["_joinData"]["index"] ?>"/>
									<?= $dash["name"] ?>
									<div class='pull-right'>
										<button class='btn btn-sm btn-delete btn-danger'>
											<i class='fa fa-trash'></i>
										</button>
									</div>
								</li>
							<?php endif; ?>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="col-md-6 min-padding-left">
					<h3 class="text-center cool-h3"><?= __d("system","Right Side Dashboard") ?></h3>
					<ul id="rightdashboard" class="connected emptylist dashboardcomp">
						<?php foreach($dashboard->dashboard_components as $key=>$dash): ?>
							<?php if($dash["_joinData"]["pos"]==2): ?>
								<li id="<?= $dash["id"] ?>">
									<input type="hidden" name="dashboard_components[<?= $key ?>][id]" class="uid" value="<?= $dash["id"] ?>"/>
									<input type="hidden" name="dashboard_components[<?= $key ?>][_joinData][pos]" class="pos" value="<?= $dash["_joinData"]["pos"] ?>"/>
									<input type="hidden" name="dashboard_components[<?= $key ?>][_joinData][index]" class="index" value="<?= $dash["_joinData"]["index"] ?>"/>
									<?= $dash["name"] ?>
									<div class='pull-right'>
										<button class='btn btn-sm btn-delete btn-danger'>
											<i class='fa fa-trash'></i>
										</button>
									</div>
								</li>
							<?php endif; ?>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<?= $this->Form->end(); ?>