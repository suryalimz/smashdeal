<?= $this->AssetCompress->script('System.Media2343',['block'=>'footer-script']) ?>
<?= $this->AssetCompress->css('System.Media2343',['block'=>'css']) ?>
<div class="toolbar clearfix">
    <?= $this->Form->create($media,['class'=>'form-inline','url'=>['action'=>'search'],'type'=>'get']) ?>
        <div class="form-group min-padding">
            <?php $options = [''=>__d("system","All Medias"),'image'=>__d("system","Image"),"video"=>__d("system","Video"),"audio"=>__d("system","Audio"),"document"=>__d("system","Document")] ?>
            <?= $this->Form->select('mediatype',$options,['class'=>'form-control','value'=>(isset($mimetype)?$mimetype:"")]) ?>
        </div>
        <div class="form-group min-padding">
            <button class="btn btn-default" type="submit"><?= __d("system","filter") ?></button>
        </div>
        <div class="form-group min-padding pull-right">
            <?= $this->Form->input('s',['class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>'Search...','value'=>(isset($s))?$s:'']) ?>
        </div>
    <?= $this->Form->end() ?>
</div>
<div class="box box-default">
    <div class="box-body no-padding">
        <?= $this->Form->create($media,['url'=>['action'=>'bulkAction'],'id'=>'bulkActionForm']); ?>
        <div class="list-action clearfix">
            <div class="col-md-2 no-padding">
                <select id="bulk-action" name="action" class="form-control i-combo">
                    <option value=""><?= __d('system',"Bulk Action") ?></option>
                    <option value="1"><?= __d('system',"Remove") ?></option>
                </select>
            </div>
            <div class="col-md-2 no-padding">
                <button class="btn btn-default" type="submit" id="btn-bulk-action">
                    <?= __d('system',"Apply") ?>
                </button>
            </div>
            <div class="pull-right item-amount">
                <?php
                $label = (count($medias)>=2)?__d("system"," items"):__d("system"," item");
                echo count($medias) . $label;
                ?>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th class="cb-wrap" width="5%"><input type="checkbox" class="checkbox-custom" id="checkall" /></th>
                    <th width="40%"><?= __d('system',"File") ?></th>
                    <th width="15%"><?= __d("system","Author") ?></th>
                    <th width="55%"><?= __d('system',"Date") ?></th>
                </tr>
            </thead>
            <tbody>
            <?php if(count($medias)>0): ?>
                <?php foreach($medias as $m): ?>
                    <tr>
                        <td class="cb-wrap">
                            <input type="checkbox" class="checkbox-custom checkbox-child" name="value[]" value="<?= $m["id"] ?>"/>
                        </td>
                        <td>
                            <div class="col-md-2">
                                <?= $this->element('mediathumbnail',['bean'=>$m]) ?>
                            </div>
                            <div class="col-md-9">
                                <div>
                                    <?php
                                        $title = null;
                                        foreach($m->langs as $body){
                                            if($body["languageid"] == $defaultlang["value"])
                                            {
                                                $title = $body["title"];
                                            }
                                        }
                                        if($title==null)
                                            {
                                            if(!empty($media->langs)){
                                                $title = $m->langs[0]["title"];
                                            }
                                            else{
                                                $title = __d("system","[no title]");
                                            }
                                        }
                                        echo $title;
                                    ?>
                                </div>
                                <div>
                                    <?= $this->Html->link(__d("system","edit"),['controller'=>"Medias","action"=>"edit","prefix"=>"Backend","plugin"=>"System",$m->id]) ?>
                                    |
                                    <?= $this->Html->link(__d("system","delete permanently"),['controller'=>'Medias','action'=>'remove','prefix'=>'Backend','plugin'=>'System',$m->id],['class'=>'text-danger']) ?>
                                </div>
                            </div>
                        </td>
                        <td>
                            <?= $m->createdby; ?>
                        </td>
                        <td>
                            <?= $m->date->i18nFormat($config["SITEDTFORM"]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="4">
                        <?php
                            if(isset($s))
                                echo __d('system',"No media found for ''{0}'' keyword.",[$s]);
                            else
                                echo __d('system',"No media available.")
                        ?>
                    </td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        <?= $this->Form->end(); ?>
    </div>
    <div class="box-footer">
        <?= $this->element('System.paginator'); ?>
    </div>
</div>