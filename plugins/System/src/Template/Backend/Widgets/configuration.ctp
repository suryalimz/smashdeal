<?= $this->AssetCompress->script('System.Widget234234',['block'=>'footer-script']); ?>
<?= $this->AssetCompress->css('System.Widget234234',['block'=>'css']); ?>
<div class="col-md-5 widget-group min-padding-left">
    <div class="box drop-remove" id="drop-remove">
        <i class="fa fa-trash"></i>
    </div>
    <?php
        $amount = count($widgets->toArray());
        $limit = round($amount/2);
    ?>
    <div class="box box-default">
        <div class="box-header">
            <h2 class="box-title"><?= __d("system","Available Widgets") ?></h2>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <p><?= __d("system","To activate a widget drag it to a sidebar. To deactive the widget please click delete link from the widget settings or drag it to the trash icon above.s"); ?></p>
            <div class="row">
                <div class="col-md-6 min-padding-right">
                    <?php for($i=0;$i<$limit;$i++): ?>
                        <?= $this->element('System.widgetbox',['widget'=>$widgets->toArray()[$i]]) ?>
                    <?php endfor; ?>
                </div>
                <div class="col-md-6 min-padding-left">
                    <?php for($i=$limit;$i<$amount;$i++): ?>
                        <?= $this->element('System.widgetbox',['widget'=>$widgets->toArray()[$i]]) ?>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-7" id="widget-group" data-url="<?= $this->Url->build(['controller'=>'Widgets','action'=>'widgets','prefix'=>'Ajax','plugin'=>'System']) ?>">
    <?php

        $amount = count($groups->toArray());

        $limit = round($amount/2);

    ?>
    <div class="row">
        <div class="col-md-6 no-padding-top min-padding">
            <?php for($i=0;$i<$limit;$i++): ?>
                <div class="widget-sortable box box-default">
                    <div class="box-header">
                        <h2 class="box-title"><?= $groups->toArray()[$i]->label ?></h2>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body clearfix">
                        <p><?= $groups->toArray()[$i]->note ?></p>
                        <ul id="<?= $groups->toArray()[$i]->code."_".$i ?>" data-id="<?= $groups->toArray()[$i]->id ?>" class="sortable">
                            <?php foreach($groups->toArray()[$i]->widgets as $widget): ?>
                                <?= $this->element('System.widgetbox',['widget'=>$widget,'widget_config'=>json_decode($widget->_joinData["config"],true)]) ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
        <div class="col-md-6 no-padding-top min-padding">
            <?php for($i=$limit;$i<count($groups->toArray());$i++): ?>
                <div class="box box-default">
                    <div class="box-header">
                        <h2 class="box-title"><?= $groups->toArray()[$i]->label ?></h2>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body clearfix">
                        <p><?= $groups->toArray()[$i]->note ?></p>
                        <ul id="<?= $groups->toArray()[$i]->code."_".$i ?>" data-id="<?= $groups->toArray()[$i]->id ?>" class="sortable">
                            <?php foreach($groups->toArray()[$i]->widgets as $widget): ?>
                                <?= $this->element('System.widgetbox',['widget'=>$widget,'widget_config'=>json_decode($widget->_joinData["config"],true)]) ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
    </div>
</div>