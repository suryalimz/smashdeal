<?= $this->AssetCompress->css('System.Conf455',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Conf455',['block'=>'footer-script']) ?>
<?= $this->Form->create($mediaConfig,['class'=>'form-horizontal']); ?>
    <div class="toolbar">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
        </button>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h2 class="box-title"><?= __d("system","Writing Config") ?></h2>
                </div>
                <div class="box-body">
                    <h4><?= __d("system","Image Size") ?></h4>
                    <p><?= __d("system","The size listed below determine the maximum dimensions in pixels when adding a new image"); ?></p>
                    <div class="form-group">
                        <label for="sitetitle" class="col-md-3 control-label">
                            <?= __d("system","Thumbnail Size") ?>
                        </label>
                        <div class="pull-left">
                            <label class="control-label"><?= __d("system","Width") ?></label>
                        </div>
                        <div class="col-md-2">
                            <?= $this->Form->input('THUMWIDTH.id',['type'=>'hidden']) ?>
                            <?= $this->Form->input('THUMWIDTH.value',['class'=>'form-control','id'=>'thumbnailwidth','label'=>false,'div'=>false,'escape'=>false,'min'=>100]) ?>
                        </div>
                        <div class="pull-left">
                            <label class="control-label"><?= __d("system","Height") ?></label>
                        </div>
                        <div class="col-md-2">
                            <?= $this->Form->input('THUMHEIGHT.id',['type'=>'hidden']) ?>
                            <?= $this->Form->input('THUMHEIGHT.value',['class'=>'form-control','id'=>'thumbheight','label'=>false,'div'=>false,'escape'=>false,'min'=>100]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <?= $this->Form->input('THUMCROP.id',['type'=>'hidden']) ?>
                            <?= $this->Form->checkbox('THUMCROP.value',['id'=>'thumbcrop','label'=>false,'div'=>false,'escape'=>false,'type'=>'checkbox']) ?>
                            <label><?= __d("system","Crop thumbnail to exact dimensions (normally thumbnails are proportional)") ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sitetitle" class="col-md-3 control-label">
                            <?= __d("system","Medium Size") ?>
                        </label>
                        <div class="pull-left">
                            <label class="control-label"><?= __d("system","Max Width") ?></label>
                        </div>
                        <div class="col-md-2">
                            <?= $this->Form->input('MIDWIDTH.id',['type'=>'hidden']) ?>
                            <?= $this->Form->input('MIDWIDTH.value',['class'=>'form-control','id'=>'midwidth','label'=>false,'div'=>false,'escape'=>false,'min'=>100]) ?>
                        </div>
                        <div class="pull-left">
                            <label class="control-label"><?= __d("system","Max Height") ?></label>
                        </div>
                        <div class="col-md-2">
                            <?= $this->Form->input('MIDHEIGHT.id',['type'=>'hidden']) ?>
                            <?= $this->Form->input('MIDHEIGHT.value',['class'=>'form-control','id'=>'midheight','label'=>false,'div'=>false,'escape'=>false,'min'=>100]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sitetitle" class="col-md-3 control-label">
                            <?= __d("system","Large Size") ?>
                        </label>
                        <div class="pull-left">
                            <label  class="control-label""><?= __d("system","Max Width") ?></label>
                        </div>
                        <div class="col-md-2">
                            <?= $this->Form->input('LRGWIDTH.id',['type'=>'hidden']) ?>
                            <?= $this->Form->input('LRGWIDTH.value',['class'=>'form-control','id'=>'largewidth','label'=>false,'div'=>false,'escape'=>false,'min'=>100]) ?>
                        </div>
                        <div class="pull-left">
                            <label class="control-label"><?= __d("system","Max Height") ?></label>
                        </div>
                        <div class="col-md-2">
                            <?= $this->Form->input('LRGHEIGHT.id',['type'=>'hidden']) ?>
                            <?= $this->Form->input('LRGHEIGHT.value',['class'=>'form-control','id'=>'largeheight','label'=>false,'div'=>false,'escape'=>false,'min'=>100]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="toolbar">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
        </button>
    </div>
<?= $this->Form->end(); ?>