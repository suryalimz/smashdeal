<?= $this->AssetCompress->css('System.Conf455',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Conf455',['block'=>'footer-script']) ?>
<?= $this->Form->create($permalinkConfig,['class'=>'form-horizontal','type'=>'file']); ?>
<div class="toolbar">
    <button type="submit" class="btn btn-primary">
        <i class="fa fa-save"></i>
    </button>
</div>
<div class="box">
    <div class="box-header">
        <h2 class="box-title"><?= __d("system","Permalink Config") ?></h2>
    </div>
    <div class="box-body">
        <div class="form-group">
            <div class="col-md-12">
                <h3><?= __d("system","Permalink Configuration"); ?></h3>
            </div>
            <div class="col-md-8">
                <?= $this->Form->input('PERMALINK.id',['type'=>'hidden']) ?>
                <?php $date = new Cake\i18n\Time(); ?>
                <div class="drop-label">
                    <?php
                        $options = [
                            0 => [
                                "value"=>"PLAIN",
                                "text"=>"<span class='lbl'><strong>Plain</strong></span><span class='highlight grey'>/post/6fb6b3b1-a18f-4d6f-aa8b-4b8eaaf12a9d</span>"
                            ],
                            1 => [
                                "value"=>"POSTNAME",
                                "text"=>"<span class='lbl'><strong>Post Name</strong></span><span class='highlight grey'>/post/sample-post</span>"
                            ]
                        ];
                    ?>
                    <?= $this->Form->radio('PERMALINK.value',$options,['escape'=>false]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="toolbar">
    <button type="submit" class="btn btn-primary">
        <i class="fa fa-save"></i>
    </button>
</div>
<?= $this->Form->end(); ?>