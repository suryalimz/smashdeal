<?= $this->AssetCompress->css('System.Conf455',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Conf455',['block'=>'footer-script']) ?>
<?= $this->Form->create($generalConfig,['class'=>'form-horizontal','type'=>'file']); ?>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<div class="row">
	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header">
				<h2 class="box-title"><?= __d("system","General Settings") ?></h2>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label for="sitetitle" class="col-md-4 control-label">
						<?= __d("system","Site Title") ?>
					</label>
					<div class="col-md-5">
						<?= $this->Form->input('SITETITLE.id',['type'=>'hidden']) ?>
						<?= $this->Form->input('SITETITLE.value',['class'=>'form-control','id'=>'sitetitle','placeholder'=>'site title','label'=>false,'div'=>false]) ?>
					</div>
				</div>
				<div class="form-group">
					<label for="sitetagline" class="col-md-4 control-label">
						<?= __d("system","Tagline"); ?>
					</label>
					<div class="col-md-8">
						<?= $this->Form->input('SITETAGLN.id',['type'=>'hidden']) ?>
						<?= $this->Form->input('SITETAGLN.value',['class'=>'form-control','id'=>'sitetagline','placeholder'=>'site tagline','label'=>false,'div'=>false,'type'=>'textarea']) ?>
					</div>
				</div>
				<div class="form-group">
					<label for="timezone" class="col-md-4 control-label">
						<?= __d("system","Timezone"); ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('TIMEZONE.id',['type'=>'hidden']) ?>
						<?= $this->Form->input('TIMEZONE.value',['class'=>'form-control','id'=>'timezone','placeholder'=>'site tagline','label'=>false,'div'=>false,'options'=>$timezones,'empty'=>__d("system","--Choose Timezone--")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label for="dateformat" class="col-md-4 control-label">
						<?= __d("system","Date Format") ?>
					</label>
					<div class="col-md-8">
						<?= $this->Form->input('SITEDTFORM.id',['type'=>'hidden']) ?>
						<?php $date = new Cake\i18n\Time(); ?>
						<div class="drop-label">
							<?php 
								$dateformatConfigs = [];
								foreach($dateformats as $key=>$format)
								{
									$dateformatConfigs[] = [
										"value"=>$key,
										"text"=>$date->i18nFormat($format,$config["TIMEZONE"],$config["ADMDFLTLG"])
									];
								}	
							?>
							<?= $this->Form->radio('SITEDTFORM.value',$dateformatConfigs); ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-md-4 control-label">
						<?= __d("system","Time Format") ?>
					</label>
					<div class="col-md-8">
						<?= $this->Form->input('SITETMFORM.id',['type'=>'hidden']) ?>
						<div class="drop-label">
							<?php 
								$timeformatConfigs = [];
								foreach($timeformats as $key=>$format)
								{
									$timeformatConfigs[] = [
										"value"=>$key,
										"text"=>$date->i18nFormat($format,$config["TIMEZONE"],$config["ADMDFLTLG"])
									];
								}	
							?>
							<?= $this->Form->radio('SITETMFORM.value',$timeformatConfigs); ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="weekday" class="col-md-4 control-label">
						<?= __d("system","Week start on") ?>
					</label>
					<div class="col-md-4">
						<?php
							$weekdayoptions = []; 
							foreach($weekdays as $key=>$weekday)
							{
								$day = new Cake\i18n\Time($weekday);
								$weekdayoptions[$key] = $day->i18nFormat("eeee",$config["TIMEZONE"],$config["ADMDFLTLG"]);
							}
						?>
						<?= $this->Form->input('SITEWKDAYS.id',['type'=>'hidden']) ?>
						<?= $this->Form->input('SITEWKDAYS.value',['class'=>'form-control','id'=>'weekday','label'=>false,'div'=>false,'options'=>$weekdayoptions,'empty'=>__d("system","--Choose Day--")]) ?>
					</div>
				</div>
				<div class="form-group">
					<label for="weblanguage" class="col-md-4 control-label">
						<?= __d("system","Website Language") ?>
					</label>
					<div class="col-md-4">
						<?= $this->Form->input('SITEDFLTLG.id',['type'=>'hidden']) ?>
						<?= $this->Form->input('SITEDFLTLG.value',['class'=>'form-control','id'=>'weblanguage','label'=>false,'div'=>false,'options'=>$languages,'empty'=>__d("system","--Choose Language--")]) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="toolbar">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
	</button>
</div>
<?= $this->Form->end(); ?>