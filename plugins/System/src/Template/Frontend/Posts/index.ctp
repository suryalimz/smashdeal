<div class="container sm-main-content">
    <div class="col-md-12">
        <?php foreach($posts as $post): ?>
            <div class="col-md-12">
                <h2 class="">
                    <?= $this->cell('System.Langs',['meta'=>'title','post'=>$post]) ?>
                </h2>
                <p>
                    <?php
                        $body = explode(" ",strip_tags($post->body));
                        echo implode(" ",array_slice($body,0,10))."...";
                    ?>
                </p>
            </div>
        <?php endforeach; ?>
    </div>
</div>