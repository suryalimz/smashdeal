<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">
			<?= __d('system',"Menu Access Detail") ?>
		</h3>
	</div>
	<div class="box-body no-padding">
		<table id="table-container" class="table">
			<thead>
				<tr>
					<th class="cb-wrap" width="3%">
						<input type="checkbox" class="checkbox-custom" id="checkall" />
					</th>
					<th width="250px">
						<?= __d('system',"Name") ?>
					</th>
					<th>
						<?= __d('system',"Description") ?>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($userRole->access_menus)): ?>
					<?php foreach($userRole->access_menus as $key=>$value): ?>
						<tr id="item-<?= $key ?>" class="item">
							<td>
								<input type='checkbox' class='checkbox-child'/>
							</td>
							<td>
								<input type="hidden" class="uid" name="access_menus[][id]" value="<?= $value->id ?>"/>
								<input type='text' class='form-control' value='<?= $value->name ?>' readonly/>
							</td>
							<td>
								<span><?= $value->description ?></span>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else : ?>
					<tr>
						<td colspan="3">
							<?= __d('system',"No access menu avaiable") ?>
						</td>
					</tr>
				<?php endif; ?> 
			</tbody>
		</table>
	</div>
</div>
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">
			<?= __d('system',"Menu Right Access Detail") ?>
		</h3>
	</div>
	<div class="box-body no-padding">
		<table id="table-container-right" class="table">
			<thead>
				<tr>
					<th class="cb-wrap" width="3%">
						<input type="checkbox" class="checkbox-custom" id="checkall2" />
					</th>
					<th width="250px">
						<?= __d('system',"Name") ?>
					</th>
					<th>
						<?= __d('system',"Description") ?>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($userRole->menu_rights)): ?>
					<?php foreach($userRole->menu_rights as $key=>$value): ?>
						<tr id="itemright-<?= $key ?>" class="item-right">
							<td>
								<input type='checkbox' class='checkbox-child2'/>
							</td>
							<td>
								<input type="hidden" class="uid" name="menu_rights[][id]" value="<?= $value->id ?>"/>
								<input type='text' class='form-control' value='<?= $value->displayname ?>' readonly/>
							</td>
							<td>
								<span><?= $value->description ?></span>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else : ?>
					<tr>
						<td colspan="3">
							<?= __d('system',"No menu right avaiable") ?>
						</td>
					</tr>
				<?php endif; ?> 
			</tbody>
		</table>
	</div>
</div>