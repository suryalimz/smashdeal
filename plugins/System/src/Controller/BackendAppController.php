<?php

namespace System\Controller;

use Cake\Cache\Cache;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\I18n\I18n;


class BackendAppController extends AppController
{
    
    public $helpers = [
        'Paginator' => ['templates'=>'System.paginator'],
        'Url'
    ];
	
    public function initialize()
	{
		parent::initialize();
        $this->loadComponent('Auth',[
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
                'plugin' => 'System',
                'prefix' => 'Backend'
            ],
            'authenticate'=>[
                'Form'=>[

                    'userModel' => 'System.TbSysUsers',
                    'finder'=>'Auth',
                    'fields'=> ['username'=>'username','password'=>'password']
                ]
            ],
            'authorize'=>[
                'System.Idea'
            ]
        ]);
		$this->ViewBuilder()->layout('System.default');
	}

	public function beforeFilter(Event $event)
    {
        if($this->Auth->user('isfirsttime') == true)
        {
            if(!($this->request->params["controller"]=="Users" 
                && $this->request->params["action"]=="firsttime" 
                && $this->request->params["prefix"]=="Backend" 
                && $this->request->params["plugin"]=="System")){
                $this->redirect(['controller'=>'Users','action'=>'firsttime','plugin'=>'System','prefix'=>'Backend']);
            }
        }
    }

    public function rewriteConfiguration($rewrite = false)
    {
        $session = $this->request->session();
        if($rewrite) {
            $session->delete("Config");
        }
        // if(Configure::read('debug')==true)
        // {
            $configString = $this->fetchConfig();
        // }
        // else {
        //     if (($configString = Cache::read('config', 'long')) === false) {
        //         $configString = $this->fetchConfig();
        //         Cache::write('config', $configString, 'long');
        //     }
        // }
        $session->write('Config',$configString);
        
        $this->Cookie->configKey('dateform','encryption', false);
        $this->Cookie->write('dateform',$configString["SITEDTFORM"]);

        if(isset($configString["ADMDFLTLG"])){
            I18n::locale($configString["ADMDFLTLG"]);
            $this->Cookie->configKey('lang', 'encryption', false);
            $this->Cookie->write('lang',$configString["ADMDFLTLG"]);
        }

        $rootFolder = str_replace($_SERVER['DOCUMENT_ROOT'], '', ROOT);
        $rootFolder = str_replace('/','',$rootFolder);
        $this->Cookie->configKey('APP','encryption',false);
        $this->Cookie->write('APP',$rootFolder);

        $configString = $session->read('Config');
        $this->set('config',$configString);
    }

    private function fetchConfig()
    {
        $this->loadModel('System.TbSysLookupDetails');
        $this->loadModel('System.TbSysUserPreferences');
        $this->loadModel('System.TbSysConfigs');
        $configs = $this->TbSysConfigs->find('all')->select(["code","value","valuetype"])->where(['userid'=>'SYSTEM']);
        $configString = [];
        foreach($configs as $config)
        {
            $value = $config["value"];
            if($config["valuetype"]==2)
            {
                $value = $this->TbSysLookupDetails->get($config["value"])["value"];
            }
            $configString[$config["code"]] = $value;
            $configString[$config["code"]."_VALUE"] = $config["value"];
        }
        $userprefs = $this->TbSysUserPreferences->find('all')->where(["userid"=>$this->Auth->user('id')]);
        foreach($userprefs as $userpref)
        {
            $value = $userpref["value"];
            if($userpref["valuetype"]==2)
            {
                $value = $this->TbSysLookupDetails->get($userpref["value"])["value"];
            }
            $configString[$userpref["groupname"]] = $value;
            $configString[$userpref["groupname"]."_VALUE"] = $userpref["value"];
        }
        return $configString;
    }

    public function beforeRender(Event $event)
    {
    	parent::beforeRender($event);
        $this->rewriteConfiguration(true);

        $this->loadModel('System.TbSysMenus');

        $controller = $this->request->param('controller');
        $action = $this->request->param('action');
        $plugin = $this->request->param('plugin');
        $prefix = $this->request->param("prefix");

        $menu = $this->TbSysMenus->find('all',['contain'=>['SubMenus'=>function($q){
            return $q->order(['SubMenus.index'=>'ASC']);
        },'ParentMenu']])->where(['TbSysMenus.controller'=>$controller,'TbSysMenus.action'=>$action,'TbSysMenus.plugin'=>$plugin,'TbSysMenus.prefix'=>$prefix])->first();
        $this->set('active',$menu);
    }

    public function setExtraUrlConfig($conf)
    {
        $this->set('extradata',$conf);
    }

    public function isHasRight($code)
    {
        $userrole = $this->Auth->user("role");
        foreach($userrole["menu_rights"] as $key=>$right)
        {
            if($right["code"] == $code)
                return true;
        }
        return false;
    }
}