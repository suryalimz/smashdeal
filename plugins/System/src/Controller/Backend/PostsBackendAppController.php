<?php


namespace System\Controller\Backend;

use System\Controller\BackendAppController as AppController;

class PostsBackendAppController extends AppController{


    protected function collectingMeta($old_metas,&$new_metas)
    {
        $index  = 0;
        foreach($old_metas as $old_meta)
        {
            foreach($new_metas as $new_meta)
            {
                if($new_meta["name"]!=$old_meta["name"])
                {
                    $additionals[$index]["name"] = $old_meta["name"];
                    $additionals[$index]["value"] = $old_meta["value"];
                }
            }
            $index++;
        }
        if(isset($additionals))
        {
            foreach($additionals as $additional){
                array_push($new_metas,$additional);
            }
        }
    }

    protected function collectingLangs($langs,&$new_langs)
    {
        $index = 0;
        foreach($langs as $lang_id=>$lg)
        {
            $found = false;
            foreach($new_langs as $lang)
            {
                if($lang["languageid"]==$lang_id)
                {
                    $found = true;
                    break;
                }
            }
            if(!$found)
            {
                $additionals[$index]["languageid"] = $lang_id;
                $additionals[$index]["title"] = "";
                $additionals[$index]["slug"] = "";
                $additionals[$index]["body"] = "";
                $index++;
            }
        }
        if(isset($additionals))
            foreach($additionals as $additional)
                array_push($new_langs,$additional);
    }

    protected function loadLanguages(){
        $this->loadModel('System.TbSysLookupDetails');
        $this->loadModel('System.TbSysWebLanguages');
        $langs = $this->TbSysWebLanguages->find('all');
        $results = [];
        foreach($langs as $lang)
        {
            $results[$lang["languageid"]] = $this->TbSysLookupDetails->get($lang['languageid'])->label;
        }
        return $results;
    }

}