<?php

namespace System\Controller\Component;

use Cake\Controller\Component;
use IntlDateFormatter;

class DateComponent extends Component
{

	public function convert($date,$config,$lang = null)
	{
        $language = ($lang==null)?$config["ADMDFLTLG"]:$lang;
       	$formatter = new IntlDateFormatter(
       		$language,
       		IntlDateFormatter::FULL,
       		IntlDateFormatter::FULL,
       		$config["TIMEZONE"],
       		IntlDateFormatter::GREGORIAN,
       		$config["SITEDTFORM"]
       	);
       	$timestamp = $formatter->parse($date);
       	return $timestamp;
	}

}