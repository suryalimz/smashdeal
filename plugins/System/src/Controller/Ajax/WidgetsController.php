<?php
namespace System\Controller\Ajax;

use Cake\Utility\Text;
use System\Controller\AppController;

/**
 * Widgets Controller
 *
 * @property \System\Model\Table\WidgetsTable $Widgets
 */
class WidgetsController extends AppController
{

    public function widgets()
    {
        $this->viewBuilder()->autoLayout(false);
        $this->autoRender = false;
        $this->loadModel('System.TbSysWidgetGroups');
        if($this->request->is("ajax"))
        {
            $widgets = $this->request->data["widget"];
            foreach($widgets as $key=>$widget)
            {
                $this->TbSysWidgetGroups->Details->deleteAll(['groupid'=>$widget["groupid"]]);
                if(isset($widget["widgets"])) {
                    $widgetEntity = $this->TbSysWidgetGroups->get($widget["groupid"]);
                    $j = 0;
                    foreach ($widget["widgets"] as $w) {
                        $data["details"][$j]["widgetid"] = $w["widgetid"];
                        $data["details"][$j]["index"] = ($j + 1);
                        $data["details"][$j]["config"] = json_encode($w["config"]);
                        $j++;
                    }
                    $widgetEntity = $this->TbSysWidgetGroups->patchEntity($widgetEntity,$data);
                    $this->TbSysWidgetGroups->save($widgetEntity);
                    debug($widgetEntity);
                }
            }
        }
    }

}
