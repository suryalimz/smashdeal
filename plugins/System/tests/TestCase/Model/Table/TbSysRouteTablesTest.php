<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysRouteTables;

/**
 * System\Model\Table\TbSysRouteTables Test Case
 */
class TbSysRouteTablesTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysRouteTables
     */
    public $TbSysRouteTables;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysRoutes') ? [] : ['className' => 'System\Model\Table\TbSysRouteTables'];
        $this->TbSysRouteTables = TableRegistry::get('TbSysRoutes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysRouteTables);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
