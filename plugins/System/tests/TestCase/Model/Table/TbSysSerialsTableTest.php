<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysSerialsTable;

/**
 * System\Model\Table\TbSysSerialsTable Test Case
 */
class TbSysSerialsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysSerialsTable
     */
    public $TbSysSerials;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_serials'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysSerials') ? [] : ['className' => 'System\Model\Table\TbSysSerialsTable'];
        $this->TbSysSerials = TableRegistry::get('TbSysSerials', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysSerials);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
