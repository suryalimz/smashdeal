<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysWidgetGroupDetailsTable;

/**
 * System\Model\Table\TbSysWidgetGroupDetailsTable Test Case
 */
class TbSysWidgetGroupDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysWidgetGroupDetailsTable
     */
    public $TbSysWidgetGroupDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_widget_group_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysWidgetGroupDetails') ? [] : ['className' => 'System\Model\Table\TbSysWidgetGroupDetailsTable'];
        $this->TbSysWidgetGroupDetails = TableRegistry::get('TbSysWidgetGroupDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysWidgetGroupDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
