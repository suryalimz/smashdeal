<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysMenuAccessesTable;

/**
 * System\Model\Table\TbSysMenuAccessesTable Test Case
 */
class TbSysMenuAccessesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysMenuAccessesTable
     */
    public $TbSysMenuAccesses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_menu_accesses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysMenuAccesses') ? [] : ['className' => 'System\Model\Table\TbSysMenuAccessesTable'];
        $this->TbSysMenuAccesses = TableRegistry::get('TbSysMenuAccesses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysMenuAccesses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
