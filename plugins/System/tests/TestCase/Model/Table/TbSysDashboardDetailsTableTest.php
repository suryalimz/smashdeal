<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysDashboardDetailsTable;

/**
 * System\Model\Table\TbSysDashboardDetailsTable Test Case
 */
class TbSysDashboardDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysDashboardDetailsTable
     */
    public $TbSysDashboardDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_dashboard_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysDashboardDetails') ? [] : ['className' => 'System\Model\Table\TbSysDashboardDetailsTable'];
        $this->TbSysDashboardDetails = TableRegistry::get('TbSysDashboardDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysDashboardDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
