<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysConfigsTable;

/**
 * System\Model\Table\TbSysConfigsTable Test Case
 */
class TbSysConfigsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysConfigsTable
     */
    public $TbSysConfigs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_configs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysConfigs') ? [] : ['className' => 'System\Model\Table\TbSysConfigsTable'];
        $this->TbSysConfigs = TableRegistry::get('TbSysConfigs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysConfigs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
