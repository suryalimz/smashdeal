<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysTermmetasTable;

/**
 * System\Model\Table\TbSysTermmetasTable Test Case
 */
class TbSysTermmetasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysTermmetasTable
     */
    public $TbSysTermmetas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_termmetas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysTermmetas') ? [] : ['className' => 'System\Model\Table\TbSysTermmetasTable'];
        $this->TbSysTermmetas = TableRegistry::get('TbSysTermmetas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysTermmetas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
