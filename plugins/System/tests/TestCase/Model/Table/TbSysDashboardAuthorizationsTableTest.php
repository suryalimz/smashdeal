<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysDashboardAuthorizationsTable;

/**
 * System\Model\Table\TbSysDashboardAuthorizationsTable Test Case
 */
class TbSysDashboardAuthorizationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysDashboardAuthorizationsTable
     */
    public $TbSysDashboardAuthorizations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_dashboard_authorizations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysDashboardAuthorizations') ? [] : ['className' => 'System\Model\Table\TbSysDashboardAuthorizationsTable'];
        $this->TbSysDashboardAuthorizations = TableRegistry::get('TbSysDashboardAuthorizations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysDashboardAuthorizations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
