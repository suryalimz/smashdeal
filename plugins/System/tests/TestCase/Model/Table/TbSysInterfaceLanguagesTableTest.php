<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysInterfaceLanguagesTable;

/**
 * System\Model\Table\TbSysInterfaceLanguagesTable Test Case
 */
class TbSysInterfaceLanguagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysInterfaceLanguagesTable
     */
    public $TbSysInterfaceLanguages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_interface_languages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysInterfaceLanguages') ? [] : ['className' => 'System\Model\Table\TbSysInterfaceLanguagesTable'];
        $this->TbSysInterfaceLanguages = TableRegistry::get('TbSysInterfaceLanguages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysInterfaceLanguages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
