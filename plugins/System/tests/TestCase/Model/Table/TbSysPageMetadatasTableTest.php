<?php
namespace System\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use System\Model\Table\TbSysPageMetadatasTable;

/**
 * System\Model\Table\TbSysPageMetadatasTable Test Case
 */
class TbSysPageMetadatasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Table\TbSysPageMetadatasTable
     */
    public $TbSysPageMetadatas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.system.tb_sys_page_metadatas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbSysPageMetadatas') ? [] : ['className' => 'System\Model\Table\TbSysPageMetadatasTable'];
        $this->TbSysPageMetadatas = TableRegistry::get('TbSysPageMetadatas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbSysPageMetadatas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
