<?php
namespace System\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use System\Model\Behavior\CreatorBehavior;

/**
 * System\Model\Behavior\CreatorBehavior Test Case
 */
class CreatorBehaviorTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \System\Model\Behavior\CreatorBehavior
     */
    public $Creator;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Creator = new CreatorBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Creator);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
