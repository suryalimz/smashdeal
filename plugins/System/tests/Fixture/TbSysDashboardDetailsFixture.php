<?php
namespace System\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TbSysDashboardDetailsFixture
 *
 */
class TbSysDashboardDetailsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'dashboardid' => ['type' => 'string', 'length' => 50, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'dashboardcomponentid' => ['type' => 'string', 'length' => 50, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'pos' => ['type' => 'integer', 'length' => 5, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'index' => ['type' => 'integer', 'length' => 5, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['dashboardid', 'dashboardcomponentid'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'dashboardid' => '98b1e3ff-283a-467a-8bc2-1682a9a17b47',
            'dashboardcomponentid' => '8feb2032-eb96-4613-ac6a-d64168e18a2e',
            'pos' => 1,
            'index' => 1
        ],
    ];
}
