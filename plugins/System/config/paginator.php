<?php

return [
    'nextActive' => '<a href="{{url}}" class="btn btn-default btn-sm next"><i class="fa fa-chevron-right"></i></a>',
    'nextDisabled' => '<a class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></a>',
    'prevActive' => '<a href="{{url}}" class="btn btn-default btn-sm prev" ><i class="fa fa-chevron-left"></i></a>',
    'prevDisabled' => '<a class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></a>'
];