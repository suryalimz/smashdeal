$("#customlinkadd").click(function(e)
{
    var datatarget = $(this).attr("data-target");
    var widgetid = datatarget;
    var index = $("#menugroupitem li").size();
    var source = 'Custom Link';
    var menutype = 1;
    var permalink = $("#"+$(this).attr("data-target")+" .permalink").val();
    var label = $("#"+$(this).attr("data-target")+" .link-label").val();
    var configForm = $($(this).attr("data-component-target")).attr("data-value");
    if(permalink != "" && label != "" && permalink != "http://")
    {
        var datas = {
            menu : []
        };
        var data = {
            "source" : source,
            "menutype" : menutype,
            "index" : index,
            "widgetid" : widgetid,
            "permalink" : permalink,
            "config" : {
                "label" : label
            },
            "extraconfig" : "",
            "configForm" : configForm
        };
        datas.menu.push(data);
        var url = $("#menuselector").attr("data-href");
        $.ajax(url,{
            type:'POST',
            data : datas,
            complete : function(data)
            {
                $("#menugroupitem").append($(data.responseText));
                enableEffect();
                $("#"+datatarget+" .permalink").val("http://");
                $("#"+datatarget+" .link-label").val("");
                $("#"+datatarget+" .permalink").removeClass("form-error");
                $("#"+datatarget+" .link-label").removeClass("form-error");
            },
            dataType:'HTML'
        });
    }
    else
    {
        $("#"+$(this).attr("data-target")+" .permalink").addClass("form-error");
        $("#"+$(this).attr("data-target")+" .link-label").addClass("form-error");
    }
});