$(document).ready(function(e){

	var addedItems = [];
	var addedItemRights = [];
	var item = 0;
	var itemright = 0;

	var isAddedMenu = function(addedItem)
	{
		if(addedItems.length > 0)
		{
			for(var idx in addedItems){
				if(addedItems[idx].uid == addedItem.uid)
				{
					return true;
				}
			}
		}
		return false;
	}

	var isAddedRight = function(addedItem)
	{
		if(addedItemRights.length > 0)
		{
			for(var idx in addedItemRights){
				if(addedItemRights[idx].uid == addedItem.uid)
				{
					return true;
				}
			}
		}
		return false;
	}

	var removeAdded = function(uid)
	{
		if(addedItems.length>0)
		{
			for(item in addedItems)
			{
				if(addedItems[item].uid == uid)
				{
					addedItems.splice(item,1);
				}
			}
		}
	}

	var removeAddedRight = function(uid)
	{
		if(addedItemRights.length>0)
		{
			for(item in addedItemRights)
			{
				if(addedItemRights[item].uid == uid)
				{
					addedItemRights.splice(item,1);
				}
			}
		}
	}

	var activeiCheck = function(selector)
	{
		$(selector).iCheck({
			checkboxClass: 'icheckbox_polaris',
		    radioClass: 'iradio_square',
		    increaseArea: '20%' // optional
	  	});
	}

	var deleteSingleItem = function(component)
	{
		var idx= $(component).parent().parent().attr("id");
		var uid = $("#"+idx+" .uid").val();
		removeAdded(uid);
		$("#"+idx).remove();
	}

	var deleteSingleItemRight = function(component)
	{
		var idx= $(component).parent().parent().attr("id");
		var uid = $("#"+idx+" .uid").val();
		removeAddedRight(uid);
		$("#"+idx).remove();
	}

	$("#lovMenu").lov({
		'title' : 'Lov Menu',
		'width' : 1000,
		'height' : 400,
		'source' : {
			datafields : 
			[
				{name : 'code', type : 'string'},
				{name : 'name', type : 'string'},
				{name : 'description', type: 'string'},
				{name : 'parent', map:'parent_menu>name',type:'string'}
			]
		},
		'columns' : [
			{'text':'Code',datafield:'code'},
			{'text':'Name',datafield:'name'},
			{'text':'Parent Menu',datafield:'parent'}
		],
		'searchType' : 'row',
		'filterable' : true,
		'selectionmode' : 'multiplerows',
		'callback' :  function(datas)
		{
			if(datas.length>0)
			{
				for(idx in datas)
				{
					if(!isAddedMenu(datas[idx]))
					{
						var id = "item-"+item;
						$("#table-container").append("<tr id='"+id+"'><td><input type='checkbox' class='checkbox-child'/></td><td><input type='hidden' class='uid' name='access_menus[][id]' value='"+datas[idx].uid+"'/><input type='text' class='form-control' value='"+datas[idx].name+"' readonly/></td><td><span>"+datas[idx].description+"</span></td><td><button class='btn btn-danger btn-delete'><i class='fa fa-trash'></i></button></td></tr>");
						addedItems.push(datas[idx]);
						$("#"+id+" .btn-delete").on("click",function(e)
						{
							e.preventDefault();
							deleteSingleItem($(this));
						});
						item++;
					}
				}
				activeiCheck("#table-container tbody input[type='checkbox']");
			}
		}
	});

	$("#lovMenuRight").lov({
		'title' : 'Lov Menu Right',
		'width' : 1000,
		'height' : 400,
		'source' : {
			datafields : 
			[
				{name : 'code', type : 'string'},
				{name : 'displayname', type : 'string'},
				{name : 'description', type: 'string'},
				{name : 'menu', map:'tb_sys_menu>name',type:'string'}
			]
		},
		'columns' : [
			{'text':'Code',datafield:'code'},
			{'text':'Name',datafield:'displayname'},
			{'text':'Menu',datafield:'menu'}
		],
		'searchType' : 'row',
		'filterable' : true,
		'selectionmode' : 'multiplerows',
		'callback' :  function(datas)
		{
			if(datas.length>0)
			{
				for(idx in datas)
				{
					if(!isAddedRight(datas[idx]))
					{
						var id = "itemright-"+itemright;
						$("#table-container-right").append("<tr id='"+id+"'><td><input type='checkbox' class='checkbox-child2'/></td><td><input type='hidden' class='uid' name='menu_rights[][id]' value='"+datas[idx].uid+"'/><input type='text' class='form-control' value='"+datas[idx].displayname+"' readonly/></td><td><span>"+datas[idx].description+"</span></td><td><button class='btn btn-danger btn-delete'><i class='fa fa-trash'></i></button></td></tr>");
						addedItemRights.push(datas[idx]);
						$("#"+id+" .btn-delete").on("click",function(e)
						{
							e.preventDefault();
							deleteSingleItemRight($(this));
						});
						itemright++;
					}
				}
				activeiCheck("#table-container-right tbody input[type='checkbox']");
			}
		}
	});

	$("#btnBulkDelete").on('click',function(e)
	{
		e.preventDefault();
		$("#table-container input.checkbox-child:checked").each(function()
		{
			var trParent = $(this).parent().parent().parent().attr("id");
			var uid = $("#"+trParent+" .uid").val();
			removeAdded(uid);
		});
		$("#table-container input.checkbox-child:checked").parent().parent().parent().remove();
	});

	$("#btnBulkDeleteRight").on('click',function(e)
	{
		e.preventDefault();
		$("#table-container-right input.checkbox-child:checked").each(function()
		{
			var trParent = $(this).parent().parent().parent().attr("id");
			var uid = $("#"+trParent+" .uid").val();
			removeAddedRight(uid);
		});
		$("#table-container-right input.checkbox-child:checked").parent().parent().parent().remove();
	});

	activeiCheck("input[type='checkbox']");

	if($(".item").length>0)
	{
		item = $(".item").length;
		$(".item .btn-delete").on("click",function(e)
		{
			e.preventDefault();
			deleteSingleItem($(this));
		});
		$(".item").each(function()
		{
			var id = $(this).attr("id");
			var uid = $("#"+id+" .uid").val();
			addedItems.push({uid:uid});
		});
	}

	if($(".item-right").length>0)
	{
		itemright = $(".item-right").length;
		$(".item-right .btn-delete").on("click",function(e)
		{
			e.preventDefault();
			deleteSingleItemRight($(this));
		});
		$(".item-right").each(function()
		{
			var id = $(this).attr("id");
			var uid = $("#"+id+" .uid").val();
			addedItemRights.push({uid:uid});
		});
	}

	$("#checkall2").on('ifChecked',function(event)
	{
	    $("input.checkbox-child2").iCheck('check');
	});
	$("#checkall2").on('ifUnchecked',function(event)
	{
	    $("input.checkbox-child2").iCheck('uncheck');
	});

});