$("#postcategorylinkadd").click(function(e)
{
    var menuid = $("#postcategorylinkid").attr("data-value");
    var datatarget = $(this).attr("data-target");
    var widgetid = datatarget;
    var index = $("#menugroupitem li").size();
    var source = 'Category';
    var menutype = 2;
    var configForm = $($(this).attr("data-component-target")).attr("data-value");
    var datas = {
        menu : []
    };
    var endindex = index;
    $("#"+datatarget+" input:checked").each(function (data) {
        var slug = $(this).attr("data-slug");
        var label = $(this).attr("data-label");
        var categoryid = $(this).attr("data-category");
        var data = {
            "source":source,
            "menutype":menutype,
            "index":endindex,
            "widgetid":widgetid,
            "menuid":menuid,
            "config": {
                "label" : label,
                'isoriginal' : 'true',
                'categoryid' : categoryid
            },
            "extraconfig" : [slug],
            "configForm": configForm
        }
        datas.menu.push(data);
        endindex++;
    });
    var url = $("#menuselector").attr("data-href");
    $.ajax(url,{
        type:'POST',
        data : datas,
        complete : function(data)
        {
            $("#menugroupitem").append($(data.responseText));
            $("#"+datatarget+" input[type='checkbox']").iCheck('uncheck');
            enableEffect();
        },
        dataType:'HTML'
    });
});