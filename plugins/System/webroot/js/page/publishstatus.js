$(document).ready(function () {
    $("#editStatus").click(function (e) {
        e.preventDefault();

        $("#editStatus").hide();

        var wrapper = $(this).parent();

        var statusSelect = $("#statusTemplate").clone();
        $(statusSelect).removeClass("hidden").attr("id", "statusSelect");
        var checkedVal = $("#publishstatus").val();


        var content = $("<div class='clearfix'></div>").hide();

        var okbutton = $("<a id='statusokbutton' href='#' class='btn btn-default btn-sm'>OK</a>");
        $(wrapper).delegate("#statusokbutton", "click", function (e) {
            e.preventDefault();
            $("#printedstatus").html($("#statusSelect").find("option:selected").html());
            $("#publishstatus").val($("#statusSelect").val());
            $(content).slideUp('slow').delay(1000).remove();
            $("#editStatus").show();
        });

        var cancelbutton = $("<a id='statuscancelbutton' href='#'>Cancel</a>");
        $(wrapper).delegate("#statuscancelbutton", "click", function (e) {
            e.preventDefault();
            $(content).slideUp('slow').delay(1000).remove();
            $("#editStatus").show();
        });

        $(content).append("<div class='col-md-7 no-padding min-padding-right'>" + $(statusSelect).prop('outerHTML') + "</div><div class='col-md-5 no-padding'>" + $(okbutton).prop("outerHTML") + $(cancelbutton).prop("outerHTML") + "</div>");

        $(content).appendTo($(wrapper));

        $("#statusSelect option[value='" + checkedVal + "']").attr("selected", "selected");
        $(content).slideDown('slow');
    });
});