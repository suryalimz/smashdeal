$(document).ready(function()
{
    var link = $("#getparent").attr("href");
    $("#getparent").remove();
    $.get(link,function(data){
        response = JSON.parse(data);
        for(index in response)
        {
            var entry = response[index];
            $("#postparent").append("<option value='" + entry.id + "' level='"+entry.level+"'>" + entry.title + "</option>");
            for(index2 in entry.children){
                var entry2 = entry.children[index2];
                $("#postparent").append("<option value='" + entry2.id + "' level='"+entry2.level+"'> -" + entry2.title + "</option>");
            }
        }

        var valueSelected = $("#postparent").attr("value");
        $("#postparent option[value='"+valueSelected+"']").attr("selected","selected");
    });
    $("#postparent").change(function(e)
    {
        var parentIndex = parseInt($(this).find("option:selected").attr("level"));
        $("#pagelevel").val(parentIndex+1);
    });
});