$("#pagelinkadd").click(function(e)
{
    var menuid = $("#pagelinkid").attr("data-value");
    var datatarget = $(this).attr("data-target");
    var widgetid = datatarget;
    var index = $("#menugroupitem li").size();
    var source = 'Page';
    var menutype = 2;
    var configForm = $($(this).attr("data-component-target")).attr("data-value");
    var datas = {
        menu : []
    };
    var endindex = index;
    var items = [];
    $("#"+datatarget+" input:checked").each(function (data) {
        var slug = $(this).attr("data-slug");
        var label = $(this).attr("data-label");
        var postid = $(this).attr("data-post");
        var data = {
            "source":source,
            "menutype":menutype,
            "index":endindex,
            "widgetid":widgetid,
            "menuid":menuid,
            "config": {
                "label" : label,
                "isoriginal": "true",
                "pageid" : postid
            },
            "extraconfig" : [slug],
            "configForm": configForm
        }
        datas.menu.push(data);
        endindex++;
    });
    var url = $("#menuselector").attr("data-href");
    $.ajax(url,{
        type:'POST',
        data : datas,
        complete : function(data)
        {
            $("#menugroupitem").append($(data.responseText));
            $("#"+datatarget+" input[type='checkbox']").iCheck('uncheck');
            enableEffect();
        },
        dataType:'HTML'
    });
});