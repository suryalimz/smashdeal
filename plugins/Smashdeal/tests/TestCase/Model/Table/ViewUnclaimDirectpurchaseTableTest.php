<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewUnclaimDirectpurchaseTable;

/**
 * Smashdeal\Model\Table\ViewUnclaimDirectpurchaseTable Test Case
 */
class ViewUnclaimDirectpurchaseTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewUnclaimDirectpurchaseTable
     */
    public $ViewUnclaimDirectpurchase;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_unclaim_directpurchase'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewUnclaimDirectpurchase') ? [] : ['className' => 'Smashdeal\Model\Table\ViewUnclaimDirectpurchaseTable'];
        $this->ViewUnclaimDirectpurchase = TableRegistry::get('ViewUnclaimDirectpurchase', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewUnclaimDirectpurchase);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
