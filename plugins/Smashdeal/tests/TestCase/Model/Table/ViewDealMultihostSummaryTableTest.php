<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewDealMultihostSummaryTable;

/**
 * Smashdeal\Model\Table\ViewDealMultihostSummaryTable Test Case
 */
class ViewDealMultihostSummaryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewDealMultihostSummaryTable
     */
    public $ViewDealMultihostSummary;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_deal_multihost_summary'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewDealMultihostSummary') ? [] : ['className' => 'Smashdeal\Model\Table\ViewDealMultihostSummaryTable'];
        $this->ViewDealMultihostSummary = TableRegistry::get('ViewDealMultihostSummary', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewDealMultihostSummary);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
