<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewDealCashTokenJournalTable;

/**
 * Smashdeal\Model\Table\ViewDealCashTokenJournalTable Test Case
 */
class ViewDealCashTokenJournalTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewDealCashTokenJournalTable
     */
    public $ViewDealCashTokenJournal;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_deal_cash_token_journal'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewDealCashTokenJournal') ? [] : ['className' => 'Smashdeal\Model\Table\ViewDealCashTokenJournalTable'];
        $this->ViewDealCashTokenJournal = TableRegistry::get('ViewDealCashTokenJournal', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewDealCashTokenJournal);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
