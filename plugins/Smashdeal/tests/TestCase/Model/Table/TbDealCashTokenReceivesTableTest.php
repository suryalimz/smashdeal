<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\TbDealCashTokenReceivesTable;

/**
 * Smashdeal\Model\Table\TbDealCashTokenReceivesTable Test Case
 */
class TbDealCashTokenReceivesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\TbDealCashTokenReceivesTable
     */
    public $TbDealCashTokenReceives;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.tb_deal_cash_token_receives'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbDealCashTokenReceives') ? [] : ['className' => 'Smashdeal\Model\Table\TbDealCashTokenReceivesTable'];
        $this->TbDealCashTokenReceives = TableRegistry::get('TbDealCashTokenReceives', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbDealCashTokenReceives);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
