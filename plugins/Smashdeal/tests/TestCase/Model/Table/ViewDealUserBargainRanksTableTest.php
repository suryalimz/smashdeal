<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewDealUserBargainRanksTable;

/**
 * Smashdeal\Model\Table\ViewDealUserBargainRanksTable Test Case
 */
class ViewDealUserBargainRanksTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewDealUserBargainRanksTable
     */
    public $ViewDealUserBargainRanks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_deal_user_bargain_ranks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewDealUserBargainRanks') ? [] : ['className' => 'Smashdeal\Model\Table\ViewDealUserBargainRanksTable'];
        $this->ViewDealUserBargainRanks = TableRegistry::get('ViewDealUserBargainRanks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewDealUserBargainRanks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
