<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\TbDealProductLangsTable;

/**
 * Smashdeal\Model\Table\TbDealProductLangsTable Test Case
 */
class TbDealProductLangsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\TbDealProductLangsTable
     */
    public $TbDealProductLangs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.tb_deal_product_langs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbDealProductLangs') ? [] : ['className' => 'Smashdeal\Model\Table\TbDealProductLangsTable'];
        $this->TbDealProductLangs = TableRegistry::get('TbDealProductLangs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbDealProductLangs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
