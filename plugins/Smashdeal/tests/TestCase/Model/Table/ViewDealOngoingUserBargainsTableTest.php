<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewDealOngoingUserBargainsTable;

/**
 * Smashdeal\Model\Table\ViewDealOngoingUserBargainsTable Test Case
 */
class ViewDealOngoingUserBargainsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewDealOngoingUserBargainsTable
     */
    public $ViewDealOngoingUserBargains;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_deal_ongoing_user_bargains'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewDealOngoingUserBargains') ? [] : ['className' => 'Smashdeal\Model\Table\ViewDealOngoingUserBargainsTable'];
        $this->ViewDealOngoingUserBargains = TableRegistry::get('ViewDealOngoingUserBargains', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewDealOngoingUserBargains);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
