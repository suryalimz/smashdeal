<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\TbDealBtPaymentConfirmationsTable;

/**
 * Smashdeal\Model\Table\TbDealBtPaymentConfirmationsTable Test Case
 */
class TbDealBtPaymentConfirmationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\TbDealBtPaymentConfirmationsTable
     */
    public $TbDealBtPaymentConfirmations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.tb_deal_bt_payment_confirmations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TbDealBtPaymentConfirmations') ? [] : ['className' => 'Smashdeal\Model\Table\TbDealBtPaymentConfirmationsTable'];
        $this->TbDealBtPaymentConfirmations = TableRegistry::get('TbDealBtPaymentConfirmations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbDealBtPaymentConfirmations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
