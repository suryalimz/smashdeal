<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewDealUserBargainsTable;

/**
 * Smashdeal\Model\Table\ViewDealUserBargainsTable Test Case
 */
class ViewDealUserBargainsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewDealUserBargainsTable
     */
    public $ViewDealUserBargains;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_deal_user_bargains'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewDealUserBargains') ? [] : ['className' => 'Smashdeal\Model\Table\ViewDealUserBargainsTable'];
        $this->ViewDealUserBargains = TableRegistry::get('ViewDealUserBargains', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewDealUserBargains);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
