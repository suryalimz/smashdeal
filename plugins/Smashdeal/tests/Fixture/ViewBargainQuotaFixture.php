<?php
namespace Smashdeal\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ViewBargainQuotaFixture
 *
 */
class ViewBargainQuotaFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'view_bargain_quota';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'productid' => ['type' => 'string', 'length' => 50, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'persentase' => ['type' => 'decimal', 'length' => 18, 'default' => null, 'null' => true, 'comment' => null, 'precision' => 2, 'unsigned' => null],
        'lastbargaintime' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'productid' => 'Lorem ipsum dolor sit amet',
            'persentase' => 1.5,
            'lastbargaintime' => 1500642108
        ],
    ];
}
