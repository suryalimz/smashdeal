$(document).ready(function(e)
{
  	$("input").on('change',function(event)
  	{
  		var target = $(this).attr("data-target");
  		$(".payment-detail").hide();
  		$("#"+target).show();	
      if(target == "cashtoken")
      {
        $("#withoutunique").show();
        $("#withunique").hide();
      }
      else
      {
        $("#withoutunique").hide();
        $("#withunique").show();
      }
  	});
});