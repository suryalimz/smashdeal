<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;

/**
 * OtherBargains cell
 */
class OtherBargainsCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($attribute = null,$widget_config = null)
    {
        $this->loadModel('Smashdeal.TbDealProducts');
        $product = $attribute["product"];
        if($product!=null)
        {
            $products = [];
        }
        else{
            $products = [];
        }
        $this->set('products',$products);
        $this->set('widget_config',$widget_config);
    }

    public function configForm($widget,$widget_config)
    {
        $this->set('widget', $widget);
        $this->set('widget_config',$widget_config);
    }
}
