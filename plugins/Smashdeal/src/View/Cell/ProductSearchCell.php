<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;

/**
 * ProductSearch cell
 */
class ProductSearchCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($q = null)
    {
        $this->loadModel('Smasdeahl.TbDealProducts');
        $product = $this->TbDealProducts->newEntity();
        $this->set('product',$product);
        $this->set('q',$q);
    }

    public function mobile($q = null)
    {
        $this->loadModel('Smasdeahl.TbDealProducts');
        $product = $this->TbDealProducts->newEntity();
        $this->set('product',$product);
        $this->set('q',$q);
    }
}
