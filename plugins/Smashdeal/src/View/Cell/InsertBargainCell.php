<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;

/**
 * InsertBargain cell
 */
class InsertBargainCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($bargainid)
    {
        $this->loadModel('Smashdeal.TbDealProducts');
        $this->loadModel('Smashdeal.TbDealUserProducts');
        $bargain = $this->TbDealUserProducts->newEntity();
        $product = $this->TbDealProducts->get($bargainid);
        $this->set('product',$product);
        $this->set('bargainid',$bargainid);
        $this->set('bargain',$bargain);
    }

    public function mobile($bargainid)
    {
        $this->loadModel('Smashdeal.TbDealProducts');
        $this->loadModel('Smashdeal.TbDealUserProducts');
        $bargain = $this->TbDealUserProducts->newEntity();
        $product = $this->TbDealProducts->get($bargainid);
        $this->set('product',$product);
        $this->set('bargainid',$bargainid);
        $this->set('bargain',$bargain);
    }
}
