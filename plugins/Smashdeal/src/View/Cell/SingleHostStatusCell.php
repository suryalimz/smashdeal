<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;

/**
 * SingleHostStatus cell
 */
class SingleHostStatusCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($bargain)
    {
        if(!$this->isSingleHost($bargain))
        {
            $this->set('isSingle',false);
            return;
        }
        else{
            $this->set('isSingle',true);
            $this->set('rank',$this->singleHostRank($bargain));
        }
    }

    private function singleHostRank($bargain)
    {
        $this->loadModel('Smashdeal.ViewDealUserBargainRanks');
        $bargains = $this->ViewDealUserBargainRanks->find('all')->where(['productid'=>$bargain->productid])->toArray();
        $rank = 1;
        foreach($bargains as $b)
        {
            if($b->price == $bargain->price)
            {
                return $rank;
            }
            $rank++;
        }
        return $rank;
    }

    private function isSingleHost($bargain)
    {
        $this->loadModel('Smashdeal.TbDealUserProducts');
        $others = $this->TbDealUserProducts->find('all')
            ->where(['userid !='=>$bargain->userid,'productid'=>$bargain->productid,'price'=>$bargain->price])
            ->toArray();
        if(count($others)>0)
        {
            return false;
        }
        return true;
    }

}
