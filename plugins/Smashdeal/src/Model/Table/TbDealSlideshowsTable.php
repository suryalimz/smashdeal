<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealSlideshows Model
 *
 * @method \Smashdeal\Model\Entity\TbDealSlideshow get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealSlideshow newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealSlideshow[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealSlideshow|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealSlideshow patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealSlideshow[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealSlideshow findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealSlideshowsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_slideshows');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->belongsTo('Medias',['className'=>'System.TbSysPosts','foreignKey'=>'mediaid','condition'=>['Medias.posttype'=>3]]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('mediaid');

        $validator
            ->allowEmpty('description');

        $validator
            ->integer('index')
            ->allowEmpty('index');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('updatedby');

        return $validator;
    }
}
