<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewDealProductGained Model
 *
 * @method \Smashdeal\Model\Entity\ViewDealProductGained get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealProductGained newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealProductGained[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealProductGained|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealProductGained patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealProductGained[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealProductGained findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ViewDealProductGainedTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_deal_product_gained');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('productid');

        $validator
            ->allowEmpty('userid');

        $validator
            ->decimal('price')
            ->allowEmpty('price');

        $validator
            ->allowEmpty('moneyowned');

        $validator
            ->decimal('difference')
            ->allowEmpty('difference');

        $validator
            ->allowEmpty('id');

        $validator
            ->date('date')
            ->allowEmpty('date');

        $validator
            ->allowEmpty('productname');

        $validator
            ->allowEmpty('slug');

        $validator
            ->allowEmpty('description');

        $validator
            ->dateTime('dealend')
            ->allowEmpty('dealend');

        $validator
            ->decimal('retailprice')
            ->allowEmpty('retailprice');

        $validator
            ->decimal('bargainstart')
            ->allowEmpty('bargainstart');

        $validator
            ->decimal('bargainend')
            ->allowEmpty('bargainend');

        $validator
            ->decimal('incrementvalue')
            ->allowEmpty('incrementvalue');

        $validator
            ->allowEmpty('merchantid');

        $validator
            ->integer('bargaintoken')
            ->allowEmpty('bargaintoken');

        $validator
            ->boolean('isactive')
            ->allowEmpty('isactive');

        $validator
            ->integer('publishstatus')
            ->allowEmpty('publishstatus');

        $validator
            ->integer('visibility')
            ->allowEmpty('visibility');

        $validator
            ->allowEmpty('postpassword');

        $validator
            ->allowEmpty('permalink');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        $validator
            ->boolean('isclosed')
            ->allowEmpty('isclosed');

        $validator
            ->allowEmpty('closedby');

        $validator
            ->dateTime('closed')
            ->allowEmpty('closed');

        $validator
            ->allowEmpty('closedhistoryid');

        return $validator;
    }

    public function findUnfullfilled(Query $query, $options)
    {
        $query->where(['difference >'=>-11000000])->order(['dealend'=>'ASC','difference'=>'DESC']);
        return $query;
    }

}