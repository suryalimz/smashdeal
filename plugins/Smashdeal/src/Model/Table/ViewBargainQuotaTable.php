<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewBargainQuota Model
 *
 * @method \Smashdeal\Model\Entity\ViewBargainQuotum get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewBargainQuotum newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewBargainQuotum[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewBargainQuotum|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewBargainQuotum patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewBargainQuotum[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewBargainQuotum findOrCreate($search, callable $callback = null, $options = [])
 */
class ViewBargainQuotaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_bargain_quota');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('productid');

        $validator
            ->decimal('persentase')
            ->allowEmpty('persentase');

        $validator
            ->dateTime('lastbargaintime')
            ->allowEmpty('lastbargaintime');

        return $validator;
    }
}
