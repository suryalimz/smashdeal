<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealUsers Model
 *
 * @method \Smashdeal\Model\Entity\TbDealUser get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealUser newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealUser[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealUser|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealUser[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealUser findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_users');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username')
            ->add('username','usernameValid',[
                    'rule'=> array('custom','/^[a-zA-Z0-9_.]{5,12}$/'),
                    'message'=>__d("smashdeal","Username only allow alphanumeric._ and .")
                ]);

        $validator
            ->allowEmpty('firstname');

        $validator
            ->allowEmpty('lastname');

        $validator
            ->allowEmpty('address');

        $validator
            ->allowEmpty('handphoneno');

        $validator
            ->allowEmpty('phoneno');

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('idno');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->boolean('isactive')
            ->allowEmpty('isactive');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['idno']));
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    public function findAuth(Query $query,array $options)
    {
        $query->where(['TbDealUsers.isactive'=>TRUE]);
        return $query;
    }
}
