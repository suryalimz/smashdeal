<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewDealConfirmBtPurchases Model
 *
 * @method \Smashdeal\Model\Entity\ViewDealConfirmBtPurchase get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealConfirmBtPurchase newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealConfirmBtPurchase[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealConfirmBtPurchase|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealConfirmBtPurchase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealConfirmBtPurchase[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealConfirmBtPurchase findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ViewDealConfirmBtPurchasesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_deal_confirm_bt_purchases');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id');

        $validator
            ->allowEmpty('invoiceno');

        $validator
            ->integer('paymenttype')
            ->allowEmpty('paymenttype');

        $validator
            ->allowEmpty('tokenproductid');

        $validator
            ->allowEmpty('userid');

        $validator
            ->decimal('price')
            ->allowEmpty('price');

        $validator
            ->decimal('uniquenumber')
            ->allowEmpty('uniquenumber');

        $validator
            ->decimal('discount')
            ->allowEmpty('discount');

        $validator
            ->decimal('grandtotal')
            ->allowEmpty('grandtotal');

        $validator
            ->integer('tokenamount')
            ->allowEmpty('tokenamount');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    public function findUser(Query $query, array $options)
    {
        $user = $options["userid"];

        $query->where(['userid'=>$user])->order(['created'=>'DESC']);

        return $query;
    }
}