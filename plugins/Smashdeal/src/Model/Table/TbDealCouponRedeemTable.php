<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealCouponRedeem Model
 *
 * @method \Smashdeal\Model\Entity\TbDealCouponRedeem get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCouponRedeem newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCouponRedeem[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCouponRedeem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCouponRedeem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCouponRedeem[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCouponRedeem findOrCreate($search, callable $callback = null)
 */
class TbDealCouponRedeemTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_coupon_redeem');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Uuid');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('couponid', 'create')
            ->notEmpty('couponid');

        $validator
            ->requirePresence('userid', 'create')
            ->notEmpty('userid');

        $validator
            ->dateTime('redeemtime')
            ->requirePresence('redeemtime', 'create')
            ->notEmpty('redeemtime');

        $validator
            ->requirePresence('computerip', 'create')
            ->notEmpty('computerip');

        $validator
            ->requirePresence('useragent', 'create')
            ->notEmpty('useragent');

        return $validator;
    }
}
