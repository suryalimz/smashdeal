<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewDealSinglehostSummary Model
 *
 * @method \Smashdeal\Model\Entity\ViewDealSinglehostSummary get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealSinglehostSummary newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealSinglehostSummary[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealSinglehostSummary|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealSinglehostSummary patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealSinglehostSummary[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealSinglehostSummary findOrCreate($search, callable $callback = null)
 */
class ViewDealSinglehostSummaryTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_deal_singlehost_summary');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('userid');

        $validator
            ->allowEmpty('productid');

        $validator
            ->decimal('price')
            ->allowEmpty('price');

        $validator
            ->allowEmpty('count');

        $validator
            ->dateTime('bargaintime')
            ->allowEmpty('bargaintime');

        return $validator;
    }
}
