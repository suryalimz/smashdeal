<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewProductUserBargaining Model
 *
 * @method \Smashdeal\Model\Entity\ViewProductUserBargaining get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewProductUserBargaining newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewProductUserBargaining[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewProductUserBargaining|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewProductUserBargaining patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewProductUserBargaining[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewProductUserBargaining findOrCreate($search, callable $callback = null)
 */
class ViewProductUserBargainingTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_product_user_bargaining');
        $this->displayField('name');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('userid');

        $validator
            ->allowEmpty('productid');

        $validator
            ->allowEmpty('username');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('idno');

        $validator
            ->allowEmpty('address');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->allowEmpty('phoneno');

        $validator
            ->allowEmpty('handphoneno');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
