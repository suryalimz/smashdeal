<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewDealUnconfirmBtPurchases Model
 *
 * @method \Smashdeal\Model\Entity\ViewDealUnconfirmBtPurchase get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUnconfirmBtPurchase newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUnconfirmBtPurchase[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUnconfirmBtPurchase|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUnconfirmBtPurchase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUnconfirmBtPurchase[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUnconfirmBtPurchase findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ViewDealUnconfirmBtPurchasesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_deal_unconfirm_bt_purchases');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id');

        $validator
            ->allowEmpty('invoiceno');

        $validator
            ->integer('paymenttype')
            ->allowEmpty('paymenttype');

        $validator
            ->allowEmpty('tokenproductid');

        $validator
            ->allowEmpty('userid');

        $validator
            ->decimal('price')
            ->allowEmpty('price');

        $validator
            ->decimal('uniquenumber')
            ->allowEmpty('uniquenumber');

        $validator
            ->decimal('discount')
            ->allowEmpty('discount');

        $validator
            ->decimal('grandtotal')
            ->allowEmpty('grandtotal');

        $validator
            ->integer('tokenamount')
            ->allowEmpty('tokenamount');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    public function findUser(Query $query, array $options)
    {
        $user = $options["userid"];

        $query->where(['userid'=>$user]);

        return $query;
    }
}
