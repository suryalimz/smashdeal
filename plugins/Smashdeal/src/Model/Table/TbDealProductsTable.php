<?php
namespace Smashdeal\Model\Table;

use Cake\I18n\Date;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealProducts Model
 *
 * @method \Smashdeal\Model\Entity\TbDealProduct get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProduct newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProduct[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProduct|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProduct patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProduct[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProduct findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_products');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->hasMany('Metas',['className'=>'Smashdeal.TbDealProductMetadatas','foreignKey'=>'productid','saveStrategy'=>'replace','dependent'=>true]);
        $this->hasMany('Langs',['className'=>'Smashdeal.TbDealProductLangs','foreignKey'=>'productid','saveStrategy'=>'replace','dependent'=>true]);
        $this->hasMany('Medias',['className'=>'Smashdeal.TbDealProductMedias','foreignKey'=>'productid','saveStrategy'=>'replace','dependent'=>true]);
        $this->belongsToMany('Terms',[
            'className'=>'System.TbSysTerms',
            'joinTable'=>'tb_sys_term_relationships',
            'foreignKey'=>'objectid',
            'targetForeignKey'=>'termid',
            'saveStrategy'=>'replace'
        ]);
        $this->belongsTo('Merchants',['className'=>'Smashdeal.TbDealMerchants','foreignKey'=>'merchantid','dependent'=>true]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('productname', 'create')
            ->notEmpty('productname');

        $validator
            ->allowEmpty('description');

        $validator
            ->dateTime('dealend')
            ->allowEmpty('dealend');

        $validator
            ->decimal('retailprice')
            ->requirePresence('retailprice', 'create')
            ->notEmpty('retailprice');

        $validator
            ->decimal('bargainstart')
            ->requirePresence('bargainstart', 'create')
            ->notEmpty('bargainstart');

        $validator
            ->decimal('bargainend')
            ->requirePresence('bargainend', 'create')
            ->notEmpty('bargainend');

        $validator
            ->decimal('incrementvalue')
            ->requirePresence('incrementvalue', 'create')
            ->notEmpty('incrementvalue');

        $validator
            ->integer('bargaintoken')
            ->allowEmpty('bargaintoken');

        $validator
            ->boolean('isactive')
            ->allowEmpty('isactive');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    public function findStatus($query,array $options)
    {
        $s = strtolower($options['s']);
        if(strtolower($s)=="draft")
            $status = 1;
        elseif(strtolower($s)=="review")
            $status = 2;
        elseif(strtolower($s)=="publish")
            $status = 3;
        $query->contain(['Langs','Metas','Medias','Terms']);
        $query->where(['publishstatus'=>$status]);
        return $query;
    }

    public function findDisplay($query,array $options)
    {
        $date = new Date();
        if(isset($options["category"]))
            $category = $options["category"];
        $query->contain(['Langs','Metas','Medias','Terms','Merchants']);
        if(isset($category))
        {
            $query->innerJoinWith('Terms', function($q) use ($category){
                return $q->where(['Terms.id'=>$category,'Terms.taxonomy'=>"product_category"]);
            });
        }
        $query->where(['and'=>['publishstatus'=>3,'date <='=>$date]]);
        return $query;
    }

    public function findCategories($query, array $options)
    {
        $date = new Date();
        if(isset($options["categories"]))
            $categories = $options["categories"];
        $query->contain(['Langs','Metas','Medias','Terms','Merchants']);
        if(isset($categories))
        {
            $query->innerJoinWith('Terms', function($q) use ($categories){
                return $q->where(['Terms.id IN'=>$categories,'Terms.taxonomy'=>"product_category"]);
            });
        }
        $query->where(['and'=>['publishstatus'=>3,'date <='=>$date,'dealend >'=>$date]]);
        return $query;
    }

    public function findSearch($query,array $options)
    {
        $date = new Date();
        if(isset($options["q"]))
            $q = strtolower($options["q"]);
        $defaultlanguage = $options["defaultlanguage"];
        $query->contain(['Langs','Metas','Medias','Terms','Merchants']);
        $query->innerJoinWith('Langs',function($query) use ($defaultlanguage){
            return $query->where(['languageid'=>$defaultlanguage]);
        })->where(['and'=>['publishstatus'=>3,'date <='=>$date,'dealend >'=>$date]])->andWhere(['lower(Langs.name) LIKE'=>'%'.$q.'%']);
        return $query;
    }

    public function findName($query,array $options)
    {
        $s = strtolower($options['s']);
        $status = $options["status"];
        $defaultlang = $options["defaultlang"];
        $query->contain(['Langs','Metas','Medias','Terms']);
        if($status==null) {
            $query->innerJoinWith('Langs',function($q) use ($defaultlang)
            {
                return $q->where(["languageid"=>$defaultlang]);
            })->where(
                [
                    "AND"=>[
                        ["OR"=>["lower(Langs.name) LIKE"=>"%".$s."%",'lower(TbDealProducts.createdby) LIKE'=>"%".$s."%"]]
                    ]
                ]
            );
        }
        else{
            if(strtolower($status)=="draft")
                $bools = 1;
            elseif(strtolower($status)=="review")
                $bools = 2;
            elseif(strtolower($status)=="publish")
                $bools = 3;
            $query->innerJoinWith('Langs',function($q) use ($defaultlang)
            {
                return $q->where(["languageid"=>$defaultlang]);
            })->where(
                [
                    "AND"=>[
                        ["OR"=>["lower(Langs.name) LIKE"=>"%".$s."%",'lower(TbDealProducts.createdby) LIKE'=>"%".$s."%"]],
                        'publishstatus'=>$bools
                    ]
                ]
            );
        }
        return $query;
    }
}
