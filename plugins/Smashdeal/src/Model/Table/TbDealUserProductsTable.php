<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealUserProducts Model
 *
 * @method \Smashdeal\Model\Entity\TbDealUserProduct get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealUserProduct newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealUserProduct[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealUserProduct|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealUserProduct patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealUserProduct[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealUserProduct findOrCreate($search, callable $callback = null)
 */
class TbDealUserProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_user_products');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Uuid');

        $this->belongsTo('Users',['className'=>'Smashdeal.TbDealUsers','foreignKey'=>'userid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('userid', 'create')
            ->notEmpty('userid');

        $validator
            ->requirePresence('productid', 'create')
            ->notEmpty('productid');

        $validator
            ->decimal('price')
            ->allowEmpty('price');

        $validator
            ->dateTime('bargaintime')
            ->allowEmpty('bargaintime');

        return $validator;
    }
}
