<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewUnclaimDirectpurchase Model
 *
 * @method \Smashdeal\Model\Entity\ViewUnclaimDirectpurchase get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewUnclaimDirectpurchase newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewUnclaimDirectpurchase[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewUnclaimDirectpurchase|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewUnclaimDirectpurchase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewUnclaimDirectpurchase[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewUnclaimDirectpurchase findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ViewUnclaimDirectpurchaseTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_unclaim_directpurchase');
        $this->displayField('name');
        $this->primaryKey('id');


        $this->addBehavior('Timestamp');

        $this->belongsTo('Merchants',['className'=>'Smashdeal.TbDealMerchants','foreignKey'=>'merchantid']);
        $this->belongsTo('Purchases',['className'=>'Smashdeal.TbDealDirectPurchases','foreignKey'=>'claimid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id');

        $validator
            ->allowEmpty('claimid');

        $validator
            ->integer('claimtype')
            ->allowEmpty('claimtype');

        $validator
            ->allowEmpty('vouchercode');

        $validator
            ->date('validity')
            ->allowEmpty('validity');

        $validator
            ->boolean('isvalid')
            ->allowEmpty('isvalid');

        $validator
            ->allowEmpty('userid');

        $validator
            ->allowEmpty('merchantid');

        $validator
            ->dateTime('claimat')
            ->allowEmpty('claimat');

        $validator
            ->allowEmpty('claimby');

        $validator
            ->allowEmpty('idno');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('address');

        $validator
            ->allowEmpty('phoneno');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    public function findUser(Query $query, array $options)
    {
        $user = $options["userid"];
        
        $query->contain(['Merchants','Purchases'=>function($q){return $q->contain(['TbDealProducts']);}]);
        $query->where(['ViewUnclaimDirectpurchase.userid'=>$user]);

        return $query;
    }
}
