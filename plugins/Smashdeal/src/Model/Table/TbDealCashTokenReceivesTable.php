<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealCashTokenReceives Model
 *
 * @method \Smashdeal\Model\Entity\TbDealCashTokenReceife get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCashTokenReceife newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCashTokenReceife[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCashTokenReceife|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCashTokenReceife patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCashTokenReceife[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCashTokenReceife findOrCreate($search, callable $callback = null)
 */
class TbDealCashTokenReceivesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_cash_token_receives');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Uuid');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('source', 'create')
            ->notEmpty('source');

        $validator
            ->requirePresence('userid', 'create')
            ->notEmpty('userid');

        $validator
            ->dateTime('receivetime')
            ->requirePresence('receivetime', 'create')
            ->notEmpty('receivetime');

        $validator
            ->decimal('cashtoken')
            ->requirePresence('cashtoken', 'create')
            ->notEmpty('cashtoken');

        return $validator;
    }
}
