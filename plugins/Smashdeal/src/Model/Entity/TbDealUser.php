<?php
namespace Smashdeal\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * TbDealUser Entity
 *
 * @property string $id
 * @property string $username
 * @property string $firstname
 * @property string $lastname
 * @property string $address
 * @property string $handphoneno
 * @property string $phoneno
 * @property string $idno
 * @property string $email
 * @property bool $isactive
 * @property string $createdby
 * @property \Cake\I18n\Time $created
 * @property string $modifiedby
 * @property \Cake\I18n\Time $modified
 */
class TbDealUser extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_hidden = [
        'password'
    ];


    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }
}
