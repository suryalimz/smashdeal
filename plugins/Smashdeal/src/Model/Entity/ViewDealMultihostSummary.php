<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * ViewDealMultihostSummary Entity
 *
 * @property int $r
 * @property string $id
 * @property string $userid
 * @property string $productid
 * @property float $price
 * @property \Cake\I18n\Time $bargaintime
 * @property int $guesthost
 */
class ViewDealMultihostSummary extends Entity
{

}
