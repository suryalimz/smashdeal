<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * ViewProductUserBargaining Entity
 *
 * @property string $userid
 * @property string $productid
 * @property string $username
 * @property string $name
 * @property string $idno
 * @property string $address
 * @property string $email
 * @property string $phoneno
 * @property string $handphoneno
 */
class ViewProductUserBargaining extends Entity
{

}
