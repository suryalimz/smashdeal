<?php if(count($slideshows->toArray())>0): ?>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php $first = true; foreach($slideshows as $key=>$slideshow): ?>
      <li data-target="#carousel-example-generic" data-slide-to="<?= $key ?>" class="<?php if($first){ echo 'active'; $first=false; }?>"></li>
    <?php endforeach; ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php $first = true; foreach($slideshows as $slideshow): ?>
        <div class="item <?php if($first){ echo 'active'; $first=false; }?>">
          <?=$this->cell('System.MediaRenderer',['mediaid'=>$slideshow['mediaid']]); ?>    
        </div>
    <?php endforeach; ?>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only"><?= __d("smashdeal","Previous") ?></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only"><?= __d("smashdeal","Nex") ?></span>
  </a>
</div>
<?php else: ?>
    <div style="clear:both;margin-top:20px;"></div>
<?php endif; ?>