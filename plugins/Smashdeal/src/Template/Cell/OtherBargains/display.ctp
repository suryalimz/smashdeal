<?php $config = json_decode($widget_config); ?>
<div class="other-bargain-wrapper clearfix">
    <h3 class="sm-prd-detail-sub-title"><?= (isset($config->title) || $config->title!="")?$config->title:__d("smashdeal","Other Bargain") ?></h3>
    <div class="other-bargain-list">
        <a href="#" class="small-bargain-link">
            <div class="small-bargain-box clearfix">
                <div class="col-md-4 col-sm-3 col-xs-3 no-padding img-wrapper">
                    <?= $this->Html->image('deal_marriott_8516.jpg') ?>
                </div>
                <div class="col-md-8 col-sm-9 col-xs-9 info-wrapper">
                    <h4 class="info-title">
                        Cake of the Month Desember "Creamy Chocolate Fudge Cake" ...
                    </h4>
                    <div class="price-wrapper">
                        <div class="col-md-8 col-sm-8 col-xs-9 no-padding price">
                            <div class="merchant-name">
                                <i class="fa fa-map-marker"></i>&nbsp;Hotel Aryaduta
                            </div>
                            Rp. 1000.0000,-
                            <br/>
                            <span class="price-range">
                                            Rp. 200.000 - Rp. 300.000
                                        </span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 no-padding token-amount">
                            10 <br/>
                            Tokens
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="#" class="small-bargain-link">
            <div class="small-bargain-box clearfix">
                <div class="col-md-4 col-sm-3 col-xs-3 no-padding img-wrapper">
                    <?= $this->Html->image('deal_marriott_8516.jpg') ?>
                </div>
                <div class="col-md-8 col-sm-9 col-xs-9 info-wrapper">
                    <h4 class="info-title">
                        Cake of the Month Desember "Creamy Chocolate Fudge Cake" ...
                    </h4>
                    <div class="price-wrapper">
                        <div class="col-md-8 col-sm-8 col-xs-9 no-padding price">
                            <div class="merchant-name">
                                <i class="fa fa-map-marker"></i>&nbsp;Hotel Aryaduta
                            </div>
                            Rp. 1000.0000,-
                            <br/>
                            <span class="price-range">
                                            Rp. 200.000 - Rp. 300.000
                                        </span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 no-padding token-amount">
                            10 <br/>
                            Tokens
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="#" class="small-bargain-link">
            <div class="small-bargain-box clearfix">
                <div class="col-md-4 col-sm-3 col-xs-3 no-padding img-wrapper">
                    <?= $this->Html->image('deal_marriott_8516.jpg') ?>
                </div>
                <div class="col-md-8 col-sm-9 col-xs-9 info-wrapper">
                    <h4 class="info-title">
                        Cake of the Month Desember "Creamy Chocolate Fudge Cake" ...
                    </h4>
                    <div class="price-wrapper">
                        <div class="col-md-8 col-sm-8 col-xs-9 no-padding price">
                            <div class="merchant-name">
                                <i class="fa fa-map-marker"></i>&nbsp;Hotel Aryaduta
                            </div>
                            Rp. 1000.0000,-
                            <br/>
                            <span class="price-range">
                                            Rp. 200.000 - Rp. 300.000
                                        </span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 no-padding token-amount">
                            10 <br/>
                            Tokens
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>