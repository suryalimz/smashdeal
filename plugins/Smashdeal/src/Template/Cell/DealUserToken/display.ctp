<?php if(isset($user)): ?>
<div class="token-list">
    <ul>
        <li>
            <?= __d("ibkcore","Bargain Token : ") ?><?= $this->Html->link($user['tokenamount'],['controller'=>'Users','action'=>'bargaintoken','plugin'=>'Smashdeal','prefix'=>'Frontend']) ?>
        </li>
        <li>
            <?= __d("ibkcore","Cash Token : ") ?><?= $this->Html->link($this->Number->currency($user['topupamount']),['controller'=>'Users','action'=>'cashtoken','plugin'=>'Smashdeal','prefix'=>'Frontend']) ?>
        </li>
    </ul>
</div>
<?php endif; ?>