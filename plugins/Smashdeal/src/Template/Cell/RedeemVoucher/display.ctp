<?= $this->Html->script('Smashdeal.redeemvoucher/action') ?>
<div class="redeem-voucher">
    <?= $this->Html->link(__d("smashdeal","Redeem Voucher"),"#",['id'=>'redeem-voucher-link']); ?>
    <div id="redeem-box" class="redeem-floating-box hidden">
        <div class="redeem-floating-box-mending"></div>
        <div class="floating-box-content clearfix">
            <?= $this->Form->create($coupon,['class'=>'horizontal-form','url'=>['controller'=>'Coupons','action'=>'redeemVoucher','prefix'=>'Frontend','plugin'=>'Smashdeal']]) ?>
            <div class="form-group">
                    <label for="sm-label"><?= __d("smashdeal","Voucher Code :") ?></label>
                    <input type="text" name="vouchercode" class="form-control sm-form-control">
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6"><button class="btn btn-smd btn-sm-primary"><?= __d("smashdeal","Redeem Voucher") ?></button></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>