<?= $this->Form->create($product,['url'=>['controller'=>'Products','action'=>'search'],'type'=>'get']) ?>
<div class="input-field">
	<input type="search" name="q" placeholder="<?= __d("smashdeal","Find what deal today?") ?>" value="<?= (isset($q))?$q:"" ?>" />
	<label class="label-icon" for="search"><i class="material-icons">search</i></label>
	<i class="material-icons">close</i>
</div>
<?= $this->Form->end(); ?>