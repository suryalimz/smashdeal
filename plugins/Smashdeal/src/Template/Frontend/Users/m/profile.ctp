<?= $this->AssetCompress->script('Smashdeal.Profile.m',['block'=>'footer-script']) ?>
<div class="container sm-main-content">
    <div class="sm-box">
        <h1 class="section-header center-align">
            <?= __d("smashdeal","Edit Profile") ?>
        </h1>
        <?= $this->Form->create($user,['class'=>'form-horizontal']) ?>
            <div class="form-group">
                <label for="#username" class="control-label col-md-3">
                    <?= __d("smashdeal","Username") ?>
                </label>
                <div class="col-md-6">
                    <?= $this->Form->input('username',['label'=>false,'div'=>false,'readonly'=>true,'class'=>'form-control']) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="#username" class="control-label col-md-3">
                    <?= __d("smashdeal","Password") ?>
                </label>
                <div class="col-md-6">
                   	<?= $this->Html->link(__d("smashdeal","Change Your Password"),['controller'=>'Users','action'=>'changePassword','plugin'=>'smashdeal','prefix'=>'Frontend']); ?>
                </div>
            </div>
            <hr class="separator">
            <div class="form-group required">
                <label for="#name" class="control-label col-md-3">
                    <?= __d("smashdeal","ID No.") ?>
                   	<small class="info"><?= __d("smashdeal","(Fill with your ID Number)"); ?></small>
                </label>
                <div class="col-md-6">
                    <?= $this->Form->input('idno',['label'=>false,'div'=>false,'class'=>'form-control','readonly'=>true]) ?>
                </div>
            </div>
            <div class="form-group required">
                <label for="#name" class="control-label col-md-3">
                    <?= __d("smashdeal","Name") ?>
                    <small class='info'><?= __d("smashdeal","(Please fill your name as written on your ID Card)") ?></small>
                </label>
                <div class="col-md-6">
                    <?= $this->Form->input('name',['label'=>false,'div'=>false,'class'=>'form-control']) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="#name" class="control-label col-md-3">
                    <?= __d("smashdeal","Birthday") ?>
                    <small class="info"><?= __d("smashdeal","(Fill your birthday and got special offer at your birthday)"); ?></small>
                </label>
                <div class="col-md-6">
                    <?= $this->Form->input('birthday',['label'=>false,'div'=>false,'type'=>'text','class'=>'form-control datepicker','value'=>(isset($user->birthday)?$user->birthday->i18nFormat("dd/MM/yyyy"):"")]); ?>
                </div>
            </div>
            <div class="form-group required">
                <label for="#name" class="control-label col-md-3">
                    <?= __d("smashdeal","Gender") ?>
                </label>
                <div class="col-md-3">
                    <?php $option = ["M"=>__d("smashdeal","Male"),"F"=>__d("smashdeal","Female"),"O"=>__d("smashdeal","Others")]; ?>
                    <?= $this->Form->input('gender',['label'=>false,'div'=>false,'options'=>$option,'class'=>'form-control']) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="#name" class="control-label col-md-3">
                    <?= __d("smashdeal","Address") ?>
                    <small class="info"><?= __d("smashdeal","(Please put address as  in your ID Card)"); ?></small>
                </label>
                <div class="col-md-6">
                    <?= $this->Form->input('address',['label'=>false,'div'=>false,'class'=>'materialize-textarea']) ?>
                </div>
            </div>
            <div class="form-group required">
                <label for="#email" class="control-label col-md-3">
                    <?= __d("smashdeal","Email") ?>
                </label>
                <div class="col-md-6">
                    <?= $this->Form->input('email',['label'=>false,'div'=>false,'class'=>'form-control']) ?>
                </div>
            </div>
            <div class="form-group required">
                <label for="#phone" class="control-label col-md-3">
                    <?= __d("smashdeal","Handphone") ?>
                </label>
                <div class="col-md-6">
                    <?= $this->Form->input('handphoneno',['label'=>false,'div'=>false,'id'=>'handphone','class'=>'form-control']) ?>
                </div>
            </div>
            <div class="button-group center-align">
                <button type="submit" class="waves-effect waves-light btn btn-sm-primary">
                    <?= __d("smashdeal","Update Profile") ?>
                </button>
            </div>
        <?= $this->Form->end() ?>
    </div>
</div>