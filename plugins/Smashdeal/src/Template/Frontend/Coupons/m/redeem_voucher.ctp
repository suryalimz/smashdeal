<div class="container sm-main-content">
    <div class="sm-box clearfix">
        <h1 class="section-header center-align">
            Redeem Voucher
        </h1>
        <?= $this->Form->create($coupon,['url'=>['controller'=>'Coupons','action'=>'redeemVoucher','prefix'=>'Frontend','plugin'=>'Smashdeal']]) ?>
            <div class="form-group">
                <label for="email" class="col-md-4 control-label"><?= __d("smashdeal", "Voucher Code") ?></label>
                <div class="col-md-8 no-padding-md-left">
                    <?= $this->Form->input('vouchercode',['class'=>'form-control','label'=>false]) ?>
                </div>
            </div>
            <div class="button-group clearfix">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-smd btn-sm-primary btn-block"><?= __d("smashdeal", "Redeem Voucher") ?></button>
                </div>
            </div>
        <?= $this->Form->end(); ?>
    </div>
</div>