<div class="container sm-main-content">
    <div class="col-md-8">
        <h1 class="section-header"><?= __d("smashdeal", "My Bargain") ?></h1>
        <?php if(count($mybargains)>0): ?>
            <?php foreach($mybargains as $product): ?>
                <?= $this->Html->link($this->cell('Smashdeal.SingleProductLong',['productid'=>$product->id,'config_front'=>$config_front]),['controller'=>'Bargains','action'=>'view','prefix'=>'Frontend','plugin'=>'Smashdeal',$product->slug],['escape'=>false]); ?>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="col-md-12 no-product">
                <?= __d("smashdeal","We currently has no bargain to display"); ?>
            </div>
        <?php endif; ?>
        <?= $this->element('System.paginator') ?>
    </div>
</div>