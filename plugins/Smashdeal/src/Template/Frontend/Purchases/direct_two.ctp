<div class="container sm-main-content">
	<div class="sm-box col-md-6 col-md-push-3 col-xs-12">
    	<h1 class="section-header">
			<?= __d("smashdeal","Purchase Successful") ?>
	    </h1>
	    <p>
			<?= __d("smashdeal","Terima Kasih telah melakukan pembelian dengan cash token. Kode Voucher sudah dikirimkan ke email anda. Silahkan cek email anda dan segera klaim produk anda di merchantnya.") ?>
		</p>
	</div>
</div>