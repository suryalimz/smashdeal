<?= $this->AssetCompress->script('Smashdeal.Directpurchase'); ?>
<div class="container sm-main-content">
	<div class="cart clearfix">
		<?= $this->Form->create($product); ?>
			<h1><?= $product->productname ?></h1>
			<table class="table cart-table">
				<thead>
					<tr>
						<th width="40%" class="text-center"><?= __d("smashdeal","ITEM") ?></th>
						<th width="20%" class="text-center"><?= __d("smashdeal","RETAIL PRICE") ?></th>
						<th width="5%"></th>
						<th width="10%" class="text-center"><?= __d("smashdeal","QUANTITY") ?></th>
						<th width="20%" class="text-center"><?= __d("smashdeal","TOTAL") ?></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<div class="item">
								<strong><?= $product->productname ?></strong>
							</div>
						</td>
						<td class="text-right">
							<div class="item">
								<input type="hidden" name="retailprice" value="<?= $product->retailprice ?>">
								<?= $this->Number->currency($product->retailprice) ?>
							</div>
						</td>
						<td class="text-center">
							<div class="item">X</div>
						</td>
						<td>
							<div class="item">
								<input type="number" data-price="<?= $product->retailprice ?>" id="quantity" name="amount" class="bordered cart-money" value="0">
							</div>
						</td>
						<td class="text-right">
							<div class="item">
								<input type="text" id="subtotal" class="money cart-money text-right" value="0" readonly/>
							</div>
						</td>
					</tr>
					<tr>
						<td class="text-right" colspan="4"><?= __d("smashdeal","Subtotal") ?></td>
						<td>
							<input type="text" id="subtotal-bottom" class="money cart-money text-right" value="0" readonly/>
						</td>
					</tr>
					<tr>
						<td class="text-right" colspan="4"><?= __d("smashdeal","Cash Token") ?> (<span class="cashtoken"><?= $this->Number->currency($user['topupamount']) ?></span>)</td>
						<td>
							<input type="hidden" name="cashtoken" id="cashtoken" />
							<input type="text" id="cashinput" class="money cart-money text-right bordered" data-target="#cashtoken" value="0"/>
						</td>
					</tr>
					<tr class="large">
						<td class="text-right" colspan="4"><?= __d("smashdeal","Must Pay To Merchant") ?></td>
						<td>
							<input type="text" id="grandtotal" class="money cart-money text-right" value="0" readonly/>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="col-md-2 col-md-push-5">
				<button type="submit" class="btn btn-block btn-sm-primary">
					<?= __d("smashdeal","Purchase") ?>
				</button>
			</div>
		<?= $this->Form->end(); ?>
	</div>
</div>