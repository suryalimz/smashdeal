<?= $this->Form->create($bargain); ?>
<div class="box box-default">
    <div class="box-header">
        <h2 class="box-title"><?= __d("smashdeal", "Single Host Summary") ?></h2>
    </div>
    <div class="box-body no-padding">
        <table class="table">
            <thead>
                <tr>
                    <th><?= __d("smashdeal","No.") ?></th>
                    <th><?= __d("smashdeal","Username") ?></th>
                    <th><?= __d("smashdeal","Name") ?></th>
                    <th><?= __d("smashdeal","Product Name") ?></th>
                    <th><?= __d("smashdeal","Price") ?></th>
                    <th><?= __d("smashdeal","Bargain Time") ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($singlehost as $key=>$sh): ?>
                    <tr>
                        <td><?= $key+1 ?></td>
                        <td><?= $sh["username"] ?></td>
                        <td><?= $sh["name"]?></td>
                        <td><?= $sh["productname"] ?></td>
                        <td><?= $this->Number->currency($sh->price,'IDR') ?></td>
                        <th><?= $sh["bargaintime"]->i18nFormat($config["SITEDTFORM"]." ".$config["SITETMFORM"]); ?></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary pull-right"><?= __d("smashdeal","Close Bargain") ?></button>
    </div>
</div>
<div class="box box-default">
    <div class="box-header">
        <h2 class="box-title">
            <?= __d("smashdeal","Multiple Host Summary") ?>
        </h2>
    </div>
    <div class="box-body">
        <table class="table">
            <thead>
            <tr>
                <th><?= __d("smashdeal","No.") ?></th>
                <th><?= __d("smashdeal","Username") ?></th>
                <th><?= __d("smashdeal","Name") ?></th>
                <th><?= __d("smashdeal","Product Name") ?></th>
                <th><?= __d("smashdeal","Price") ?></th>
                <th><?= __d("smashdeal","Bargain Time") ?></th>
                <th><?= __d("smashdeal","Guest Host") ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($multihost as $key=>$mh): ?>
                <tr>
                    <td><?= $key+1 ?></td>
                    <td><?= $mh["username"] ?></td>
                    <td><?= $mh["name"] ?></td>
                    <td><?= $mh["productname"] ?></td>
                    <td><?= $this->Number->currency($mh["price"],'IDR') ?></td>
                    <th><?= $mh["bargaintime"]->i18nFormat($config["SITEDTFORM"]." ".$config["SITETMFORM"]); ?></th>
                    <td><?= $mh["guesthost"] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary pull-right"><?= __d("smashdeal","Close Bargain") ?></button>
    </div>
</div>
<?= $this->Form->end(); ?>