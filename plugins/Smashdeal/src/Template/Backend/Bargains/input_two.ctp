<?php Cake\I18n\I18n::locale($config["SITEDFLTLG"]); ?>
<?= $this->AssetCompress->script('Smashdeal.InputDeal234',['block'=>'footer-script']) ?>
<?= $this->Form->create($bargain,['class'=>'form-horizontal']) ?>
<div class="box box-default">
    <div class="box-header">
        <h2 class="box-title"><?= __d("smashdeal","List Of Single Host") ?></h2>
    </div>
    <div class="box-body">
        <table class="table">
            <thead>
            <tr>
                <td><?= __d("smashdeal","No.") ?></td>
                <td><?= __d("smashdeal","Username") ?></td>
                <td><?= __d("smashdeal","Price") ?></td>
                <td><?= __d("smashdeal","Check") ?></td>
                <td><?= __d("smashdeal","User") ?></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach($singlehosts as $key=>$singlehost): ?>
                <tr>
                    <td><?= $key+1 ?></td>
                    <td><?= $singlehost["username"] ?><?= ($singlehost["isfake"])?"(FAKE)":"" ?></td>
                    <td><?= $this->Number->currency($singlehost["price"]) ?></td>
                    <td><input type="checkbox" name="bargain[<?= $key ?>][price]" value="<?= $singlehost["price"] ?>" /></td>
                    <td><?= $this->Form->input('bargain.'.$key.".user",['class'=>'form-control','label'=>false,'div'=>false,'options'=>$fakeusers,'empty'=>__d("smashdeal","--Choose User--")]) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="box-footer">
        <button type="submit" name="execute" class="btn btn-primary"><?= __d("smashdeal","Submit") ?></button>
    </div>
</div>
<div class="box box-default">
    <div class="box-header">
        <h2 class="box-title"><?= __d("smashdeal","Input New Deal") ?></h2>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label class="control-label col-md-2"><?= __d("smashdeal","User") ?></label>
            <div class="col-md-4">
                <?= $this->Form->input('user',['class'=>'form-control','label'=>false,'div'=>false,'options'=>$fakeusers,'empty'=>__d("smashdeal","--Choose User--")]) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2"><?= __d("smashdeal","Price") ?></label>
            <div class="col-md-4">
                <?= $this->Form->input('price',['id'=>'price','label'=>false,'type'=>'hidden']) ?>
                <input type="text" data-target="#price" class="form-control money" value="0"/>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <button type="submit" name="input" class="btn btn-primary"><?= __d("smashdeal","Submit") ?></button>
    </div>
</div>
<?= $this->Form->end(); ?>