<?= $this->AssetCompress->css('Smashdeal.slideshowidx',['block'=>'css']) ?>
<?= $this->AssetCompress->script('Smashdeal.slideshowidx',['block'=>'footer-script']) ?>
<div class="box box-default">
    <div class="box-header">
        <h2 class="box-title"><?= __d("smashdeal", "Manage Slide Show's Images") ?></h2>
    </div>
    <div class="box-body no-padding">
        <?= $this->Form->create($slideshow,['url'=>['action'=>'bulkAction'],'id'=>'bulkActionForm']); ?>
        <div class="list-action clearfix">
            <div class="col-md-2 no-padding">
                <select id="bulk-action" name="action" class="form-control i-combo">
                    <option value=""><?= __d('system',"Bulk Action") ?></option>
                    <option value="1"><?= __d('system',"Activate") ?></option>
                    <option value="2"><?= __d('system',"Non-Activate") ?></option>
                    <option value="3"><?= __d('system',"Remove") ?></option>
                </select>
            </div>
            <div class="col-md-2 no-padding">
                <button class="btn btn-default" type="submit" id="btn-bulk-action">
                    <?= __d('system',"Apply") ?>
                </button>
            </div>
            <div class="pull-right item-amount">
                <?php
                $label = (count($slideshows->toArray())>=2)?__d("system"," items"):__d("system"," item");
                echo count($slideshows->toArray()) . $label;
                ?>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th class="cb-wrap" width="5%"><input type="checkbox" class="checkbox-custom" id="checkall" /></th>
                    <th width="30%"><?= __d("smashdeal","Images") ?></th>
                    <th><?= __d("smashdeal","Description") ?></th>
                    <th width="6%" class="text-center"><?= __d("smashdeal","Index") ?></th>
                    <th width="6%" class="text-center"><?= __d("smashdeal","Status") ?></th>
                    <th width="7%" class="text-center"><?= __d("smashdeal","Action") ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($slideshows->toArray())>0): ?>
                    <?php foreach($slideshows as $s): ?>
                        <tr>
                            <td><input type="checkbox" class="checkbox-custom checkbox-child" name="value[]" value="<?= $s["id"] ?>"/></td>
                            <td>
                                <?= $this->cell('System.MediaRenderer',['mediaid'=>$s['mediaid']]) ?>
                            </td>
                            <td>
                                <?= $s['description'] ?>
                            </td>
                            <td class="text-center">
                                <?= $s["index"] ?>
                            </td>
                            <td class="text-center">
                                <input type="checkbox" class="cbreadonly" <?= ($s["isactive"])?"checked":"" ?> readonly/>
                            </td>
                            <td>
                                <div class="btn-group btn-group-sm">
                                <?= $this->Html->link("<i class='fa fa-edit'></i>",['controller'=>'SlideShows','action'=>'edit','plugin'=>'Smashdeal','prefix'=>'Backend',$s["id"]],['class'=>'btn btn-sm btn-default','escape'=>false]) ?>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <?php if(!$s["isactive"]): ?>
                                            <?= $this->Html->link(__d('system',"Activate"),['controller'=>'SlideShows','action'=>'changeStatus','plugin'=>'Smashdeal','prefix'=>'Backend','active',$s["id"]]); ?>
                                        <?php else: ?>
                                            <?= $this->Html->link(__d('system',"Inactivate"),['controller'=>'SlideShows','action'=>'changeStatus','plugin'=>'Smashdeal','prefix'=>'Backend','inactive',$s["id"]]); ?>
                                        <?php endif; ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link(__d('system',"Delete"),['controller'=>'SlideShows','action'=>'delete','plugin'=>'Smashdeal','prefix'=>'Backend',$s["id"]]); ?>
                                    </li>
                                </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="6"><?= __d("smashdeal","No Slide Show Images Available"); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <?= $this->Form->end(); ?>
    </div>
</div>