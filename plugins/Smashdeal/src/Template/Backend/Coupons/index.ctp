<?= $this->AssetCompress->css('System.Use54dds',['block'=>'css']) ?>
<?= $this->AssetCompress->script('System.Use54dds',['block'=>'footer-script']) ?>
<?php
$allAmount = count($allcoupons->toArray());
$validAmount = 0;
$redeemAmount = 0;
foreach($allcoupons as $mdl){
    if($mdl["isvalid"]==true)
        $validAmount++;
    else
        $redeemAmount++;
}
?>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">
            <small>
                <strong>
                    <?php if($status==null): ?>
                        <?= "All (".$allAmount.")" ?>
                    <?php else: ?>
                        <?php if(isset($s)): ?>
                            <?= $this->Html->link("All (".$allAmount.")",['controller'=>'Coupons','action'=>'search','plugin'=>'Smashdeal','prefix'=>'Backend',"s"=>$s]); ?>
                        <?php else: ?>
                            <?= $this->Html->link("All (".$allAmount.")",['controller'=>'Coupons','action'=>'index','plugin'=>'Smashdeal','prefix'=>'Backend']); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    |
                    <?php if($status=="valid"): ?>
                        <?= "Valid (".$validAmount.")" ?>
                    <?php else: ?>
                        <?php if(isset($s)): ?>
                            <?= $this->Html->link("Valid (".$validAmount.")",['controller'=>'Coupons','action'=>'search','plugin'=>'Smashdeal','prefix'=>'Backend',"valid","s"=>$s]); ?>
                        <?php else: ?>
                            <?= $this->Html->link("Valid (".$validAmount.")",['controller'=>'Coupons','action'=>'index','plugin'=>'Smashdeal','prefix'=>'Backend',"valid"]); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    |
                    <?php if($status=="redeem"): ?>
                        <?= "Redeemed (".$redeemAmount.")" ?>
                    <?php else: ?>
                        <?php if(isset($s)): ?>
                            <?= $this->Html->link("Redeemed (".$redeemAmount.")",['controller'=>'Coupons','action'=>'search','plugin'=>'Smashdeal','prefix'=>'Backend',"redeem","s"=>$s]); ?>
                        <?php else: ?>
                            <?= $this->Html->link("Redeemed (".$redeemAmount.")",['controller'=>'Coupons','action'=>'index','plugin'=>'Smashdeal','prefix'=>'Backend',"redeem"]); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </strong>
            </small>
        </h3>
        <div class="box-tools col-md-4 no-padding">
            <?= $this->Form->create($coupon,['url'=>['action'=>'search',$status],'type'=>'get']) ?>
            <div class="input-group input-group-sm">
                <input value="<?= (isset($s))?$s:''; ?>" name="s" placeholder="<?= __d('system',"Search...") ?>" class="form-control"/>
                <span class="input-group-btn">
                    <button type="submit" class="btn">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
    <div class="box-body no-padding">
        <?= $this->Form->create($coupon,['url'=>['action'=>'bulkAction'],'id'=>'bulkActionForm']); ?>
        <table class="table">
            <thead>
            <tr>
                <th class="cb-wrap" width="3%">
                    <input type="checkbox" class="checkbox-custom" id="checkall" />
                </th>
                <th><?= __d('system',"Coupon Code") ?></th>
                <th width="5%"><?= __d('system',"Token") ?></th>
                <th width="5%"><?= __d('system',"Status") ?></th>
                <th width="5%"><?= __d('system',"Redeemed") ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($coupons)>0): ?>
                <?php foreach($coupons as $u): ?>
                    <tr>
                        <td class="cb-wrap">
                            <input type="checkbox" name="value[]" value="<?= $u["id"] ?>" class="cb-item checkbox-custom checkbox-child" data-value="<?= $u["id"] ?>"/>
                        </td>
                        <td class="courier"><?= $u["couponcode"] ?></td>
                        <td><?= $u["tb_deal_tokenproduct"]["tokenamount"] ?></td>
                        <td style="text-align:center">
                            <input type="checkbox" class="cbreadonly" <?= ($u["isactive"])?"checked":"" ?> readonly/>
                        </td>
                        <td style="text-align:center">
                            <input type="checkbox" class="cbreadonly" <?= (!$u["isvalid"])?"checked":"" ?> readonly/>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="9">
                        <?php
                        if(isset($s))
                            echo __d('system',"No coupon found for ''{0}'' keyword.",[$s]);
                        else
                            echo __d('system',"No coupon avaiable.");
                        ?>
                    </td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        <?= $this->Form->end(); ?>
    </div>
    <div class="box-footer">
        <?= $this->element('System.paginator'); ?>
    </div>
</div>