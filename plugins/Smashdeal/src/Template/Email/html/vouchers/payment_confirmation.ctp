<?php
	use Cake\I18n\I18n;

	I18n::locale('id_ID');
?>
<div style="max-width:600px;width:600px;margin:auto">
    <div style="content: '';display:table; clear: both;width: 100%">
    	<div style="width: 100%; background-color:#fff;padding:5px;">
	    	<p>Dear <span style="font-weight: bold;color:#01688c"><?= $user['name'] ?></span>,</p>
	    	<p>Kami telah menerima konfirmasi pembayaran anda untuk invoice <span style="color:#01688c;font-weight: bold"><?= $confirmation['purchase']['invoiceno'] ?></span> dengan detail seperti dibawah ini : </p>
	    	<p style="padding:10px;">
	    		<table style="border-collapse: collapse; width: 100%">
	    			<tbody>
	    				<tr>
	    					<td style="background-color:#01688c; color:#fff;width: 50%;padding:5px">Transfer Ke :</td>
	    					<td style="padding:5px"><?= $confirmation['bank_account']['label'] ?></td>
	    				</tr>
	    				<tr>
	    					<td style="background-color:#01688c; color:#fff;width: 50%;padding:5px">Tanggal Ke :</td>
	    					<td style="padding:5px"><?= $confirmation['transferdate']->i18nFormat("dd/MM/yyyy") ?></td>
	    				</tr>
	    				<tr>
	    					<td style="background-color:#01688c; color:#fff;width: 50%;padding:5px">Nama Rekening :</td>
	    					<td style="padding:5px"><?= $confirmation['accountname'] ?></td>
	    				</tr>
	    				<tr>
	    					<td style="background-color:#01688c; color:#fff;width: 50%;padding:5px">Nominal :</td>
	    					<td style="padding:5px"><?= $this->Number->currency($confirmation['nominal']) ?></td>
	    				</tr>
	    			</tbody>
	    		</table>
	    	</p>
	    	<p>Proses konfirmasi akan kami proses selama-lamanya dalam 1x24 Jam.</p>
	    	<p>
				Salam,<br/>
				<span style="font-weight: bold;color:#01688c">Team Dealsmash</span>
			</p>
		</div>
    </div>
</div>