<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?php if(!isset($title)): ?>
                <?= $config_front["SITETITLE"] ?> | <?= $config_front["SITETAGLN"] ?>
            <?php else: ?>
                <?= $config_front["SITETITLE"]; ?> | <?= $title ?>
            <?php endif; ?>
        </title>
        <link href="https://fonts.googleapis.com/css?family=PT+Sans|Titillium+Web:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <?php
        	echo $this->Html->css('materialize.min');
        	echo $this->Html->script('essential.min');
            echo $this->Html->css('Smashdeal.merchant');
        	echo $this->Html->script('materialize.min');
	        echo $this->fetch('css');
        ?>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    </head>
    <body>
        <nav>
            <div class="nav-wrapper sm-nav-wrapper">
                <h1 class="hide"><?= $config_front["SITETITLE"] ?></h1>
                <?= $this->Html->link($this->Html->image('assets_logo_mobile.png'),['controller'=>'Products','action'=>'homepage','prefix'=>'Frontend','plugin'=>'Smashdeal'],['escape'=>false,'class'=>'brand-logo center']) ?>
            </div>
        </nav>
        <main>
            <div class="container">
                <div class="row">
                    <?php if(isset($this->request->session()->read('Merchant')["User"])): ?>
                        <div class="col m3 s12 sidebar-wrapper no-padding">
                            <ul class="sidebar">
                                <li><?= $this->Html->link(__d("Smashdeal","Klaim Produk"),['controller'=>"Merchants",'action'=>'productClaim','plugin'=>'Smashdeal','prefix'=>'Merchant']) ?></li>
                                <li><?= $this->Html->link(__d("Smashdeal","Ganti Password"),['controller'=>'Users','action'=>'changePassword','plugin'=>'Smashdeal','prefix'=>'Merchant']) ?></li>
                                <li><?= $this->Html->link(__d("Smashdeal","Logout"),['controller'=>'Users','action'=>'logout','plugin'=>'Smashdeal','prefix'=>'Merchant']) ?></li>
                            </ul>
                        </div>
                        <div class="col m9 s12">
                            <?= $this->Flash->render('auth'); ?>
                            <?= $this->Flash->render(); ?>
                            <?= $this->fetch('content'); ?>
                        </div>
                    <?php else: ?>
                        <div class="col s12">
                            <?= $this->Flash->render('auth'); ?>
                            <?= $this->Flash->render(); ?>
                            <?= $this->fetch('content'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </main>
    </body>
</html>