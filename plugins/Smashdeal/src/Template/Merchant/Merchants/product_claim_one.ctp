<div class="sm-box">
    <h1 class="section-header"><?= __d("smashdeal", "Klaim Produk - Langkah Pertama") ?></h1>
    <div class="row">
        <div class="col m6 s12 offset-m3">
            <p><?= __d("smashdeal","Silahkan masukan kode voucher") ?></p>
            <?= $this->Form->create($claim,['class'=>'form-horizontal']) ?>
                <div class="form-group">
                    <label for="" class="col-md-3 control-label">
                        <?= __d("smashdeal","Voucher Code") ?>
                    </label>
                    <div class="col-md-6">
                        <input type="text" name="vouchercode" class="form-control" placeholder="<?= __d("smashdeal","Please input the voucher code") ?>">
                        <input type="hidden" name="step" value="two"/>
                    </div>
                </div>
                <div class="button-group clearfix">
                    <div class="col-md-4 col-md-push-4">
                        <button type="submit" class="btn btn-smd btn-sm-primary">
                            <?= __d("smashdeal","Send Code") ?>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>