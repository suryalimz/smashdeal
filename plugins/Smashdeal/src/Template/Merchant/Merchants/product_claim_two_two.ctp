<div class="sm-box">
    <h1 class="section-header"><?= __d("smashdeal", "Klaim Produk - Langkah Kedua") ?></h1>
    <div class="row">
        <div class="col m12 s12">
            <?= $this->Form->create($claim,['class'=>'form-horizontal']) ?>
                <div class="col m6 s12">
                    <?= $this->Form->input('vouchercode',['label'=>'Kode Voucher','readonly'=>true]) ?>
                </div>
                <div class="col m12 s12">
                    <div class="section-header">
                        Detail Pembelian
                    </div>
                </div>
                <div class="col s12">
                    <table class="table cart-table">
                        <thead>
                            <tr>
                                <th width="40%" class="text-center"><?= __d("smashdeal","ITEM") ?></th>
                                <th width="20%" class="text-center"><?= __d("smashdeal","RETAIL PRICE") ?></th>
                                <th width="5%"></th>
                                <th width="10%" class="text-center"><?= __d("smashdeal","QUANTITY") ?></th>
                                <th width="20%" class="text-center"><?= __d("smashdeal","TOTAL") ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="item">
                                        <strong><?= $purchase['tb_deal_product']['productname'] ?></strong>
                                    </div>
                                </td>
                                <td class="text-right">
                                    <div class="item">
                                        <?= $this->Number->currency($purchase['retailprice']) ?>
                                    </div>
                                </td>
                                <td class="center-align">
                                    <div class="item">X</div>
                                </td>
                                <td>
                                    <div class="item">
                                        <?= $purchase['amount'] ?>
                                    </div>
                                </td>
                                <td class="right-align">
                                    <div class="item">
                                        <?= $this->Number->currency($purchase['subtotal']) ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="right-align" colspan="4"><?= __d("smashdeal","Subtotal") ?></td>
                                <td class="right-align">
                                    <?= $this->Number->currency($purchase['subtotal']) ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="right-align" colspan="4"><?= __d("smashdeal","Cash Token") ?></td>
                                <td class="right-align">
                                    <?= $this->Number->currency($purchase['cashtoken']) ?>
                                </td>
                            </tr>
                            <tr class="large">
                                <td class="right-align" colspan="4"><?= __d("smashdeal","Must Pay To Merchant") ?></td>
                                <td class="right-align">
                                    <?= $this->Number->currency($purchase['grandtotal']) ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col m12 s12">
                    <div class="section-header">
                        Data Pembeli
                    </div>
                </div>
                <div class="col m6 s12">
                    <?= $this->Form->input('idno',['label'=>'No. KTP','value'=>$purchase['tb_deal_user']['idno'],'readonly'=>true]) ?>
                </div>
                <div class="col m6 s12">
                    <?= $this->Form->input('name',['label'=>'Nama','value'=>$purchase['tb_deal_user']['name'],'readonly'=>true]) ?>
                </div>
                <div class="col m6 s12">
                    <?= $this->Form->input('address',['label'=>'Alamat','value'=>$purchase['tb_deal_user']['address'],'readonly'=>true]) ?>
                </div>
                <div class="col m6 s12">
                    <?= $this->Form->input('phoneno',['label'=>'No. Handphone','value'=>$purchase['tb_deal_user']['phoneno'],'readonly'=>true]) ?>
                </div>
                <div class="col m12 s12">
                    <div class="section-header">
                        Password User
                    </div>
                    <p>Silahkan meminta user untuk meng-input password</p>
                </div>
                <div class="col m6 s12">
                    <input type="hidden" name="step" value="three"/>
                    <?= $this->Form->input('password',['label'=>'Password']) ?>
                </div>
                <div class="button-group clearfix">
                    <div class="center-align">
                        <button type="submit" class="btn btn-smd btn-sm-primary">
                            <?= __d("smashdeal","Klaim Produk") ?>
                        </button>
                    </div>
                </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>