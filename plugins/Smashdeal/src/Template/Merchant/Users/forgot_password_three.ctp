<div class="container sm-main-content">
    <div class="col-md-6 col-md-push-3">
        <div class="sm-box">
            <h1 class="section-header"><?= __d("smashdeal", "Forgot Password?") ?></h1>
            <p><?= __d("smashdeal","Reset Code has been send to your email. Please check your email inbox") ?></p>
            <?= $this->Form->create($user,['class'=>'form-horizontal']) ?>
                <div class="form-group">
                    <label for="" class="col-md-4 control-label">
                        <?= __d("smashdeal","New Password") ?>
                    </label>
                    <div class="col-md-6">
                        <input type="hidden" name="email" value="<?= $email ?>"/>
                        <input type="password" name="password" class="form-control" placeholder="<?= __d("smashdeal","New Password") ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-md-4 control-label">
                        <?= __d("smashdeal","Confirm Password") ?>
                    </label>
                    <div class="col-md-6">
                        <input type="password" name="confirm_password" class="form-control" placeholder="<?= __d("smashdeal","Confirm Password") ?>">
                    </div>
                </div>
                <div class="button-group clearfix">
                    <div class="col-md-4 col-md-push-4">
                        <button type="submit" class="btn btn-smd btn-sm-primary">
                            Ganti Password
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>