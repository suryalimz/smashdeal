<?php
namespace Smashdeal\Controller\Merchant;

use Smashdeal\Controller\MerchantAreaAppController as AppController;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Merchants Controller
 *
 * @property \Smashdeal\Model\Table\MerchantsTable $Merchants
 */
class MerchantsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Smashdeal.ViewDealUnclaimProduct');
    }

    public function productClaim()
    {
        $this->loadModel('Smashdeal.TbDealClaimVouchers');
        $this->loadModel('Smashdeal.TbDealDirectPurchases');
        $this->loadModel('Smashdeal.ViewDealSinglehostSummary');
        $this->loadModel('Smashdeal.TbDealUsers');
        $claim = $this->TbDealClaimVouchers->newEntity();
        $this->set('claim',$claim);
    	if($this->request->is("post"))
        {
            if($this->request->data["step"]=="two")
            {
                $claim = $this->ViewDealUnclaimProduct->find('all')->where(['vouchercode'=>$this->request->data["vouchercode"],'merchantid'=>$this->Auth->user('id')])->first();
                if($claim!=null){
                    if($claim->claimtype == 1){

                        $summaries = $this->ViewDealSinglehostSummary->find('all')->where(['productid'=>$claim['claimid']])->limit('1');

                        $userid = $summaries->toArray()[0]['userid'];

                        $winnerUser = $this->TbDealUsers->get($userid);
                        $this->loadModel('Smashdeal.TbDealProducts');
                        $product = $this->TbDealProducts->get($claim['claimid']);

                        // $random = rand(1000,9999);
                        // $this->request->session()->write('validation_code',$random);
                        // $email = new Email('waruna');

                        // $email
                        //     ->template('Smashdeal.bargains/validation_code','Smashdeal.default')
                        //     ->emailFormat('html')
                        //     ->from(['support@dealsmash.id' => 'Dealsmash - Support'])
                        //     ->to($winnerUser['email'])
                        //     ->addBcc('admin@dealsmash.id','andy@dealsmash.id')
                        //     ->subject(__d("smashdeal","Dealsmash - Kode Validasi"))
                        //     ->viewVars(['rand'=>$random,'user'=>$winnerUser])
                        //     ->send();

                        // $this->set('email',$winnerUser['email']);

                        $this->set('product',$product);
                        $this->set('winnerUser',$winnerUser);
                        $this->set('summary',$summaries->toArray()[0]);
                        $this->render('product_claim_two_one');
                    }
                    else{
                        $purchase = $this->TbDealDirectPurchases->get($claim->claimid,['contain'=>['TbDealProducts','TbDealUsers']]);
                        // $random = rand(1000,9999);
                        // $this->request->session()->write('validation_code',$random);
                        // $email = new Email('waruna');

                        // $email
                        //     ->template('Smashdeal.bargains/validation_code','Smashdeal.default')
                        //     ->emailFormat('html')
                        //     ->from(['support@dealsmash.id' => 'Dealsmash - Support'])
                        //     ->to($purchase['tb_deal_user']['email'])
                        //     ->addBcc('admin@dealsmash.id','andy@dealsmash.id')
                        //     ->subject(__d("smashdeal","Dealsmash - Kode Validasi"))
                        //     ->viewVars(['rand'=>$random,'user'=>$purchase['tb_deal_user']])
                        //     ->send();

                        // $this->set('email',$purchase['tb_deal_user']['email']);

                        $this->set('purchase',$purchase);
                        $this->render('product_claim_two_two');
                    }
                }
                else{
                    $this->Flash->error(__d('smashdeal',"Tidak Ada Kode Voucher ditemukan."));
                    return $this->render('product_claim_one');  
                }
            }
            else if($this->request->data["step"]=="three")
            {
                $claim = $this->ViewDealUnclaimProduct->find('all')->where(['vouchercode'=>$this->request->data["vouchercode"],'merchantid'=>$this->Auth->user('id')])->first();
                $userid = $claim['userid'];
                $user = $this->TbDealUsers->get($userid);

                $password = $this->request->data["password"];
                // $validation = $this->request->session()->read('validation_code');
                if(!(new DefaultPasswordHasher)->check($password,$user->password))
                {
                    $c = $this->TbDealClaimVouchers->get($claim['id']);

                    $c['claimat'] = new Time();
                    $c['isvalid'] = false;
                    $c['claimby'] = $this->Auth->user('id');
                    $c['idno'] = $this->request->data['idno'];
                    $c['name'] = $this->request->data['name'];
                    $c['address'] = $this->request->data['address'];
                    $c['phoneno'] = $this->request->data['phoneno'];

                    $this->TbDealClaimVouchers->save($c);

                    if($claim->claimtype == 1)
                    {
                        $summaries = $this->ViewDealSinglehostSummary->find('all')->where(['productid'=>$claim['claimid']])->limit('1');

                        $userid = $summaries->toArray()[0]['userid'];
                        $winnerUser = $this->TbDealUsers->get($userid);
                        $email = new Email('waruna');
                        $email
                            ->template('Smashdeal.bargains/claim_validation','Smashdeal.default')
                            ->emailFormat('html')
                            ->from(['support@dealsmash.id' => 'Dealsmash - Support'])
                            ->to($winnerUser['email'])
                            ->addBcc('admin@dealsmash.id','andy@dealsmash.id')
                            ->subject(__d("smashdeal","Dealsmash - Proses Klaim Selesai"))
                            ->viewVars(['user'=>$winnerUser,'vouchercode'=>$c['vouchercode']])
                            ->send();
                    }
                    else
                    {
                        $purchase = $this->TbDealDirectPurchases->get($claim->claimid,['contain'=>['TbDealProducts','TbDealUsers']]);
                        $email = new Email('waruna');
                        $email
                            ->template('Smashdeal.bargains/claim_validation','Smashdeal.default')
                            ->emailFormat('html')
                            ->from(['support@dealsmash.id' => 'Dealsmash - Support'])
                            ->to($purchase['tb_deal_user']['email'])
                            ->addBcc('admin@dealsmash.id','andy@dealsmash.id')
                            ->subject(__d("smashdeal","Dealsmash - Proses Klaim Selesai"))
                            ->viewVars(['user'=>$purchase['tb_deal_user'],'vouchercode'=>$c['vouchercode']])
                            ->send();

                        $this->set('email',$purchase['tb_deal_user']['email']);
                    }

                    $this->Flash->success('Validasi Berhasil dilakukan. Email Bukti Validasi sudah diemail ke peng-klaim');
                    return $this->redirect(['action'=>'productClaim']);
                }
                else
                {
                    $this->Flash->error('Password Tidak Cocok.');
                    $claim = $this->ViewDealUnclaimProduct->find('all')->where(['vouchercode'=>$this->request->data["vouchercode"],'merchantid'=>$this->Auth->user('id')])->first();
                    if($claim->claimtype == 1){
                        $summaries = $this->ViewDealSinglehostSummary->find('all')->where(['productid'=>$claim['claimid']])->limit('1');

                        $userid = $summaries->toArray()[0]['userid'];

                        $winnerUser = $this->TbDealUsers->get($userid);
                        $this->loadModel('Smashdeal.TbDealProducts');
                        $product = $this->TbDealProducts->get($claim['claimid']);

                        // $random = rand(1000,9999);
                        // $this->request->session()->write('validation_code',$random);
                        // $email = new Email('waruna');

                        // $email
                        //     ->template('Smashdeal.bargains/validation_code','Smashdeal.default')
                        //     ->emailFormat('html')
                        //     ->from(['support@dealsmash.id' => 'Dealsmash - Support'])
                        //     ->to($winnerUser['email'])
                        //     ->addBcc('admin@dealsmash.id','andy@dealsmash.id')
                        //     ->subject(__d("smashdeal","Dealsmash - Kode Validasi"))
                        //     ->viewVars(['rand'=>$random,'user'=>$winnerUser])
                        //     ->send();

                        // $this->set('email',$winnerUser['email']);
                        $this->set('product',$product);
                        $this->set('winnerUser',$winnerUser);
                        $this->set('summary',$summaries->toArray()[0]);
                        $this->render('product_claim_two_one');
                    }
                    else{
                        $purchase = $this->TbDealDirectPurchases->get($claim->claimid,['contain'=>['TbDealProducts','TbDealUsers']]);
                        $random = rand(1000,9999);
                        $this->request->session()->write('validation_code',$random);
                        $email = new Email('waruna');

                        $email
                            ->template('Smashdeal.bargains/validation_code','Smashdeal.default')
                            ->emailFormat('html')
                            ->from(['support@dealsmash.id' => 'Dealsmash - Support'])
                            ->to($purchase['tb_deal_user']['email'])
                            ->addBcc('admin@dealsmash.id','andy@dealsmash.id')
                            ->subject(__d("smashdeal","Dealsmash - Kode Validasi"))
                            ->viewVars(['rand'=>$random,'user'=>$purchase['tb_deal_user']])
                            ->send();

                        $this->set('email',$purchase['tb_deal_user']['email']);

                        $this->set('purchase',$purchase);
                        $this->render('product_claim_two_two');
                    }
                }
            }
        }
        else
        {
            $this->render('product_claim_one');
        }
    }
}