<?php

return [
    'nextActive' => '<li class="waves-effect"><a href="{{url}}" class="next"><i class="fa fa-chevron-right"></i></a></li>',
    'nextDisabled' => '<li class="waves-effect"><a><i class="fa fa-chevron-right"></i></a></li>',
    'prevActive' => '<li class="waves-effect"><a href="{{url}}" class="prev"><i class="fa fa-chevron-left"></i></a></li>',
    'prevDisabled' => '<li class="waves-effect"><a><i class="fa fa-chevron-left"></i></a></li>',
    'number' => '<li class="waves-effect"><a href="{{url}}">{{text}}</a></li>',
    'current' => '<li class="active"><a>{{text}}</a></li>'
];