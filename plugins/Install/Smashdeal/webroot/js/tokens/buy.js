$(document).ready(function(e)
{
	$('input').iCheck({
	    checkboxClass: 'icheckbox_polaris',
	    radioClass: 'iradio_polaris',
	    increaseArea: '-10%' // optional
  	});

  	$("input").on('ifChecked',function(event)
  	{
  		var target = $(this).attr("data-target");
  		$(".payment-detail").hide();
  		$("#"+target).show();	
      if(target == "cashtoken")
      {
        $("#withoutunique").show();
        $("#withunique").hide();
      }
      else
      {
        $("#withoutunique").hide();
        $("#withunique").show();
      }
  	});
});