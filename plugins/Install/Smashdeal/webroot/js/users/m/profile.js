$(document).ready(function(e)
{
	$('.datepicker').pickadate({
	    selectMonths: true, // Creates a dropdown to control month
	    selectYears: 50, // Creates a dropdown of 15 years to control year,
 		format:'dd/mm/yyyy'
 	});
 	
});