<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;

/**
 * HomepageSlideShow cell
 */
class HomepageSlideShowCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $this->loadModel('Smashdeal.TbDealSlideshows');
        $slideshows = $this->TbDealSlideshows->find('all')->contain('Medias')->where(['isactive'=>true])->order(['index'=>'ASC']);
        $this->set('slideshows',$slideshows);
    }

    public function mobile()
    {
        $this->loadModel('Smashdeal.TbDealSlideshows');
        $slideshows = $this->TbDealSlideshows->find('all')->contain('Medias')->where(['isactive'=>true])->order(['index'=>'ASC']);
        $this->set('slideshows',$slideshows);
    }
}
