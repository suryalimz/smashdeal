<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;

/**
 * Remaintime cell
 */
class RemaintimeCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($bargainquota,$bargaintimes)
    {
        $this->set('bargainquota',$bargainquota);
        $this->set('bargaintimes',$bargaintimes);
    }

    public function bar($bargainquota,$bargaintimes)
    {
        $bargain_percentage = 0;
        if($bargainquota>0 && $bargaintimes>0)
            $bargain_percentage = ($bargaintimes/$bargainquota)*100;
        $this->set('percentage',$bargain_percentage);
        $this->set('bargainquota',$bargainquota);
        $this->set('bargaintimes',$bargaintimes);
    }

}