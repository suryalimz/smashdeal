<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;

/**
 * RedeemVoucher cell
 */
class RedeemVoucherCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $this->loadModel('Smashdeal.TbDealCoupons');
        $coupon = $this->TbDealCoupons->newEntity();
        $this->set('coupon',$coupon);
    }
}
