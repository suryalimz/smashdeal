<?php
namespace Smashdeal\View\Cell;

use Cake\View\Cell;

/**
 * SingleProductLong cell
 */
class SingleProductLongCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($productid = null,$config_front)
    {
        $this->loadModel('Smashdeal.ViewDealProducts');
        $product = $this->ViewDealProducts->get($productid,['contain'=>['Langs','Metas','Medias','Terms','Merchants']]);
        $this->set('product',$product);
        $this->set('config_front',$config_front);
    }

    public function mobile($productid = null,$config_front)
    {
        $this->loadModel('Smashdeal.ViewDealProducts');
        $product = $this->ViewDealProducts->get($productid,['contain'=>['Langs','Metas','Medias','Terms','Merchants']]);
        $this->set('product',$product);
        $this->set('config_front',$config_front);
    }
}
