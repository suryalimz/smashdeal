<?php
namespace Smashdeal\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Category component
 */
class CategoryComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function getGroupCategories($categorid)
    {
    	$this->TbSysTerms = TableRegistry::get('System.TbSysTerms');
    	$results = [];
    	$children = $this->TbSysTerms->find('all')->select('id')->where(['parentid'=>$categorid]);
    	if(count($children->toArray())>0)
    	{
			$results = array_merge($results,$children->toArray());
    		foreach($children as $child)
    		{
    			$results = array_merge($results,$this->getGroupCategories($child['id']));
    		}
    	}
    	return $results;
    }
}
