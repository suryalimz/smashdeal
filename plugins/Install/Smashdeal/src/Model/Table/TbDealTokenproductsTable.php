<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealTokenproducts Model
 *
 * @method \Smashdeal\Model\Entity\TbDealTokenproduct get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealTokenproduct newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealTokenproduct[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealTokenproduct|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealTokenproduct patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealTokenproduct[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealTokenproduct findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealTokenproductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_tokenproducts');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->hasMany('Smashdeal.TbDealCoupons',['foreignKey'=>'tokenproductid']);
        $this->hasMany('ValidCoupons',['className'=>'Smashdeal.TbDealCoupons','foreignKey'=>'tokenproductid','conditions'=>['and'=>['isvalid'=>true]]]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->integer('tokenamount')
            ->requirePresence('tokenamount', 'create')
            ->notEmpty('tokenamount')
            ->add('tokenamount', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->decimal('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');

        $validator
            ->integer('discount')
            ->requirePresence('discount', 'create')
            ->notEmpty('discount');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['tokenamount']));

        return $rules;
    }
}
