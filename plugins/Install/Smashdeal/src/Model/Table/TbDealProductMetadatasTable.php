<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealProductMetadatas Model
 *
 * @method \Smashdeal\Model\Entity\TbDealProductMetadata get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProductMetadata newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProductMetadata[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProductMetadata|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProductMetadata patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProductMetadata[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealProductMetadata findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealProductMetadatasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_product_metadatas');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->belongsTo('Products',['Smashdeal.TbDealProducts','foreignKey'=>'productid']);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('value');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
