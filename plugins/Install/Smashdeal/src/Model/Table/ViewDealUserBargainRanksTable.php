<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewDealUserBargainRanks Model
 *
 * @method \Smashdeal\Model\Entity\ViewDealUserBargainRank get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUserBargainRank newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUserBargainRank[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUserBargainRank|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUserBargainRank patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUserBargainRank[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\ViewDealUserBargainRank findOrCreate($search, callable $callback = null)
 */
class ViewDealUserBargainRanksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('view_deal_user_bargain_ranks');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('productid');

        $validator
            ->decimal('price')
            ->allowEmpty('price');

        $validator
            ->allowEmpty('count');

        return $validator;
    }
}
