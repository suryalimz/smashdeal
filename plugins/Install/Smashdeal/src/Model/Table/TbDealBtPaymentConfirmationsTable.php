<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealBtPaymentConfirmations Model
 *
 * @method \Smashdeal\Model\Entity\TbDealBtPaymentConfirmation get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBtPaymentConfirmation newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBtPaymentConfirmation[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBtPaymentConfirmation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBtPaymentConfirmation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBtPaymentConfirmation[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealBtPaymentConfirmation findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealBtPaymentConfirmationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_bt_payment_confirmations');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');
        $this->addBehavior('Creator');

        $this->belongsTo('Purchase',['className'=>'Smashdeal.TbDealBtPurchases','foreignKey'=>'purchaseid']);
        $this->belongsTo('BankAccount',['className'=>'Smashdeal.TbDealBankAccounts','foreignKey'=>'bankaccountid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('purchaseid', 'create')
            ->notEmpty('purchaseid');

        $validator
            ->requirePresence('bankaccountid', 'create')
            ->notEmpty('bankaccountid');

        $validator
            ->date('transferdate')
            ->requirePresence('transferdate', 'create')
            ->notEmpty('transferdate');

        $validator
            ->requirePresence('accountname', 'create')
            ->notEmpty('accountname');

        $validator
            ->decimal('nominal')
            ->requirePresence('nominal', 'create')
            ->notEmpty('nominal');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }
}
