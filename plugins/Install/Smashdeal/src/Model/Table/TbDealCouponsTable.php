<?php
namespace Smashdeal\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealCoupons Model
 *
 * @method \Smashdeal\Model\Entity\TbDealCoupon get($primaryKey, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCoupon newEntity($data = null, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCoupon[] newEntities(array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCoupon|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCoupon patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCoupon[] patchEntities($entities, array $data, array $options = [])
 * @method \Smashdeal\Model\Entity\TbDealCoupon findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TbDealCouponsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_coupons');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Uuid');


        $this->belongsTo('Smashdeal.TbDealTokenproducts',['foreignKey'=>'tokenproductid']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('couponcode', 'create')
            ->notEmpty('couponcode');

        $validator
            ->requirePresence('tokenproductid', 'create')
            ->notEmpty('tokenproductid');

        $validator
            ->boolean('isvalid')
            ->allowEmpty('isvalid');

        $validator
            ->boolean('isactive')
            ->allowEmpty('isactive');

        $validator
            ->date('expdate')
            ->allowEmpty('expdate');

        $validator
            ->allowEmpty('createdby');

        $validator
            ->allowEmpty('modifiedby');

        return $validator;
    }

    public function findStatus($query,array $options)
    {
        $s = strtolower($options['s']);
        $valid = (strtolower($s)=="valid")?TRUE:FALSE;
        $query->where(['isvalid'=>$valid]);
        return $query;
    }

    public function findName($query,array $options)
    {
        $s = strtolower($options['s']);
        $status = $options["status"];
        $query->contain('TbDealTokenproducts');
        if($status==null)
            $query->where([
                'or'=>[
                    ['lower(couponcode) LIKE'=>'%'.$s.'%'],
                    ['TbDealTokenproducts.tokenamount'=>$s]
                ]]);
        else{
            $bools = (strtolower($status)=="valid")?TRUE:FALSE;
            $query->where(
                [
                    'AND'=>
                        ['or'=>[
                            ['lower(couponcode) LIKE'=>'%'.$s.'%'],
                            ['lower(TbDealTokenproducts.tokenamount)'=>$s]
                        ]],
                    'isvalid'=>$bools
                ]);
        }
        return $query;
    }
}
