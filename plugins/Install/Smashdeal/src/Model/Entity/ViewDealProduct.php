<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * ViewDealProduct Entity
 *
 * @property string $id
 * @property \Cake\I18n\Time $date
 * @property string $productname
 * @property string $slug
 * @property string $description
 * @property \Cake\I18n\Time $dealend
 * @property float $retailprice
 * @property float $bargainstart
 * @property float $bargainend
 * @property float $incrementvalue
 * @property string $merchantid
 * @property int $bargaintoken
 * @property bool $isactive
 * @property int $publishstatus
 * @property int $visibility
 * @property string $postpassword
 * @property string $permalink
 * @property \Cake\I18n\Time $created
 * @property string $createdby
 * @property \Cake\I18n\Time $modified
 * @property string $modifiedby
 * @property bool $isclosed
 * @property string $closedby
 * @property \Cake\I18n\Time $closed
 * @property string $closedhistoryid
 * @property string $product_category
 */
class ViewDealProduct extends Entity
{

}
