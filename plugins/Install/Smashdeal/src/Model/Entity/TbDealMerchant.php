<?php
namespace Smashdeal\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * TbDealMerchant Entity
 *
 * @property string $id
 * @property string $name
 * @property string $address
 * @property string $phonenoone
 * @property string $phonenotwo
 * @property string $email
 * @property \Cake\I18n\Time $created
 * @property string $createby
 * @property \Cake\I18n\Time $modified
 * @property string $modifiedby
 */
class TbDealMerchant extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_hidden = [
        'password'
    ];


    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }
}
