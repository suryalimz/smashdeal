<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * TbDealBankAccount Entity
 *
 * @property string $id
 * @property string $bank
 * @property string $accountno
 * @property string $accountname
 * @property \Cake\I18n\Time $created
 * @property string $createdby
 * @property \Cake\I18n\Time $modified
 * @property string $modifiedby
 */
class TbDealBankAccount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    public function _getLabel()
    {
        return $this->_properties['bank']." : ".$this->_properties['accountno'].' A/N '.$this->_properties['accountname'];
    }
}
