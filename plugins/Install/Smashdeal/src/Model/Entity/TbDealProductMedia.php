<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * TbDealProductMedia Entity
 *
 * @property string $id
 * @property string $productid
 * @property string $mediaid
 * @property int $index
 * @property bool $isdefault
 * @property \Cake\I18n\Time $created
 * @property string $createdby
 * @property \Cake\I18n\Time $modified
 * @property string $modifiedby
 */
class TbDealProductMedia extends Entity
{

}
