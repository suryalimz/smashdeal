<?php
namespace Smashdeal\Model\Entity;

use Cake\ORM\Entity;

/**
 * ViewDealUnclaimProduct Entity
 *
 * @property string $id
 * @property string $vouchercode
 * @property string $productid
 * @property string $userid
 * @property int $vouchertype
 * @property bool $isvalid
 * @property \Cake\I18n\Time $invalidate
 * @property string $invalidateuser
 * @property string $productname
 * @property string $slug
 * @property string $description
 * @property string $merchantid
 * @property string $username
 * @property string $idno
 * @property string $address
 * @property \Cake\I18n\Time $birthday
 * @property string $handphoneno
 * @property string $phoneno
 * @property string $email
 * @property string $name
 */
class ViewDealUnclaimProduct extends Entity
{

}
