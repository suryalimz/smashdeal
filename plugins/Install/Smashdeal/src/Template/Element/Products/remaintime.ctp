<?php
    function countRemainTime($dt)
    {
        $date = strtotime($dt->i18nFormat("YYYY-MM-dd HH:mm"));

        $result = ["days"=>0,"hours"=>0];
        $diff=$date-time();//time returns current time in seconds
        $days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
        $hours=round(($diff-$days*60*60*24)/(60*60));
        $result["days"] = $days;
        $result["hours"] = $hours;
        return $result;
    }
    $remain =  countRemainTime($enddate);
    if($remain["days"]<0||$remain["hours"]<0)
    {
        echo __d("smashdeal","Bargain Over");
    }
    else{

        echo $remain["days"]." ";
        if($remain["days"]>1){
            echo __d("smashdeal","Days ");
        }
        else
        {
            echo __d("smashdeal","Day ");
        }
        echo $remain["hours"]." ";
        if($remain["hours"]>1){
            echo __d("smashdeal","Hours ");
        }
        else
        {
            echo __d("smashdeal","Hour ");
        }
    }
?>