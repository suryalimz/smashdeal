<div class="sm-bargain-box">
    <div class="row">
        <div class="ribbon">
            Hemat <?= round((($product['retailprice']-$product['bargainstart'])/$product['retailprice']) * 100) ?>%
        </div>
        <div class="col s12 img-wrapper">
            <?php
            if(isset($product->medias)){
                foreach($product->medias as $media)
                {
                    if($media["isdefault"])
                    {
                        $image = $media["mediaid"];
                        break;
                    }
                }
                if(!isset($image) && count($product->medias)>0)
                {
                    $image = $bean->medias[0]["mediaid"];
                }
            }
            ?>
            <?php if(isset($image)): ?>
                <?= $this->cell('System.MediaRenderer',['mediaId'=>$image]); ?>
            <?php else: ?>
                <?= $this->Html->image('no_image.png') ?>
            <?php endif; ?>
        </div>
        <?php $language = []; ?>
        <?php foreach($product->langs as $lng){
            if($lng["languageid"]==$config_front["SITEDFLTLG_VALUE"])
            {
                $language = $lng;
                break;
            }
        } ?>
        <div class="col s12 info-wrapper">
            <h2 class="bargain-title center-align">
                <?= (isset($language["name"])&&$language["name"]!="")?$language["name"]:$product["productname"]; ?>
            </h2>
            <div class="line sm-second-line clearfix">
                <div class="col s3 no-padding">
                            <span class="merchant-name">
                                <i class="fa fa-map-marker"></i>
                                <?= $product->merchant["name"] ?>
                            </span>
                </div>
                <div class="col s9 right-align sm-bargain-retail-price">
                    <span class="extra-label"><?= __d("smashdeal","Retail Price : "); ?></span>
                    <?= $this->Number->currency($product->retailprice); ?>
                </div>
            </div>
            <div class="line sm-third-line clearfix" style="padding-top:10px;padding-bottom: 10px;">
                <div class="col s3  no-padding sm-prd-token-amount">
                    <?= $this->Number->format($product->bargaintoken); ?><br/>
                    <?= ($product->bargaintoken>=2)?__d("smashdeal","Tokens"):__d("smashdeal","Token"); ?>
                </div>
                <div class="col s9">
                    <div class="sm-bargain-detail-price-range right-align">
                        <?= $this->Number->currency($product->bargainstart) ?>-<?= $this->Number->currency($product->bargainend); ?>
                        <br/><?= __d("smashdeal","Multiply : ");?><?= $this->Number->currency($product->incrementvalue) ?></div>
                </div>
            </div>
            <div class="line sm-fifth-line clearfix" style="padding-top: 15px;">
                <?= $this->cell('Smashdeal.Remaintime::bar',['bargainquota'=>$product['bargainquota'],'bargaintimes'=>$product->bargaintimes]) ?>
            </div>
        </div>
    </div>
</div>