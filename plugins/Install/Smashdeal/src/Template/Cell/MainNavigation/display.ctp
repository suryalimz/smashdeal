<?php
function generatedList($list,$config,$html)
{
    echo "<ul>";
    foreach($list as $l)
    {
        $label = $l["name"];
        if(isset($l["term_langs"]))
        {
            foreach($l["term_langs"] as $lang)
            {
                if($lang["languageid"]==$config["SITEDFLTLG_VALUE"])
                {
                    if($lang["name"]!="")
                        $label = $lang["name"];
                    else
                        $label = $l["name"];
                }
            }
        }
        echo "<li>".$html->link($label,['controller'=>'Products','action'=>'index','plugin'=>'Smashdeal','prefix'=>'Frontend',$l["slug"]]);
        if(!empty($l->children))
        {
            generatedList($l->children,$config,$html);
        }
        echo "</li>";
    }
    echo "</ul>";
}
?>
<div class="sm-dropdown">
    <ul>
        <li><?= $this->Html->link(__d("smashdeal","All Bargain"),['controller'=>'Products','action'=>'homepage','plugin'=>'Smashdeal','prefix'=>'Frontend']) ?></li>
        <li>
            <?= $this->Html->link(__d("smashdeal","Category"),"#") ?>
            <?= generatedList($categories,$config,$this->Html) ?>
        </li>
        <li><?= $this->Html->link(__d("smashdeal","Buy Token Online!"),['controller'=>'Vouchers','action'=>'index','prefix'=>'Frontend','plugin'=>'Smashdeal']) ?></li>
        <li><?= $this->cell('Smashdeal.RedeemVoucher',['config'=>$config]); ?></li>
    </ul>
</div>