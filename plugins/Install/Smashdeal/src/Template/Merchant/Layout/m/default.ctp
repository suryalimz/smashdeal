<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?php if(!isset($title)): ?>
                <?= $config_front["SITETITLE"] ?> | <?= $config_front["SITETAGLN"] ?>
            <?php else: ?>
                <?= $config_front["SITETITLE"]; ?> | <?= $title ?>
            <?php endif; ?>
        </title>
        <link href="https://fonts.googleapis.com/css?family=PT+Sans|Titillium+Web:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <?php
        	echo $this->Html->css('materialize.min');
        	echo $this->Html->script('essential.min');
            echo $this->Html->css('Smashdeal.merchant');
        	echo $this->Html->script('materialize.min');
	        echo $this->fetch('css');
        ?>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    </head>
    <body>
        <nav>
            <div class="nav-wrapper sm-nav-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
                <h1 class="hide"><?= $config_front["SITETITLE"] ?></h1>
                <?= $this->Html->link($this->Html->image('assets_logo_mobile.png'),['controller'=>'Products','action'=>'homepage','prefix'=>'Frontend','plugin'=>'Smashdeal'],['escape'=>false,'class'=>'brand-logo center']) ?>
                <?php if(!isset($this->request->session()->read('DealAuth')["User"])): ?>
                    <ul id="slide-out" class="side-nav">
                        <li>
                            <div class="userView">
                                <div class="background">
                                </div>
                                <div style="height: 40px;"></div>
                            </div>
                        </li>
                        <li>
                            <?= $this->Html->link("<i class='fa fa-sign-in'></i>".__d("smashdeal","Login"),['controller'=>'Users','action'=>'login','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false,'class'=>'collapsible-header']); ?>
                        </li>
                    </ul>
                <?php else: ?>
                    <ul id="slide-out" class="side-nav">
                        <li>
                            <div class="userView">
                                <div class="background">
                                </div>
                                <a><?= $this->Html->image('user-white.svg',['class'=>'user-img']) ?></a>
                                <a><span class="white-text name"><?= $this->request->session()->read('DealAuth')['User']['name'] ?></span></a>
                                <?= $this->Html->link("<span class='white-text name'> Bargain Token : ".$this->request->session()->read('DealAuth')['User']['tokenamount']."</span>",['controller'=>'Users','action'=>'bargaintoken','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]) ?>
                                <?= $this->Html->link("<span class='white-text name'> Cash Token : ".$this->Number->currency($this->request->session()->read('DealAuth')['User']['topupamount'])."</span>",['controller'=>'Users','action'=>'cashtoken','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]) ?>
                                <div style="padding-top:10px;"></div>
                            </div>
                        </li>
                        <li class="no-padding">
                            <?= $this->Html->link("<i class='fa fa-home'></i>".__d("smashdeal","Beranda"),['controller'=>'Products','action'=>'homepage','prefix'=>'Frontend','plugin'=>'Smashdeal'],['escape'=>false,'class'=>'collapsible-header']); ?>
                        </li>
                        <li>
                            <?= $this->Html->link("<i class='fa fa-ticket'></i>"."Redeem Voucher",['controller'=>'Coupons','action'=>'redeemVoucher','plugin'=>'smashdeal','prefix'=>'Frontend'],['escape'=>false,'class'=>'collapsible-header']) ?>
                        </li>
                        <li><div class="divider"></div></li>
                        <li>
                            <?= $this->Html->link("<i class='fa fa-ticket'></i>".__d("smashdeal","Buy Token Online!"),['controller'=>'Vouchers','action'=>'index','prefix'=>'Frontend','plugin'=>'Smashdeal'],['escape'=>false,'class'=>'collapsible-header']) ?>
                        </li>
                        <li><div class="divider"></div></li>
                        <li class="no-padding">
                            <ul class="collapsible collapsible-accordion">
                              <li>
                                <a class="collapsible-header"><i class="fa fa-user"></i>My Profile</a>
                                <div class="collapsible-body">
                                  <ul>
                                    <li><?= $this->Html->link(__d("smashdeal","Edit Profile"),['controller'=>'Users','action'=>'profile','plugin'=>'Smashdeal','prefix'=>'Frontend']); ?></li>
                                    <li><?= $this->Html->link(__d("smashdeal","Change Password"),['controller'=>'Users','action'=>'changePassword','plugin'=>'Smashdeal','prefix'=>'Frontend']); ?></li>
                                  </ul>
                                </div>
                              </li>
                            </ul>
                        </li>
                        <li><div class="divider"></div></li>
                        <li class="no-padding">
                            <ul class="collapsible collapsible-accordion">
                              <li>
                                <a class="collapsible-header"><i class="fa fa-handshake-o"></i>My Bargain</a>
                                <div class="collapsible-body">
                                  <ul>
                                    <li><?= $this->Html->link(__d("smashdeal","On Going"),['controller'=>'Bargains','action'=>'ongoing','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
                                    <li><?= $this->Html->link(__d("smashdeal","Status Klaim"),['controller'=>'Users','action'=>'changePassword','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
                                    <li><?= $this->Html->link(__d("smashdeal","History"),['controller'=>'Bargains','action'=>'history','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
                                  </ul>
                                </div>
                              </li>
                            </ul>
                        </li>
                        <li><div class="divider"></div></li>
                        <li class="no-padding">
                            <ul class="collapsible collapsible-accordion">
                              <li>
                                <a class="collapsible-header"><i class="fa fa-shopping-cart"></i>My Purchase</a>
                                <div class="collapsible-body">
                                  <ul>
                                    <li><?= $this->Html->link(__d("smashdeal","Status Klaim"),['controller'=>'Users','action'=>'changePassword','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
                                    <li><?= $this->Html->link(__d("smashdeal","History"),['controller'=>'Users','action'=>'changePassword','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
                                </div>
                              </li>
                            </ul>
                        </li>
                        <li><div class="divider"></div></li>
                        <li class="no-padding">
                            <ul class="collapsible collapsible-accordion">
                              <li>
                                <a class="collapsible-header"><i class="fa fa-shopping-cart"></i></i>BT Purchase</a>
                                <div class="collapsible-body">
                                  <ul>
                                    <li><?= $this->Html->link(__d("smashdeal","Konfirmasi Pembayaran"),['controller'=>'Vouchers','action'=>'unconfirm','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
                                    <li><?= $this->Html->link(__d("smashdeal","Invited Friends"),['controller'=>'Users','action'=>'changePassword','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
                                </div>
                              </li>
                            </ul>
                        </li>
                        <li><div class="divider"></div></li>
                        <li class="no-padding">
                            <ul class="collapsible collapsible-accordion">
                              <li>
                                <a class="collapsible-header"><i class="fa fa-users"></i>My Friends</a>
                                <div class="collapsible-body">
                                  <ul>
                                    <li><?= $this->Html->link(__d("smashdeal","Invite Friend"),['controller'=>'Vouchers','action'=>'unconfirm','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?>
                                    </li>
                                    <li><?= $this->Html->link(__d("smashdeal","Invited Friends"),['controller'=>'Users','action'=>'changePassword','plugin'=>'Smashdeal','prefix'=>'Frontend'],['escape'=>false]); ?></li>
                                </div>
                              </li>
                            </ul>
                        </li>
                        <li><div class="divider"></div></li>
                        <li class="no-padding">
                            <?= $this->Html->link("<i class='fa fa-sign-out'></i>".__d("smashdeal", "Logout"),['controller'=>'Users','action'=>'logout','prefix'=>'Frontend','plugin'=>'Smashdeal'],['escape'=>false,'class'=>'collapsible-header']) ?>
                        </li>
                    </ul>
                <?php endif; ?>
            </div>
        </nav>
        <main>
            <?= $this->Flash->render('auth'); ?>
            <?= $this->Flash->render(); ?>
            <?= $this->fetch('content'); ?>
        </main>
    </body>
    <script type="text/javascript">
        $(".button-collapse").sideNav();
    </script>
</html>