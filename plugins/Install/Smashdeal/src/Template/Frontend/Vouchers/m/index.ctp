<div class="container sm-main-content">
    <h1 class="section-header center-align">
        <?= __d("smashdeal","Buy Token Online") ?>
    </h1>
    <div class="row">
        <?php foreach($tokens  as $token): ?>
            <div class="col s12">
                <div class="token-box">
                    <?= $this->Html->image('coin.png',['style'=>'width:100%']) ?>
                    <h2 class="token-box-title">
                        <?= $token->tokenamount ?>
                        <?= ($token->tokenamount>1)?__d("smashdeal","Tokens"):__d("smashdeal","Token"); ?>
                    </h2>
                    <div class="separator"></div>
                    <div class="body">
                        <div class="stroke-price center-align">
                            <?php if($token->discount!=0): ?>
                                <?= $this->Number->currency($token->price); ?>
                            <?php endif; ?>
                        </div>
                        <div class="price center-align">
                            <?php if($token->discount==0): ?>
                                <?= $this->Number->currency($token->price); ?>
                            <?php else: ?>
                                <?php
                                    $newprice = 0;
                                    $tmp = $token->price * ($token->discount/100);
                                    $newprice = $token->price - $tmp;
                                    echo $this->Number->currency($newprice);
                                ?>
                                <span class="discount">
                                    <?= $token->discount ?>% OFF
                                </span>
                            <?php endif; ?>
                        </div>
                        <div class="btn-wrapper center-align" style="margin-top:10px;">
                            <?=  $this->Html->link(__d("smashdeal","Buy"),['controller'=>'Vouchers','action'=>'buy','prefix'=>'Frontend','plugin'=>'Smashdeal',$token['id']],['class'=>'waves-effect waves-light btn btn-sm-primary']) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<div class="container sm-main-content">
    <h1 class="section-header center-align">
        <?= __d("smashdeal","Beli Bargain Voucher") ?>
    </h1>
    <div class="row">
        <div class="token-box">
            <?= $this->Html->image('20170624123019.jpeg',['style'=>'width:100%']); ?>
            <h2 class="token-box-title">
                <?= __d("smashdeal","Travel Mall") ?>
            </h2>
            <div class="body">
                <div>
                    <i class="fa fa-address-card"></i>
                    Jalan Asia, Komplek Asia Mega Mas Blok N-1 
                </div>
            </div>
        </div>
        <div class="token-box">
            <?= $this->Html->image('20170624134403.jpeg',['style'=>'width:100%']); ?>
            <h2 class="token-box-title">
                <?= __d("smashdeal","Selular 1") ?>
            </h2>
            <div class="body">
                <div>
                    <i class="fa fa-address-card"></i>
                    Sun Plaza LG Floor B-12a-15, B-11 & B-17 
                </div>
            </div>
        </div>
    </div>
</div>