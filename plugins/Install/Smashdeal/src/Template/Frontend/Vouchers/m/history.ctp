<div class="container sm-main-content">
	<h1 class="section-header"><?= __d("smashdeal", "History Pembelian") ?></h1>
	<div class="row scroll">
		<?php if(count($unconfirms)>0): ?>
		    <?php foreach($unconfirms as $unconfirm): ?>
		        <div class="unconfirm-payment">
		        	<div class="row">
		        		<div class="col s6">
		        			<span class="text-label">
		        				<?= __d("smashdeal","No. Invoice : ") ?>
		        			</span>
		        			<br>
		        			<?= $unconfirm["invoiceno"] ?>
		        		</div>
		        		<div class="col s6">
		        			<span class="text-label">
		        				<?= __d("smashdeal","Tanggal Transaksi : ") ?>
		        			</span>
		        			<br>
		        			<?= $unconfirm["created"]->i18nFormat("dd/MM/yyyy") ?>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col s6">
		        			<span class="text-label">
		        				<?= __d("smashdeal","Jenis Pembayaran : ") ?>
		        			</span>
		        			<br>
		        			<?= ($unconfirm["paymenttype"]==1)?"Transfer":"Cash Token" ?>
		        		</div>
		        		<div class="col s6">
		        			<span class="text-label">
		        				<?= __d("smashdeal","Total Pembayaran : ") ?>
		        			</span>
		        			<br>
		        			<?= $this->Number->currency($unconfirm["grandtotal"]); ?>
		        		</div>
		        	</div>
		        </div>
		    <?php endforeach; ?>
	        <?php if(count($unconfirms)>0): ?>
		        <div class="hide paginator">
		            <?= $this->element('System.paginator') ?>
		        </div>
		    <?php endif; ?>
		<?php else: ?>
		    <div class="col-md-12 no-product">
		        <?= __d("smashdeal","Anda tidak memiliki history pembelian bargain token"); ?>
		    </div>
		<?php endif; ?>
	</div>
	<?php if(count($unconfirms)>0): ?>
        <div class="center-align" id="loader" style="margin-bottom: 20px;display: none">
            <div class="preloader-wrapper active">
                <div class="spinner-layer sm-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                        </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<script type="text/javascript">
	$(document).ready(function(e)
	{
	  $(window).scroll(function() {
	     if($(window).scrollTop()>150) {
	        if($("a.next").length>0){
	          console.log("here 2");
	          var href = $("a.next").attr("href");
	          $(".paginator").remove();
	          $("#loader").show();
	          $.get(href,function(data)
	          {
	            $(".scroll").append(data).fadeIn("slow");
	            $("#loader").hide();
	          });
	        }
	     }
	  });
	});
</script>