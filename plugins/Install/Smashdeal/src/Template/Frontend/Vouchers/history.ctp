<?= $this->extend('../Users/template'); ?>
<?= $this->start('profile') ?>
<div class="sm-box clearfix">
	<h1 class="section-header"><?= __d("smashdeal", "History Pembelian Bargain Token") ?></h1>
	<h3 class="section-header">
		<?= __d("smashdeal","Berikut ini adalah history pembelian Bargain Token : ") ?>
	</h3>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th><?= __d("smashdeal","Date") ?></th>
				<th><?= __d("smashdeal","No Invoice") ?></th>
				<th><?= __d("smashdeal","Detail Item") ?></th>
				<th><?= __d("smashdeal","Jenis Pembayaran") ?></th>
				<th><?= __d("smashdeal","Jumlah Pembayaran") ?></th>
			</tr>
		</thead>

		<?php foreach($unconfirms as $key=>$unconfirm): ?>
			<tbody>
				<tr>
					<td><?= $unconfirm['created']->i18nFormat('dd/MM/yyyy') ?></td>
					<td><?= $unconfirm['invoiceno'] ?></td>
					<td><?= $unconfirm['tokenamount']." Bargain Token" ?></td>
					<td><?= ($unconfirm['paymenttype']==1)?"Transfer":"Cash Token" ?></td>
					<td><?= $this->Number->currency($unconfirm['grandtotal']) ?></td>
				</tr>
			</tbody>
		<?php endforeach; ?>
	</table>
	<?= $this->element('System.paginator') ?>
</div>
<?= $this->end(); ?>