<div class="container sm-main-content">
    <div class="col-md-6 col-md-push-3">
        <div class="sm-box">
            <h1 class="section-header"><?= __d("smashdeal", "Forgot Password?") ?></h1>
            <p><?= __d("smashdeal","Please input your registered email.") ?></p>
            <?= $this->Form->create($user,['class'=>'form-horizontal']) ?>
                <div class="form-group">
                    <label for="" class="col-md-3 control-label">
                        <?= __d("smashdeal","Email") ?>
                    </label>
                    <div class="col-md-6">
                        <input type="text" name="email" class="form-control" placeholder="<?= __d("smashdeal","Please input your email") ?>">
                    </div>
                </div>
                <div class="button-group clearfix">
                    <div class="col-md-4 col-md-push-4">
                        <button type="submit" class="btn btn-smd btn-sm-primary">
                            <?= __d("smashdeal","Send Code") ?>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>