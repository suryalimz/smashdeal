<div class="container sm-main-content">
	<div class="sm-box col-md-6 col-md-push-3 col-xs-12">
    	<h1 class="section-header">
	        <?= __d("smashdeal","Successfully Register New Account") ?>
	    </h1>
	    <p>
	        <?= __d("smashdeal","You have successfully register your new account at DealSmash. We have send you a validation email. Please check your inbox and validate your email account.") ?>
	    </p>
    </div>
</div>