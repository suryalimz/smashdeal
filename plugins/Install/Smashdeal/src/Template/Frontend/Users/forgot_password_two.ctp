<div class="container sm-main-content">
    <div class="col-md-6 col-md-push-3">
        <div class="sm-box">
            <h1 class="section-header"><?= __d("smashdeal", "Forgot Password?") ?></h1>
            <p><?= __d("smashdeal","Reset Code has been send to your email. Please check your email inbox") ?></p>
            <?= $this->Form->create($user,['class'=>'form-horizontal']) ?>
                <div class="form-group">
                    <label for="" class="col-md-3 control-label">
                        <?= __d("smashdeal","Reset Code") ?>
                    </label>
                    <div class="col-md-6">
                        <input type="hidden" name="email" value="<?= $email ?>"/>
                        <input type="text" name="code" class="form-control" placeholder="<?= __d("smashdeal","Reset Code") ?>" required="required">
                    </div>
                </div>
                <div class="button-group clearfix">
                    <div class="col-md-5 pull-right">
                        <button type="submit" class="btn btn-smd btn-sm-primary">
                            Reset Password
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>