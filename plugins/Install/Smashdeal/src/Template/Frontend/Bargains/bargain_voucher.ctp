<div class="container sm-main-content">
	<div class="col-md-6 col-md-push-3">
		<div class="sm-box voucher">
			<h1 class="vouchercode">
				<small><?= __d("smashdeal","Kode Voucher") ?></small><br>
				<?= $voucher['vouchercode'] ?>
			</h1>
			<div class="text-center">Validity : <?= $voucher["validity"]->i18nFormat("dd/MM/yyyy") ?></div>
			<h2 class="sub-section text-center">
				<?= __d("smashdeal","Merchant") ?>
			</h2>
			<table style="border-collapse:collapse;border:1px solid #ddd;width:100%">
                <tr>
                    <td style="background-color:#01688c;border:1px solid #ddd;color:#fff;width: 130px;padding:10px;">Nama Merchant</td>
                    <td style="border:1px solid #ddd;width: 270px;padding:10px;"><?= $voucher['merchant']['name'] ?></td>
                </tr>
                <tr>
                    <td style="background-color:#01688c;border:1px solid #ddd;color:#fff;width: 130px;padding:10px;">Alamat</td>
                    <td style="border:1px solid #ddd;width: 270px;padding:10px;"><?= $voucher['merchant']['address'] ?></td>
                </tr>
                <tr>
                    <td style="background-color:#01688c;border:1px solid #ddd;color:#fff;width: 130px;padding:10px;">Telephone</td>
                    <td style="border:1px solid #ddd;width: 270px;padding:10px;"><?= $voucher['merchant']['phonenoone'] ?></td>
                </tr>
                <tr>
                    <td style="background-color:#01688c;border:1px solid #ddd;color:#fff;width: 130px;padding:10px;">Telephone</td>
                    <td style="border:1px solid #ddd;width: 270px;padding:10px;"><?= $voucher['merchant']['phonenotwo'] ?></td>
                </tr>
            </table>
            <h2 class="sub-section text-center">
				<?= __d("smashdeal","Detail Produk") ?>
			</h2>
			<table class="table cart-table">
                <thead>
                    <tr>
                        <th width="40%" class="text-center"><?= __d("smashdeal","ITEM") ?></th>
                        <th width="5%"></th>
                        <th width="10%" class="text-center"><?= __d("smashdeal","QUANTITY") ?></th>
                        <th width="20%" class="text-center"><?= __d("smashdeal","Harga Yang Dimenangkan") ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class="item">
                                <strong><?= $voucher['product']['productname'] ?></strong>
                            </div>
                        </td>
                        <td></td>
                        <td>
                            <div class="item">
                                1
                            </div>
                        </td>
                        <td class="right-align">
                            <div class="item">
                                <?= $this->Number->currency($voucher['price']) ?>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
		</div>
	</div>
</div>