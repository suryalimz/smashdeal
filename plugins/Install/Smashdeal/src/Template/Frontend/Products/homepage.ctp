<?= $this->AssetCompress->script('Smashdeal.Homepage',['block'=>'footer-script']); ?>
<?= $this->cell('Smashdeal.HomepageSlideShow') ?>
<div class="container">
    <h2 class="section-header"><?= __d("smashdeal","All Products") ?></h2>
    <div class="row scroll">
        <?php if(count($products)>0): ?>
            <?php  foreach($products as $product): ?>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <?= $this->Html->link($this->element('Smashdeal.Products/singleproductmd',['product'=>$product]),['controller'=>'Products','action'=>'view','plugin'=>'Smashdeal','prefix'=>'Frontend',$product->slug],['escape'=>false]); ?>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="col-md-12 no-product">
                <?= __d("smashdeal","We currently has no product to display"); ?>
            </div>
        <?php endif; ?>
        <div class="hidden">
            <?= $this->element('System.paginator') ?>
        </div>
    </div>
</div>