<div class="box box-default">
    <div class="box-header">
        <h2 class="box-title"><?= __d("smashdeal","Close Bargains") ?></h2>
    </div>
    <div class="box-body no-padding">
        <table class="table">
            <thead>
            <tr>
                <th><?= __d("smashdeal","No.") ?></th>
                <th><?= __d("smashdeal","Product Name") ?></th>
                <th><?= __d("smashdeal","Publish At") ?></th>
                <th><?= __d("smashdeal","End At") ?></th>
                <th><?= __d("smashdeal","Action") ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($bargains)>0): ?>
            <?php foreach($bargains as $key=>$bargain): ?>
                <tr>
                    <td><?= $key+1 ?></td>
                    <td><?= $bargain["productname"] ?></td>
                    <td><?= $bargain["date"]->i18nFormat($config["SITEDTFORM"]) ?></td>
                    <td><?= $this->cell('Smashdeal.Remaintime',['bargainquota'=>$bargain->bargainquota,'bargaintimes'=>$bargain->bargaintimes]) ?></td>
                    <td><?= $this->Html->link(__d("smashdeal","close"),['controller'=>'Bargains','action'=>'close','prefix'=>'Backend','plugin'=>'Smashdeal',$bargain->id]) ?></td>
                </tr>
            <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="4">
                        <?= __d("smashdeal","No Bargains to be closed"); ?>
                    </td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <div class="box-footer">
        <?= $this->element('System.paginator'); ?>
    </div>
</div>