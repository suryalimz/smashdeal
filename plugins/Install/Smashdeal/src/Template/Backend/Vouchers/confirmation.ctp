<div class="box box-default">
	<div class="box-header">
		<h2 class="box-title"><?= __d("smashdeal","Bank Transfer Confirmation") ?></h2>
	</div>
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th><?= __d("smashdeal","No.") ?></th>
					<th><?= __d("smashdeal","Invoice No.") ?></th>
					<th><?= __d("smashdeal","Bank Account") ?></th>
					<th><?= __d("smashdeal","Account Name") ?></th>
					<th><?= __d("smashdeal","Nominal") ?></th>
					<th><?= __d("smashdeal","Note") ?></th>
					<th><?= __d("smashdeal","action") ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($confirmations->toArray())>0): ?>
					<?php foreach($confirmations as $key=>$confirmation): ?>
						<tr>
							<td><?= $key+1 ?></td>
							<td>
								<?= $confirmation['purchase']['invoiceno']; ?>
							</td>
							<td>
								<?= $confirmation['bank_account']['label'] ?>
							</td>
							<td>
								<?= $confirmation['accountname'] ?>
							</td>
							<td>
								<?= $this->Number->currency($confirmation['nominal']) ?>
							</td>
							<td>
								<textarea id="<?= $confirmation['purchase']['invoiceno']; ?>"></textarea>
							</td>
							<td>
								<div class="btn">
									<?= $this->Html->link('approve',['controller'=>'Vouchers','action'=>'approval','prefix'=>'Ajax','plugin'=>'Smashdeal',$confirmation['id'],2],['class'=>'btn-approve text-success','data-target'=>"#".$confirmation['purchase']['invoiceno']]) ?>
									|
									<?= $this->Html->link('reject',['controller'=>'Vouchers','action'=>'approval','prefix'=>'Ajax','plugin'=>'Smashdeal',$confirmation['id'],0],['class'=>'btn-reject text-danger','data-target'=>"#".$confirmation['purchase']['invoiceno']]) ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="6">
							<?= __d("smashdeal","No Unconfirm Payment Confirmation") ?>
						</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>

<script>
	$(document).ready(function(e)
	{
		$(".btn-approve").click(function(e)
		{
			e.preventDefault();
			var target = $($(this).attr("data-target"));
			var parent = $(this).parent().parent();
			$(this).parent().hide();
			$(parent).append("<div class='loading'>Please Wait...</div>");

			$.post(
				$(this).attr("href"),
				{'note':$(target).val()},
				function(response){
					data = JSON.parse(response);
					$(parent).find(".loading").remove();
					if(data.status)
					{
						$(parent).empty().append(data.type);
						$(target).attr("readonly","readonly");
					}
				}
			);
		});

		$(".btn-reject").click(function(e)
		{
			e.preventDefault();
			var target = $($(this).attr("data-target"));
			var parent = $(this).parent().parent();
			$(this).parent().hide();
			if($(target).val()=="")
			{
				$(target).focus();
				alert("Please Input The Note");
			}
			else
			{
				$(parent).append("<div class='loading'>Please Wait...</div>");
				$.post(
					$(this).attr("href"),
					{'note':$(target).val()},
					function(response){
						data = JSON.parse(response);
						$(parent).find(".loading").remove();
						if(data.status)
						{
							$(parent).empty().append(data.type);
							$(target).attr("readonly","readonly");
						}
					}
				);
			}
		});
	});
</script>