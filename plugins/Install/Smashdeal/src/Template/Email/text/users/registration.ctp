<h1><?= __d("smashdeal","Activate Your Account"); ?></h1>
<p><?= __d("smashdeal","We just need to validate your email address to activate your Dealsmash account. Simply click the following button:") ?></p>
<p><a href="<?= $this->Url->build(['controller'=>'Users','action'=>'validation','plugin'=>'Smashdeal','prefix'=>'Frontend',$user->validationcode],['fullBase'=>true]) ?>" class="btn btn-smd"><?= __d("smashdeal","Activate Account") ?></a></p>
<p><?= __d("smashdeal","If the link doesn’t work, copy this URL into your browser:") ?></p>
<p><?= $this->Url->build(['controller'=>'Users','action'=>'validation','plugin'=>'Smashdeal','prefix'=>'Frontend',$user->validationcode],['fullBase'=>true]); ?></p>
<p><?= __d("smashdeal","Welcome!!") ?><br/><?= __d("smashdeal","The Deal Smash Crew") ?></p>
<p><?= __d("smashdeal","This email was sent to you by DealSmash because you signed up for a DealSmash account. Please let us know if you feel that this email was sent to you by error."); ?></p>