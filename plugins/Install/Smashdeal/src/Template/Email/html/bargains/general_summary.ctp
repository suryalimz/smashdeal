<?php
	use Cake\I18n\I18n;

	I18n::locale('id_ID');
?>
<div style="max-width: 900px; margin:auto;width: 800px;">
    <div style="content: '';display:table; clear: both;width: 100%">
    	<div style="width: 100%; background-color:#fff;padding:10px;">
	    	<p>Dear <span style="font-weight: bold;color:#01688c"><?= $user['name'] ?></span>,</p>
	    	<p>Berikut ini adalah summary Top 10 Single Host untuk produk : </p>
	    	<p>
	    		<div style="width:80%;margin:auto;border:1px solid #ddd;content: '';display:table; clear: both;padding:15px;">
	    			<div style="width:150px;height:150px; margin:auto;background:url('http://dealsmash.id/files/<?= $image ?>'); background-repeat: no-repeat;background-position: center;background-size: contain">
	    			</div>
	    			<h2 style="text-align: center; font-size:1.3em;color:#333">
	    				<?= $product['productname'] ?>
	    			</h2>
	    			<p style="text-align: center;color:#333">
	    				<div style="width: 60%;margin:auto;">
		    				<div style="float:left;width: 30%">
		    					<div style="background-color:#01688c;color:#fff;font-size: 1.2em;font-weight: bold;width: 100%;text-align: center">
		    						<?= $product['bargaintoken'] ?> <br> Bargain <br/> Token
		    					</div>
		    				</div>
		    				<div style="float:left;width: 70%;">
		    					<div style="padding-left: 10px">
				    				<span>
				    					<span style="color:#01688c;font-size: 1.1em;font-weight:bold;">Limit Nilai Bargain :</span>
				    					<br>
				    					<span><?= $this->Number->currency($product['bargainstart']) ?></span>-
				    					<span><?= $this->Number->currency($product['bargainend']) ?></span>
		    					 	</span>
				    				<br/>
				    				<span>
				    					<span style="color:#01688c;font-size: 1.1em;font-weight:bold;">Harga Retail :</span> 
				    					<br>		
				    					<span><?= $this->Number->currency($product['retailprice']) ?></span>
				    				</span>
			    				</div>
		    				</div>
	    				</div>
	    			</p>
	    		</div>
	    	</p>
	    	<?php if(count($summaries)>0): ?>
	    		<div style="width: 80%;margin:auto;">
			    	<h2 style="font-size: 1.3em;text-align: center;font-family: 'San Serif">Top 10</h2>
			    	<p style="text-align: center">
			    		<table style="width: 100%;border-collapse: collapse;">
			    			<thead>
			    				<tr style="background-color: #01688c; color: #fff;border-bottom: 2px solid #f9b916">
			    					<th style="padding: 5px;">User</th>
			    					<th style="padding: 5px;">Bargain</th>
			    					<th style="padding: 5px;">Rank</th>
			    				</tr>
			    			</thead>
			    			<tbody>
			    				<?php foreach($summaries as $key=>$summary): ?>
			    					<tr style="border-bottom:1px solid #ccc">
			    						<td style="padding:5px;">
			    							<?= $summary['username'] ?>
			    						</td>
			    						<td style="padding:5px;text-align: center;">
			    							<?= $this->Number->currency($summary['price']) ?>
			    						</td>
			    						<td style="padding:5px;text-align: center;">
	    									<?= $this->Number->ordinal($key+1,['locale'=>'en_EN']); ?>
			    						</td>
			    					</tr>
			    				<?php endforeach; ?>
			    			</tbody>
			    		</table>
			    	</p>
		    	</div>
		    	<p>
		    		Untuk pemenang akan mendapatkan email terpisah berisikan kode voucher yang akan digunakan pada saat klaim produk ke merchant.
		    	</p>
		    <?php else: ?>
		    	<p>Sayang sekali, tidak ada single host untuk bargain ini.</p>
		    <?php endif; ?>
		    <p>
				Salam,<br/>
				<span style="font-weight: bold;color:#01688c">Team Dealsmash</span>
			</p>
		</div>
    </div>
</div>