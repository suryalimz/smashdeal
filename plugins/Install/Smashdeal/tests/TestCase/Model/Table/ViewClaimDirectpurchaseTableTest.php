<?php
namespace Smashdeal\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Model\Table\ViewClaimDirectpurchaseTable;

/**
 * Smashdeal\Model\Table\ViewClaimDirectpurchaseTable Test Case
 */
class ViewClaimDirectpurchaseTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Model\Table\ViewClaimDirectpurchaseTable
     */
    public $ViewClaimDirectpurchase;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.smashdeal.view_claim_directpurchase'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ViewClaimDirectpurchase') ? [] : ['className' => 'Smashdeal\Model\Table\ViewClaimDirectpurchaseTable'];
        $this->ViewClaimDirectpurchase = TableRegistry::get('ViewClaimDirectpurchase', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ViewClaimDirectpurchase);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
