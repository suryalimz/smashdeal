<?php
namespace Smashdeal\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Smashdeal\Controller\Component\SerialComponent;

/**
 * Smashdeal\Controller\Component\SerialComponent Test Case
 */
class SerialComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Smashdeal\Controller\Component\SerialComponent
     */
    public $Serial;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Serial = new SerialComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Serial);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
