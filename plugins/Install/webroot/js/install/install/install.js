/**
 * When Database Engine change then the default port changing according to the engine.
 **/
$("#dbengine").change(function(e){
	e.preventDefault();
	switch($(this).val()){
		case "mysql":
			$("#port").val("3306");
			break;
		case "pgsql":
			$("#port").val("5432");
			break;
		default:
			$("#port").val("");
			break;
	}	
});