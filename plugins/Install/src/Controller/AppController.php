<?php

namespace Install\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;

class AppController extends Controller
{
    public $helpers = [
        'AssetCompress.AssetCompress'
    ];

	public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('Install.default');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');
    }

    public function beforeRender(Event $event)
    {
    	if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
}
