<?php

namespace Install\Controller;

use Install\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Install\Form\SetupForm;

class InstallController extends AppController
{

	public function install($language)
	{
		ConnectionManager::config('installation', [
	        'className' => 'Cake\Database\Connection',
	        'driver' => 'Cake\Database\Driver\Sqlite',
	        'database' => PLUGINS."Install".DS."config".DS."schema".DS.'installation.db',
	        'encoding' => 'utf8',
	        'cacheMetadata' => true,
	        'quoteIdentifiers' => false,
	    ]);
	    $c = ConnectionManager::get('installation');
	    $results = $c->newQuery()
	    			 ->select(['id','label'])
	    			 ->from('tb_sys_lookup_details')
	    			 ->where(['lookupid'=>'bf47d5fd-00fc-44ec-abd1-97b8ef948dab'])
	    			 ->order(['label'=>'ASC'])
	    			 ->execute()
	    			 ->fetchAll('assoc');
	    $languages = [];
	    foreach($results as $result)
	    {
	    	$languages[$result["id"]] = $result["label"];
	    }
		$setupForm = new SetupForm();
		if($this->request->is('post'))
		{
			$data = $this->request->data;
			$dataSource["DataSource"] = [
				"driver" => $data["dbengine"],
				"port" => $data["port"],
				"host" => $data["host"],
				"database" => $data["database"],
				"username" => $data["username"],
				"password" => $data["password"]
			];
			switch ($dataSource["DataSource"]["driver"]) {
				case 'mysql':
					$dataSource["DataSource"]["driver"] = "Cake\Database\Driver\Mysql";
					break;
				case 'pgsql':
					$dataSource["DataSource"]["driver"] = "Cake\Database\Driver\Postgres";
					break;
				default:
					# code...
					break;
			}

			try{
				ConnectionManager::config('iconnection',[
				    'className' => 'Cake\Database\Connection',
				    'driver' => $dataSource["DataSource"]["driver"],
				    'persistent' => false,
				    'host' => $dataSource["DataSource"]["host"],
				    'username' => $dataSource["DataSource"]["username"],
				    'password' => $dataSource["DataSource"]["password"],
				    'database' => $dataSource["DataSource"]["database"],
				    'encoding' => 'utf8',
				    'timezone' => 'UTC',
				    'cacheMetadata' => true,
				]);
				$conn = ConnectionManager::get('iconnection');
				$conn->begin();

				/** Reading Core Sql, Create Database, etc. **/
				$core = file_get_contents(PLUGINS."Install".DS."config".DS."schema".DS."core.sql");

				$cores = explode("---  Separator ---",$core);
				foreach($cores as $key=>$sql)
				{
					$conn->query($sql);
				}

				/** Reading Setup Sql, Inserting Data **/
				$setup =  file_get_contents(PLUGINS."Install".DS."config".DS."schema".DS."setup.sql");
				$setups =  explode("---  Separator ---", $setup);
				foreach($setups as $key=>$sql)
				{
					$conn->query($sql);
				}

				if(isset($data["webtitle"]))
				{
					$data["webtitle"] = str_replace("'","''",$data["webtitle"]);
					$conn->query("UPDATE tb_sys_configs SET \"value\" ='".$data["webtitle"]."' WHERE id = 'f2951f27-97e9-4a38-aaaf-4017b06d9f9f' or code = 'SITETITLE'");
				}
				
				if(isset($data["tagline"]))
				{
					$data["tagline"] = str_replace("'","''",$data["tagline"]);
					$conn->query("UPDATE tb_sys_configs SET \"value\" ='".$data["tagline"]."' WHERE id = '5b5d9b86-6fba-4171-80c6-22b3919573f9' or code = 'SITETAGLN'");
				}

				if(isset($data["language"]))
				{
					$conn->query("UPDATE tb_sys_configs SET \"value\" ='".$data["language"]."' WHERE id = '8af4113b-ec0b-4f75-a375-895488a46d3f' or code = 'SITEDFLTLG'");
				}

				if(isset($data["adminprx"]))
				{
					$data["adminprx"] = "/".str_replace("/", "", $data["adminprx"]);
					$conn->query("UPDATE tb_sys_configs SET \"value\" ='".$data["adminprx"]."' WHERE id = '86cbfe54-802e-4de5-833e-b91b396b2854' or code = 'ADMPRX'");
				}

				/** Saving Configuration File to config folder with name config.ini **/
				if(file_exists(CONFIG."config.ini"))
				{
					unlink(CONFIG."config.ini");
				}
				$this->write_ini_file($dataSource,CONFIG."config.ini",true);

				$conn->commit();
				$this->redirect($data["adminprx"]."/");

				//Check If other plugins is already installed on.
			}
			catch(PDOException $ex){
				$this->Flash->error("Fail to create database please make sure you have the right configuration");
			}
		}
		$this->set('setup',$setupForm);
		$this->set('defaultlanguage','52f87f69-8024-4aee-a1e6-aa88d12876fd');
		$this->set('languages',$languages);
	}

	public function toInstall()
	{
		return $this->redirect(['controller'=>'Install','action'=>'install','plugin'=>'Install','en_En']);
	}

	private function write_ini_file($assoc_arr, $path, $has_sections=FALSE)
	{ 
	    $content = ""; 
	    if ($has_sections) { 
	        foreach ($assoc_arr as $key=>$elem) { 
	            $content .= "[".$key."]\n"; 
	            foreach ($elem as $key2=>$elem2) { 
	                if(is_array($elem2)) 
	                { 
	                    for($i=0;$i<count($elem2);$i++) 
	                    { 
	                        $content .= $key2."[] = \"".$elem2[$i]."\"\n"; 
	                    } 
	                } 
	                else if($elem2=="") $content .= $key2." = \n"; 
	                else $content .= $key2." = \"".$elem2."\"\n"; 
	            } 
	        } 
	    } 
	    else { 
	        foreach ($assoc_arr as $key=>$elem) { 
	            if(is_array($elem)) 
	            { 
	                for($i=0;$i<count($elem);$i++) 
	                { 
	                    $content .= $key."[] = \"".$elem[$i]."\"\n"; 
	                } 
	            } 
	            else if($elem=="") $content .= $key." = \n"; 
	            else $content .= $key." = \"".$elem."\"\n"; 
	        } 
	    } 

	    if (!$handle = fopen($path, 'w')) { 
	        return false; 
	    }

	    $success = fwrite($handle, $content);
	    fclose($handle); 

	    return $success; 
	}
}