<div class="col-md-8 col-md-push-2 install-wrapper">
	<h1 class="text-center"><?= __d("install","Installation") ?></h1>
	<div class="col-md-12 install-box">
		<?= $this->Form->create($setup,['class'=>'form-horizontal']); ?>
		<div class="form-header">
			<h2><i class="fa fa-database"></i>&nbsp;<?= __d("install","Database Setup") ?></h2>
		</div>
		<p class="form-header-desc"><?= __d("install","Below you should enter your database connection details. If you're not sure about these, contact your host."); ?></p>
		<div class="form-group">
			<label class="control-label col-md-3">
				<?= __d("install","Database Engine") ?>
			</label>
			<div class="col-md-5">
				<?php
					$options  = ['mysql'=>__d("install","MySql"),'pgsql'=>__d("install","PostgreSql")];
					echo $this->Form->input('dbengine',['label'=>false,'class'=>'form-control','id'=>'dbengine','empty'=>__d("install","--Choose Engine--"),'options'=>$options]);?>
			</div>
			<div class="col-md-4 field-info">
				<?= __d("install","This is database engine you use.") ?>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">
				<?= __d("install","Database Host") ?>
			</label>
			<div class="col-md-5">
				<?= $this->Form->input('host',['class'=>'form-control','placeholder'=>__d("install","localhost"),'label'=>false]); ?>
			</div>
			<div class="col-md-4 field-info">
				<?= __d("install","This is host where iCMS connect to your database.") ?>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">
				<?= __d("install","Database Port") ?>
			</label>
			<div class="col-md-5">
				<?= $this->Form->input('port',['class'=>'form-control','placeholder'=>__d("install","port"),'label'=>false]); ?>
			</div>
			<div class="col-md-4 field-info">
				<?= __d("install","This is port where your database engine exist.") ?>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">
				<?= __d("install","Database Name") ?>
			</label>
			<div class="col-md-5">
				<?= $this->Form->input('database',['class'=>'form-control','placeholder'=>__d("install","icms"),'label'=>false]); ?>
			</div>
			<div class="col-md-4 field-info">
				<?= __d("install","This is name of your database where run iCMS in.") ?>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">
				<?= __d("install","Username") ?>
			</label>
			<div class="col-md-5">
				<?= $this->Form->input('username',['class'=>'form-control','placeholder'=>__d("install","username"),'label'=>false]); ?>
			</div>
			<div class="col-md-4 field-info">
				<?= __d("install","Your database server username") ?>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">
				<?= __d("install","Password") ?>
			</label>
			<div class="col-md-5">
				<?= $this->Form->input('password',['class'=>'form-control','placeholder'=>__d("install","password"),'label'=>false]); ?>
			</div>
			<div class="col-md-4 field-info">
				<?= __d("install","... and Last your database server password") ?>
			</div>
		</div>
		<div class="form-header">
			<h2><i class="fa fa-globe"></i>&nbsp;<?= __d("install","Website Setup") ?></h2>
		</div>
		<p class="form-header-desc">
			<?= __d("install","Below you should enter your some of your website information.") ?>
		</p>
		<div class="form-group">
			<label class="control-label col-md-3">
				<?= __d("install","Website Title") ?>
			</label>
			<div class="col-md-5">
				<?= $this->Form->input('webtitle',['class'=>'form-control','placeholder'=>__d("install","eq: Talefeast, Foodeck"),'label'=>false]); ?>
			</div>
			<div class="col-md-4">
				<?= __d("install","This is your website title.") ?>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">
				<?= __d("install","Tagline") ?>
			</label>
			<div class="col-md-5">
				<?= $this->Form->input('tagline',['class'=>'form-control','placeholder'=>__d("install","eq: Serve Better Information, Everystory for everyone"),'label'=>false,'rows'=>3]); ?>
			</div>
			<div class="col-md-4">
				<?= __d("install","Descripbe your website in one sentence.") ?>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">
				<?= __d("install","Default Language") ?>
			</label>
			<div class="col-md-5">
				<?= $this->Form->input('language',['label'=>false,'class'=>'form-control','empty'=>__d("install","--Choose Language--"),'options'=>$languages,'value'=>$defaultlanguage]); ?>
			</div>
			<div class="col-md-4">
				<?= __d("install","This is your website default language.") ?>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">
				<?= __d("install","Admin Prefix") ?>
			</label>
			<div class="col-md-5">
				<?= $this->Form->input('adminprx',['class'=>'form-control','placeholder'=>__d("install","eq: admin, manager, manager-gui, etc."),'label'=>false,'value'=>'manage']); ?>
			</div>
			<div class="col-md-4">
				<?= __d("install","This is where you access your admin page. (default : [domain]/manage)") ?>
			</div>
		</div>
		<div class="form-group">
			<input type="submit" value="<?= __d("install","Finish!") ?>" class="btn btn-lg btn-primary center-block bg-i-grey" style="width:150px"/>
		</div>
		<?= $this->Form->end(); ?>
	</div>
</div>