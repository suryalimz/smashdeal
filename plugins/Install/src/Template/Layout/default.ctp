<!DOCTYPE html>
<html>
<head>
	<title><?= __d("install","iCMS - Installation"); ?></title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:700,400' rel='stylesheet' type='text/css'>
	<?php
		echo $this->AssetCompress->css('Install.eDd2XSD');
		echo $this->fetch('css');
	?>
</head>
<body class="hold-transition register-page">
	
	<div class="container">
		
        <?= $this->fetch('content'); ?>

	</div>

</body>
<?php
	echo $this->AssetCompress->script('Install.eDd2XSD');
    echo $this->fetch('footer-script');
?>
</html>