<?php
$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
?>
<div class="alert alert-warning alert-dismissable alert-icms">
    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
    <?= h($message) ?>
    <script>
        $(".alert").delay(5000).fadeOut();
    </script>
</div>
