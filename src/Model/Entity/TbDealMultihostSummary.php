<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TbDealMultihostSummary Entity
 *
 * @property int $r
 * @property string $id
 * @property string $userid
 * @property string $productid
 * @property float $price
 * @property \Cake\I18n\Time $bargaintime
 * @property int $guesthost
 */
class TbDealMultihostSummary extends Entity
{

}
