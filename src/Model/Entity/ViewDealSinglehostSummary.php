<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ViewDealSinglehostSummary Entity
 *
 * @property string $userid
 * @property string $productid
 * @property float $price
 * @property int $count
 * @property \Cake\I18n\Time $bargaintime
 */
class ViewDealSinglehostSummary extends Entity
{

}
