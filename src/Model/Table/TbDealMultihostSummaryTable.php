<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbDealMultihostSummary Model
 *
 * @method \App\Model\Entity\TbDealMultihostSummary get($primaryKey, $options = [])
 * @method \App\Model\Entity\TbDealMultihostSummary newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TbDealMultihostSummary[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TbDealMultihostSummary|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TbDealMultihostSummary patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TbDealMultihostSummary[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TbDealMultihostSummary findOrCreate($search, callable $callback = null)
 */
class TbDealMultihostSummaryTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tb_deal_multihost_summary');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('r');

        $validator
            ->allowEmpty('id');

        $validator
            ->allowEmpty('userid');

        $validator
            ->allowEmpty('productid');

        $validator
            ->decimal('price')
            ->allowEmpty('price');

        $validator
            ->dateTime('bargaintime')
            ->allowEmpty('bargaintime');

        $validator
            ->allowEmpty('guesthost');

        return $validator;
    }
}
